package com.topica.toppick.web.rest.vm;

/**
 * 
 * @author dienlh
 *
 *         - user_id - avatar(image) - description
 * 
 */
public class CurrentInfoVM {
	private Long wpPostId;
	private String description;

	public Long getWpPostId() {
		return wpPostId;
	}

	public void setWpPostId(Long wpPostId) {
		this.wpPostId = wpPostId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
