package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpPostmetaService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpPostmetaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpPostmeta.
 */
@RestController
@RequestMapping("/api")
public class WpPostmetaResource {

    private final Logger log = LoggerFactory.getLogger(WpPostmetaResource.class);

    private static final String ENTITY_NAME = "wpPostmeta";

    private WpPostmetaService wpPostmetaService;

    public WpPostmetaResource(WpPostmetaService wpPostmetaService) {
        this.wpPostmetaService = wpPostmetaService;
    }

    /**
     * POST  /wp-postmetas : Create a new wpPostmeta.
     *
     * @param wpPostmetaDTO the wpPostmetaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpPostmetaDTO, or with status 400 (Bad Request) if the wpPostmeta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-postmetas")
    @Timed
    public ResponseEntity<WpPostmetaDTO> createWpPostmeta(@RequestBody WpPostmetaDTO wpPostmetaDTO) throws URISyntaxException {
        log.debug("REST request to save WpPostmeta : {}", wpPostmetaDTO);
        if (wpPostmetaDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpPostmeta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpPostmetaDTO result = wpPostmetaService.save(wpPostmetaDTO);
        return ResponseEntity.created(new URI("/api/wp-postmetas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-postmetas : Updates an existing wpPostmeta.
     *
     * @param wpPostmetaDTO the wpPostmetaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpPostmetaDTO,
     * or with status 400 (Bad Request) if the wpPostmetaDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpPostmetaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-postmetas")
    @Timed
    public ResponseEntity<WpPostmetaDTO> updateWpPostmeta(@RequestBody WpPostmetaDTO wpPostmetaDTO) throws URISyntaxException {
        log.debug("REST request to update WpPostmeta : {}", wpPostmetaDTO);
        if (wpPostmetaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpPostmetaDTO result = wpPostmetaService.save(wpPostmetaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpPostmetaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-postmetas : get all the wpPostmetas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpPostmetas in body
     */
    @GetMapping("/wp-postmetas")
    @Timed
    public ResponseEntity<List<WpPostmetaDTO>> getAllWpPostmetas(Pageable pageable) {
        log.debug("REST request to get a page of WpPostmetas");
        Page<WpPostmetaDTO> page = wpPostmetaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-postmetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-postmetas/:id : get the "id" wpPostmeta.
     *
     * @param id the id of the wpPostmetaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpPostmetaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-postmetas/{id}")
    @Timed
    public ResponseEntity<WpPostmetaDTO> getWpPostmeta(@PathVariable Long id) {
        log.debug("REST request to get WpPostmeta : {}", id);
        Optional<WpPostmetaDTO> wpPostmetaDTO = wpPostmetaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpPostmetaDTO);
    }

    /**
     * DELETE  /wp-postmetas/:id : delete the "id" wpPostmeta.
     *
     * @param id the id of the wpPostmetaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-postmetas/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpPostmeta(@PathVariable Long id) {
        log.debug("REST request to delete WpPostmeta : {}", id);
        wpPostmetaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-postmetas?query=:query : search for the wpPostmeta corresponding
     * to the query.
     *
     * @param query the query of the wpPostmeta search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-postmetas")
    @Timed
    public ResponseEntity<List<WpPostmetaDTO>> searchWpPostmetas(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpPostmetas for query {}", query);
        Page<WpPostmetaDTO> page = wpPostmetaService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-postmetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
