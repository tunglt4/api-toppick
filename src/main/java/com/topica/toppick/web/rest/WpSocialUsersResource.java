package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpSocialUsersService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpSocialUsersDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpSocialUsers.
 */
@RestController
@RequestMapping("/api")
public class WpSocialUsersResource {

    private final Logger log = LoggerFactory.getLogger(WpSocialUsersResource.class);

    private static final String ENTITY_NAME = "wpSocialUsers";

    private WpSocialUsersService wpSocialUsersService;

    public WpSocialUsersResource(WpSocialUsersService wpSocialUsersService) {
        this.wpSocialUsersService = wpSocialUsersService;
    }

    /**
     * POST  /wp-social-users : Create a new wpSocialUsers.
     *
     * @param wpSocialUsersDTO the wpSocialUsersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpSocialUsersDTO, or with status 400 (Bad Request) if the wpSocialUsers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-social-users")
    @Timed
    public ResponseEntity<WpSocialUsersDTO> createWpSocialUsers(@RequestBody WpSocialUsersDTO wpSocialUsersDTO) throws URISyntaxException {
        log.debug("REST request to save WpSocialUsers : {}", wpSocialUsersDTO);
        if (wpSocialUsersDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpSocialUsers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpSocialUsersDTO result = wpSocialUsersService.save(wpSocialUsersDTO);
        return ResponseEntity.created(new URI("/api/wp-social-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-social-users : Updates an existing wpSocialUsers.
     *
     * @param wpSocialUsersDTO the wpSocialUsersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpSocialUsersDTO,
     * or with status 400 (Bad Request) if the wpSocialUsersDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpSocialUsersDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-social-users")
    @Timed
    public ResponseEntity<WpSocialUsersDTO> updateWpSocialUsers(@RequestBody WpSocialUsersDTO wpSocialUsersDTO) throws URISyntaxException {
        log.debug("REST request to update WpSocialUsers : {}", wpSocialUsersDTO);
        if (wpSocialUsersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpSocialUsersDTO result = wpSocialUsersService.save(wpSocialUsersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpSocialUsersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-social-users : get all the wpSocialUsers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpSocialUsers in body
     */
    @GetMapping("/wp-social-users")
    @Timed
    public ResponseEntity<List<WpSocialUsersDTO>> getAllWpSocialUsers(Pageable pageable) {
        log.debug("REST request to get a page of WpSocialUsers");
        Page<WpSocialUsersDTO> page = wpSocialUsersService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-social-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-social-users/:id : get the "id" wpSocialUsers.
     *
     * @param id the id of the wpSocialUsersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpSocialUsersDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-social-users/{id}")
    @Timed
    public ResponseEntity<WpSocialUsersDTO> getWpSocialUsers(@PathVariable Long id) {
        log.debug("REST request to get WpSocialUsers : {}", id);
        Optional<WpSocialUsersDTO> wpSocialUsersDTO = wpSocialUsersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpSocialUsersDTO);
    }

    /**
     * DELETE  /wp-social-users/:id : delete the "id" wpSocialUsers.
     *
     * @param id the id of the wpSocialUsersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-social-users/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpSocialUsers(@PathVariable Long id) {
        log.debug("REST request to delete WpSocialUsers : {}", id);
        wpSocialUsersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-social-users?query=:query : search for the wpSocialUsers corresponding
     * to the query.
     *
     * @param query the query of the wpSocialUsers search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-social-users")
    @Timed
    public ResponseEntity<List<WpSocialUsersDTO>> searchWpSocialUsers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpSocialUsers for query {}", query);
        Page<WpSocialUsersDTO> page = wpSocialUsersService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-social-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
