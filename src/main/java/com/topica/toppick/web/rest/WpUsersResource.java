package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpUsersService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpUsersDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpUsers.
 */
@RestController
@RequestMapping("/api")
public class WpUsersResource {

    private final Logger log = LoggerFactory.getLogger(WpUsersResource.class);

    private static final String ENTITY_NAME = "wpUsers";

    private WpUsersService wpUsersService;

    public WpUsersResource(WpUsersService wpUsersService) {
        this.wpUsersService = wpUsersService;
    }

    /**
     * POST  /wp-users : Create a new wpUsers.
     *
     * @param wpUsersDTO the wpUsersDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpUsersDTO, or with status 400 (Bad Request) if the wpUsers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-users")
    @Timed
    public ResponseEntity<WpUsersDTO> createWpUsers(@RequestBody WpUsersDTO wpUsersDTO) throws URISyntaxException {
        log.debug("REST request to save WpUsers : {}", wpUsersDTO);
        if (wpUsersDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpUsers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpUsersDTO result = wpUsersService.save(wpUsersDTO);
        return ResponseEntity.created(new URI("/api/wp-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-users : Updates an existing wpUsers.
     *
     * @param wpUsersDTO the wpUsersDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpUsersDTO,
     * or with status 400 (Bad Request) if the wpUsersDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpUsersDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-users")
    @Timed
    public ResponseEntity<WpUsersDTO> updateWpUsers(@RequestBody WpUsersDTO wpUsersDTO) throws URISyntaxException {
        log.debug("REST request to update WpUsers : {}", wpUsersDTO);
        if (wpUsersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpUsersDTO result = wpUsersService.save(wpUsersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpUsersDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-users : get all the wpUsers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpUsers in body
     */
    @GetMapping("/wp-users")
    @Timed
    public ResponseEntity<List<WpUsersDTO>> getAllWpUsers(Pageable pageable) {
        log.debug("REST request to get a page of WpUsers");
        Page<WpUsersDTO> page = wpUsersService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-users/:id : get the "id" wpUsers.
     *
     * @param id the id of the wpUsersDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpUsersDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-users/{id}")
    @Timed
    public ResponseEntity<WpUsersDTO> getWpUsers(@PathVariable Long id) {
        log.debug("REST request to get WpUsers : {}", id);
        Optional<WpUsersDTO> wpUsersDTO = wpUsersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpUsersDTO);
    }

    /**
     * DELETE  /wp-users/:id : delete the "id" wpUsers.
     *
     * @param id the id of the wpUsersDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-users/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpUsers(@PathVariable Long id) {
        log.debug("REST request to delete WpUsers : {}", id);
        wpUsersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-users?query=:query : search for the wpUsers corresponding
     * to the query.
     *
     * @param query the query of the wpUsers search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-users")
    @Timed
    public ResponseEntity<List<WpUsersDTO>> searchWpUsers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpUsers for query {}", query);
        Page<WpUsersDTO> page = wpUsersService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
