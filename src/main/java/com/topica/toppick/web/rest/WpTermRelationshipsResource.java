package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpTermRelationshipsService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpTermRelationshipsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpTermRelationships.
 */
@RestController
@RequestMapping("/api")
public class WpTermRelationshipsResource {

    private final Logger log = LoggerFactory.getLogger(WpTermRelationshipsResource.class);

    private static final String ENTITY_NAME = "wpTermRelationships";

    private WpTermRelationshipsService wpTermRelationshipsService;

    public WpTermRelationshipsResource(WpTermRelationshipsService wpTermRelationshipsService) {
        this.wpTermRelationshipsService = wpTermRelationshipsService;
    }

    /**
     * POST  /wp-term-relationships : Create a new wpTermRelationships.
     *
     * @param wpTermRelationshipsDTO the wpTermRelationshipsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpTermRelationshipsDTO, or with status 400 (Bad Request) if the wpTermRelationships has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-term-relationships")
    @Timed
    public ResponseEntity<WpTermRelationshipsDTO> createWpTermRelationships(@RequestBody WpTermRelationshipsDTO wpTermRelationshipsDTO) throws URISyntaxException {
        log.debug("REST request to save WpTermRelationships : {}", wpTermRelationshipsDTO);
        if (wpTermRelationshipsDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpTermRelationships cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpTermRelationshipsDTO result = wpTermRelationshipsService.save(wpTermRelationshipsDTO);
        return ResponseEntity.created(new URI("/api/wp-term-relationships/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-term-relationships : Updates an existing wpTermRelationships.
     *
     * @param wpTermRelationshipsDTO the wpTermRelationshipsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpTermRelationshipsDTO,
     * or with status 400 (Bad Request) if the wpTermRelationshipsDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpTermRelationshipsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-term-relationships")
    @Timed
    public ResponseEntity<WpTermRelationshipsDTO> updateWpTermRelationships(@RequestBody WpTermRelationshipsDTO wpTermRelationshipsDTO) throws URISyntaxException {
        log.debug("REST request to update WpTermRelationships : {}", wpTermRelationshipsDTO);
        if (wpTermRelationshipsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpTermRelationshipsDTO result = wpTermRelationshipsService.save(wpTermRelationshipsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpTermRelationshipsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-term-relationships : get all the wpTermRelationships.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpTermRelationships in body
     */
    @GetMapping("/wp-term-relationships")
    @Timed
    public ResponseEntity<List<WpTermRelationshipsDTO>> getAllWpTermRelationships(Pageable pageable) {
        log.debug("REST request to get a page of WpTermRelationships");
        Page<WpTermRelationshipsDTO> page = wpTermRelationshipsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-term-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-term-relationships/:id : get the "id" wpTermRelationships.
     *
     * @param id the id of the wpTermRelationshipsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpTermRelationshipsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-term-relationships/{id}")
    @Timed
    public ResponseEntity<WpTermRelationshipsDTO> getWpTermRelationships(@PathVariable Long id) {
        log.debug("REST request to get WpTermRelationships : {}", id);
        Optional<WpTermRelationshipsDTO> wpTermRelationshipsDTO = wpTermRelationshipsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpTermRelationshipsDTO);
    }

    /**
     * DELETE  /wp-term-relationships/:id : delete the "id" wpTermRelationships.
     *
     * @param id the id of the wpTermRelationshipsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-term-relationships/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpTermRelationships(@PathVariable Long id) {
        log.debug("REST request to delete WpTermRelationships : {}", id);
        wpTermRelationshipsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-term-relationships?query=:query : search for the wpTermRelationships corresponding
     * to the query.
     *
     * @param query the query of the wpTermRelationships search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-term-relationships")
    @Timed
    public ResponseEntity<List<WpTermRelationshipsDTO>> searchWpTermRelationships(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpTermRelationships for query {}", query);
        Page<WpTermRelationshipsDTO> page = wpTermRelationshipsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-term-relationships");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
