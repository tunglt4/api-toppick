package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpPopularpostssummaryService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpPopularpostssummaryDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpPopularpostssummary.
 */
@RestController
@RequestMapping("/api")
public class WpPopularpostssummaryResource {

    private final Logger log = LoggerFactory.getLogger(WpPopularpostssummaryResource.class);

    private static final String ENTITY_NAME = "wpPopularpostssummary";

    private WpPopularpostssummaryService wpPopularpostssummaryService;

    public WpPopularpostssummaryResource(WpPopularpostssummaryService wpPopularpostssummaryService) {
        this.wpPopularpostssummaryService = wpPopularpostssummaryService;
    }

    /**
     * POST  /wp-popularpostssummaries : Create a new wpPopularpostssummary.
     *
     * @param wpPopularpostssummaryDTO the wpPopularpostssummaryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpPopularpostssummaryDTO, or with status 400 (Bad Request) if the wpPopularpostssummary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-popularpostssummaries")
    @Timed
    public ResponseEntity<WpPopularpostssummaryDTO> createWpPopularpostssummary(@RequestBody WpPopularpostssummaryDTO wpPopularpostssummaryDTO) throws URISyntaxException {
        log.debug("REST request to save WpPopularpostssummary : {}", wpPopularpostssummaryDTO);
        if (wpPopularpostssummaryDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpPopularpostssummary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpPopularpostssummaryDTO result = wpPopularpostssummaryService.save(wpPopularpostssummaryDTO);
        return ResponseEntity.created(new URI("/api/wp-popularpostssummaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-popularpostssummaries : Updates an existing wpPopularpostssummary.
     *
     * @param wpPopularpostssummaryDTO the wpPopularpostssummaryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpPopularpostssummaryDTO,
     * or with status 400 (Bad Request) if the wpPopularpostssummaryDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpPopularpostssummaryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-popularpostssummaries")
    @Timed
    public ResponseEntity<WpPopularpostssummaryDTO> updateWpPopularpostssummary(@RequestBody WpPopularpostssummaryDTO wpPopularpostssummaryDTO) throws URISyntaxException {
        log.debug("REST request to update WpPopularpostssummary : {}", wpPopularpostssummaryDTO);
        if (wpPopularpostssummaryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpPopularpostssummaryDTO result = wpPopularpostssummaryService.save(wpPopularpostssummaryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpPopularpostssummaryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-popularpostssummaries : get all the wpPopularpostssummaries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpPopularpostssummaries in body
     */
    @GetMapping("/wp-popularpostssummaries")
    @Timed
    public ResponseEntity<List<WpPopularpostssummaryDTO>> getAllWpPopularpostssummaries(Pageable pageable) {
        log.debug("REST request to get a page of WpPopularpostssummaries");
        Page<WpPopularpostssummaryDTO> page = wpPopularpostssummaryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-popularpostssummaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-popularpostssummaries/:id : get the "id" wpPopularpostssummary.
     *
     * @param id the id of the wpPopularpostssummaryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpPopularpostssummaryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-popularpostssummaries/{id}")
    @Timed
    public ResponseEntity<WpPopularpostssummaryDTO> getWpPopularpostssummary(@PathVariable Long id) {
        log.debug("REST request to get WpPopularpostssummary : {}", id);
        Optional<WpPopularpostssummaryDTO> wpPopularpostssummaryDTO = wpPopularpostssummaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpPopularpostssummaryDTO);
    }

    /**
     * DELETE  /wp-popularpostssummaries/:id : delete the "id" wpPopularpostssummary.
     *
     * @param id the id of the wpPopularpostssummaryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-popularpostssummaries/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpPopularpostssummary(@PathVariable Long id) {
        log.debug("REST request to delete WpPopularpostssummary : {}", id);
        wpPopularpostssummaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-popularpostssummaries?query=:query : search for the wpPopularpostssummary corresponding
     * to the query.
     *
     * @param query the query of the wpPopularpostssummary search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-popularpostssummaries")
    @Timed
    public ResponseEntity<List<WpPopularpostssummaryDTO>> searchWpPopularpostssummaries(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpPopularpostssummaries for query {}", query);
        Page<WpPopularpostssummaryDTO> page = wpPopularpostssummaryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-popularpostssummaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
