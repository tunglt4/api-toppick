package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpPopularpostsdataService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpPopularpostsdataDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpPopularpostsdata.
 */
@RestController
@RequestMapping("/api")
public class WpPopularpostsdataResource {

    private final Logger log = LoggerFactory.getLogger(WpPopularpostsdataResource.class);

    private static final String ENTITY_NAME = "wpPopularpostsdata";

    private WpPopularpostsdataService wpPopularpostsdataService;

    public WpPopularpostsdataResource(WpPopularpostsdataService wpPopularpostsdataService) {
        this.wpPopularpostsdataService = wpPopularpostsdataService;
    }

    /**
     * POST  /wp-popularpostsdata : Create a new wpPopularpostsdata.
     *
     * @param wpPopularpostsdataDTO the wpPopularpostsdataDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpPopularpostsdataDTO, or with status 400 (Bad Request) if the wpPopularpostsdata has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-popularpostsdata")
    @Timed
    public ResponseEntity<WpPopularpostsdataDTO> createWpPopularpostsdata(@RequestBody WpPopularpostsdataDTO wpPopularpostsdataDTO) throws URISyntaxException {
        log.debug("REST request to save WpPopularpostsdata : {}", wpPopularpostsdataDTO);
        if (wpPopularpostsdataDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpPopularpostsdata cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpPopularpostsdataDTO result = wpPopularpostsdataService.save(wpPopularpostsdataDTO);
        return ResponseEntity.created(new URI("/api/wp-popularpostsdata/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-popularpostsdata : Updates an existing wpPopularpostsdata.
     *
     * @param wpPopularpostsdataDTO the wpPopularpostsdataDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpPopularpostsdataDTO,
     * or with status 400 (Bad Request) if the wpPopularpostsdataDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpPopularpostsdataDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-popularpostsdata")
    @Timed
    public ResponseEntity<WpPopularpostsdataDTO> updateWpPopularpostsdata(@RequestBody WpPopularpostsdataDTO wpPopularpostsdataDTO) throws URISyntaxException {
        log.debug("REST request to update WpPopularpostsdata : {}", wpPopularpostsdataDTO);
        if (wpPopularpostsdataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpPopularpostsdataDTO result = wpPopularpostsdataService.save(wpPopularpostsdataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpPopularpostsdataDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-popularpostsdata : get all the wpPopularpostsdata.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpPopularpostsdata in body
     */
    @GetMapping("/wp-popularpostsdata")
    @Timed
    public ResponseEntity<List<WpPopularpostsdataDTO>> getAllWpPopularpostsdata(Pageable pageable) {
        log.debug("REST request to get a page of WpPopularpostsdata");
        Page<WpPopularpostsdataDTO> page = wpPopularpostsdataService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-popularpostsdata");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-popularpostsdata/:id : get the "id" wpPopularpostsdata.
     *
     * @param id the id of the wpPopularpostsdataDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpPopularpostsdataDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-popularpostsdata/{id}")
    @Timed
    public ResponseEntity<WpPopularpostsdataDTO> getWpPopularpostsdata(@PathVariable Long id) {
        log.debug("REST request to get WpPopularpostsdata : {}", id);
        Optional<WpPopularpostsdataDTO> wpPopularpostsdataDTO = wpPopularpostsdataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpPopularpostsdataDTO);
    }

    /**
     * DELETE  /wp-popularpostsdata/:id : delete the "id" wpPopularpostsdata.
     *
     * @param id the id of the wpPopularpostsdataDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-popularpostsdata/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpPopularpostsdata(@PathVariable Long id) {
        log.debug("REST request to delete WpPopularpostsdata : {}", id);
        wpPopularpostsdataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-popularpostsdata?query=:query : search for the wpPopularpostsdata corresponding
     * to the query.
     *
     * @param query the query of the wpPopularpostsdata search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-popularpostsdata")
    @Timed
    public ResponseEntity<List<WpPopularpostsdataDTO>> searchWpPopularpostsdata(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpPopularpostsdata for query {}", query);
        Page<WpPopularpostsdataDTO> page = wpPopularpostsdataService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-popularpostsdata");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
