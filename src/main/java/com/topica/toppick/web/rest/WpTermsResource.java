package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpTermsService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpTermsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpTerms.
 */
@RestController
@RequestMapping("/api")
public class WpTermsResource {

    private final Logger log = LoggerFactory.getLogger(WpTermsResource.class);

    private static final String ENTITY_NAME = "wpTerms";

    private WpTermsService wpTermsService;

    public WpTermsResource(WpTermsService wpTermsService) {
        this.wpTermsService = wpTermsService;
    }

    /**
     * POST  /wp-terms : Create a new wpTerms.
     *
     * @param wpTermsDTO the wpTermsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpTermsDTO, or with status 400 (Bad Request) if the wpTerms has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-terms")
    @Timed
    public ResponseEntity<WpTermsDTO> createWpTerms(@RequestBody WpTermsDTO wpTermsDTO) throws URISyntaxException {
        log.debug("REST request to save WpTerms : {}", wpTermsDTO);
        if (wpTermsDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpTerms cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpTermsDTO result = wpTermsService.save(wpTermsDTO);
        return ResponseEntity.created(new URI("/api/wp-terms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-terms : Updates an existing wpTerms.
     *
     * @param wpTermsDTO the wpTermsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpTermsDTO,
     * or with status 400 (Bad Request) if the wpTermsDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpTermsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-terms")
    @Timed
    public ResponseEntity<WpTermsDTO> updateWpTerms(@RequestBody WpTermsDTO wpTermsDTO) throws URISyntaxException {
        log.debug("REST request to update WpTerms : {}", wpTermsDTO);
        if (wpTermsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpTermsDTO result = wpTermsService.save(wpTermsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpTermsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-terms : get all the wpTerms.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpTerms in body
     */
    @GetMapping("/wp-terms")
    @Timed
    public ResponseEntity<List<WpTermsDTO>> getAllWpTerms(Pageable pageable) {
        log.debug("REST request to get a page of WpTerms");
        Page<WpTermsDTO> page = wpTermsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-terms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-terms/:id : get the "id" wpTerms.
     *
     * @param id the id of the wpTermsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpTermsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-terms/{id}")
    @Timed
    public ResponseEntity<WpTermsDTO> getWpTerms(@PathVariable Long id) {
        log.debug("REST request to get WpTerms : {}", id);
        Optional<WpTermsDTO> wpTermsDTO = wpTermsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpTermsDTO);
    }

    /**
     * DELETE  /wp-terms/:id : delete the "id" wpTerms.
     *
     * @param id the id of the wpTermsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-terms/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpTerms(@PathVariable Long id) {
        log.debug("REST request to delete WpTerms : {}", id);
        wpTermsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-terms?query=:query : search for the wpTerms corresponding
     * to the query.
     *
     * @param query the query of the wpTerms search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-terms")
    @Timed
    public ResponseEntity<List<WpTermsDTO>> searchWpTerms(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpTerms for query {}", query);
        Page<WpTermsDTO> page = wpTermsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-terms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
