package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpUsermetaService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpUsermetaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpUsermeta.
 */
@RestController
@RequestMapping("/api")
public class WpUsermetaResource {

    private final Logger log = LoggerFactory.getLogger(WpUsermetaResource.class);

    private static final String ENTITY_NAME = "wpUsermeta";

    private WpUsermetaService wpUsermetaService;

    public WpUsermetaResource(WpUsermetaService wpUsermetaService) {
        this.wpUsermetaService = wpUsermetaService;
    }

    /**
     * POST  /wp-usermetas : Create a new wpUsermeta.
     *
     * @param wpUsermetaDTO the wpUsermetaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpUsermetaDTO, or with status 400 (Bad Request) if the wpUsermeta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-usermetas")
    @Timed
    public ResponseEntity<WpUsermetaDTO> createWpUsermeta(@RequestBody WpUsermetaDTO wpUsermetaDTO) throws URISyntaxException {
        log.debug("REST request to save WpUsermeta : {}", wpUsermetaDTO);
        if (wpUsermetaDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpUsermeta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpUsermetaDTO result = wpUsermetaService.save(wpUsermetaDTO);
        return ResponseEntity.created(new URI("/api/wp-usermetas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-usermetas : Updates an existing wpUsermeta.
     *
     * @param wpUsermetaDTO the wpUsermetaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpUsermetaDTO,
     * or with status 400 (Bad Request) if the wpUsermetaDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpUsermetaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-usermetas")
    @Timed
    public ResponseEntity<WpUsermetaDTO> updateWpUsermeta(@RequestBody WpUsermetaDTO wpUsermetaDTO) throws URISyntaxException {
        log.debug("REST request to update WpUsermeta : {}", wpUsermetaDTO);
        if (wpUsermetaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpUsermetaDTO result = wpUsermetaService.save(wpUsermetaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpUsermetaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-usermetas : get all the wpUsermetas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpUsermetas in body
     */
    @GetMapping("/wp-usermetas")
    @Timed
    public ResponseEntity<List<WpUsermetaDTO>> getAllWpUsermetas(Pageable pageable) {
        log.debug("REST request to get a page of WpUsermetas");
        Page<WpUsermetaDTO> page = wpUsermetaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-usermetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-usermetas/:id : get the "id" wpUsermeta.
     *
     * @param id the id of the wpUsermetaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpUsermetaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-usermetas/{id}")
    @Timed
    public ResponseEntity<WpUsermetaDTO> getWpUsermeta(@PathVariable Long id) {
        log.debug("REST request to get WpUsermeta : {}", id);
        Optional<WpUsermetaDTO> wpUsermetaDTO = wpUsermetaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpUsermetaDTO);
    }

    /**
     * DELETE  /wp-usermetas/:id : delete the "id" wpUsermeta.
     *
     * @param id the id of the wpUsermetaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-usermetas/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpUsermeta(@PathVariable Long id) {
        log.debug("REST request to delete WpUsermeta : {}", id);
        wpUsermetaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-usermetas?query=:query : search for the wpUsermeta corresponding
     * to the query.
     *
     * @param query the query of the wpUsermeta search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-usermetas")
    @Timed
    public ResponseEntity<List<WpUsermetaDTO>> searchWpUsermetas(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpUsermetas for query {}", query);
        Page<WpUsermetaDTO> page = wpUsermetaService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-usermetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
