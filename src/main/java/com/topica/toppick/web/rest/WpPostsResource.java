package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpPostsService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpPostsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpPosts.
 */
@RestController
@RequestMapping("/api")
public class WpPostsResource {

    private final Logger log = LoggerFactory.getLogger(WpPostsResource.class);

    private static final String ENTITY_NAME = "wpPosts";

    private WpPostsService wpPostsService;

    public WpPostsResource(WpPostsService wpPostsService) {
        this.wpPostsService = wpPostsService;
    }

    /**
     * POST  /wp-posts : Create a new wpPosts.
     *
     * @param wpPostsDTO the wpPostsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpPostsDTO, or with status 400 (Bad Request) if the wpPosts has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-posts")
    @Timed
    public ResponseEntity<WpPostsDTO> createWpPosts(@RequestBody WpPostsDTO wpPostsDTO) throws URISyntaxException {
        log.debug("REST request to save WpPosts : {}", wpPostsDTO);
        if (wpPostsDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpPosts cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpPostsDTO result = wpPostsService.save(wpPostsDTO);
        return ResponseEntity.created(new URI("/api/wp-posts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-posts : Updates an existing wpPosts.
     *
     * @param wpPostsDTO the wpPostsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpPostsDTO,
     * or with status 400 (Bad Request) if the wpPostsDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpPostsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-posts")
    @Timed
    public ResponseEntity<WpPostsDTO> updateWpPosts(@RequestBody WpPostsDTO wpPostsDTO) throws URISyntaxException {
        log.debug("REST request to update WpPosts : {}", wpPostsDTO);
        if (wpPostsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpPostsDTO result = wpPostsService.save(wpPostsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpPostsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-posts : get all the wpPosts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpPosts in body
     */
    @GetMapping("/wp-posts")
    @Timed
    public ResponseEntity<List<WpPostsDTO>> getAllWpPosts(Pageable pageable) {
        log.debug("REST request to get a page of WpPosts");
        Page<WpPostsDTO> page = wpPostsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-posts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-posts/:id : get the "id" wpPosts.
     *
     * @param id the id of the wpPostsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpPostsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-posts/{id}")
    @Timed
    public ResponseEntity<WpPostsDTO> getWpPosts(@PathVariable Long id) {
        log.debug("REST request to get WpPosts : {}", id);
        Optional<WpPostsDTO> wpPostsDTO = wpPostsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpPostsDTO);
    }

    /**
     * DELETE  /wp-posts/:id : delete the "id" wpPosts.
     *
     * @param id the id of the wpPostsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-posts/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpPosts(@PathVariable Long id) {
        log.debug("REST request to delete WpPosts : {}", id);
        wpPostsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-posts?query=:query : search for the wpPosts corresponding
     * to the query.
     *
     * @param query the query of the wpPosts search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-posts")
    @Timed
    public ResponseEntity<List<WpPostsDTO>> searchWpPosts(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpPosts for query {}", query);
        Page<WpPostsDTO> page = wpPostsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-posts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
