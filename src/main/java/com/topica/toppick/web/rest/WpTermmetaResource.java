package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpTermmetaService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpTermmetaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpTermmeta.
 */
@RestController
@RequestMapping("/api")
public class WpTermmetaResource {

    private final Logger log = LoggerFactory.getLogger(WpTermmetaResource.class);

    private static final String ENTITY_NAME = "wpTermmeta";

    private WpTermmetaService wpTermmetaService;

    public WpTermmetaResource(WpTermmetaService wpTermmetaService) {
        this.wpTermmetaService = wpTermmetaService;
    }

    /**
     * POST  /wp-termmetas : Create a new wpTermmeta.
     *
     * @param wpTermmetaDTO the wpTermmetaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpTermmetaDTO, or with status 400 (Bad Request) if the wpTermmeta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-termmetas")
    @Timed
    public ResponseEntity<WpTermmetaDTO> createWpTermmeta(@RequestBody WpTermmetaDTO wpTermmetaDTO) throws URISyntaxException {
        log.debug("REST request to save WpTermmeta : {}", wpTermmetaDTO);
        if (wpTermmetaDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpTermmeta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpTermmetaDTO result = wpTermmetaService.save(wpTermmetaDTO);
        return ResponseEntity.created(new URI("/api/wp-termmetas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-termmetas : Updates an existing wpTermmeta.
     *
     * @param wpTermmetaDTO the wpTermmetaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpTermmetaDTO,
     * or with status 400 (Bad Request) if the wpTermmetaDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpTermmetaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-termmetas")
    @Timed
    public ResponseEntity<WpTermmetaDTO> updateWpTermmeta(@RequestBody WpTermmetaDTO wpTermmetaDTO) throws URISyntaxException {
        log.debug("REST request to update WpTermmeta : {}", wpTermmetaDTO);
        if (wpTermmetaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpTermmetaDTO result = wpTermmetaService.save(wpTermmetaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpTermmetaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-termmetas : get all the wpTermmetas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpTermmetas in body
     */
    @GetMapping("/wp-termmetas")
    @Timed
    public ResponseEntity<List<WpTermmetaDTO>> getAllWpTermmetas(Pageable pageable) {
        log.debug("REST request to get a page of WpTermmetas");
        Page<WpTermmetaDTO> page = wpTermmetaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-termmetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-termmetas/:id : get the "id" wpTermmeta.
     *
     * @param id the id of the wpTermmetaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpTermmetaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-termmetas/{id}")
    @Timed
    public ResponseEntity<WpTermmetaDTO> getWpTermmeta(@PathVariable Long id) {
        log.debug("REST request to get WpTermmeta : {}", id);
        Optional<WpTermmetaDTO> wpTermmetaDTO = wpTermmetaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpTermmetaDTO);
    }

    /**
     * DELETE  /wp-termmetas/:id : delete the "id" wpTermmeta.
     *
     * @param id the id of the wpTermmetaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-termmetas/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpTermmeta(@PathVariable Long id) {
        log.debug("REST request to delete WpTermmeta : {}", id);
        wpTermmetaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-termmetas?query=:query : search for the wpTermmeta corresponding
     * to the query.
     *
     * @param query the query of the wpTermmeta search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-termmetas")
    @Timed
    public ResponseEntity<List<WpTermmetaDTO>> searchWpTermmetas(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpTermmetas for query {}", query);
        Page<WpTermmetaDTO> page = wpTermmetaService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-termmetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
