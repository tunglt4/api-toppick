package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpOptionsService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpOptionsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpOptions.
 */
@RestController
@RequestMapping("/api")
public class WpOptionsResource {

    private final Logger log = LoggerFactory.getLogger(WpOptionsResource.class);

    private static final String ENTITY_NAME = "wpOptions";

    private WpOptionsService wpOptionsService;

    public WpOptionsResource(WpOptionsService wpOptionsService) {
        this.wpOptionsService = wpOptionsService;
    }

    /**
     * POST  /wp-options : Create a new wpOptions.
     *
     * @param wpOptionsDTO the wpOptionsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpOptionsDTO, or with status 400 (Bad Request) if the wpOptions has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-options")
    @Timed
    public ResponseEntity<WpOptionsDTO> createWpOptions(@RequestBody WpOptionsDTO wpOptionsDTO) throws URISyntaxException {
        log.debug("REST request to save WpOptions : {}", wpOptionsDTO);
        if (wpOptionsDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpOptions cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpOptionsDTO result = wpOptionsService.save(wpOptionsDTO);
        return ResponseEntity.created(new URI("/api/wp-options/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-options : Updates an existing wpOptions.
     *
     * @param wpOptionsDTO the wpOptionsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpOptionsDTO,
     * or with status 400 (Bad Request) if the wpOptionsDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpOptionsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-options")
    @Timed
    public ResponseEntity<WpOptionsDTO> updateWpOptions(@RequestBody WpOptionsDTO wpOptionsDTO) throws URISyntaxException {
        log.debug("REST request to update WpOptions : {}", wpOptionsDTO);
        if (wpOptionsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpOptionsDTO result = wpOptionsService.save(wpOptionsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpOptionsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-options : get all the wpOptions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpOptions in body
     */
    @GetMapping("/wp-options")
    @Timed
    public ResponseEntity<List<WpOptionsDTO>> getAllWpOptions(Pageable pageable) {
        log.debug("REST request to get a page of WpOptions");
        Page<WpOptionsDTO> page = wpOptionsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-options");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-options/:id : get the "id" wpOptions.
     *
     * @param id the id of the wpOptionsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpOptionsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-options/{id}")
    @Timed
    public ResponseEntity<WpOptionsDTO> getWpOptions(@PathVariable Long id) {
        log.debug("REST request to get WpOptions : {}", id);
        Optional<WpOptionsDTO> wpOptionsDTO = wpOptionsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpOptionsDTO);
    }

    /**
     * DELETE  /wp-options/:id : delete the "id" wpOptions.
     *
     * @param id the id of the wpOptionsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-options/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpOptions(@PathVariable Long id) {
        log.debug("REST request to delete WpOptions : {}", id);
        wpOptionsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-options?query=:query : search for the wpOptions corresponding
     * to the query.
     *
     * @param query the query of the wpOptions search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-options")
    @Timed
    public ResponseEntity<List<WpOptionsDTO>> searchWpOptions(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpOptions for query {}", query);
        Page<WpOptionsDTO> page = wpOptionsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-options");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
