package com.topica.toppick.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.social.support.URIBuilder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.view.RedirectView;

import com.topica.toppick.config.Constants;
import com.topica.toppick.domain.User;
import com.topica.toppick.service.SocialService;

@RestController
@RequestMapping("/social")
public class SocialController {

	private final Logger log = LoggerFactory.getLogger(SocialController.class);

	private final SocialService socialService;

	private final ProviderSignInUtils providerSignInUtils;

	private final SignInAdapter signInAdapter;

	public SocialController(SocialService socialService, ProviderSignInUtils providerSignInUtils,
			SignInAdapter signInAdapter) {
		this.socialService = socialService;
		this.providerSignInUtils = providerSignInUtils;
		this.signInAdapter = signInAdapter;
	}

	@GetMapping("/signup")
	public RedirectView signUp(WebRequest webRequest,
			@CookieValue(name = "NG_TRANSLATE_LANG_KEY", required = false, defaultValue = Constants.DEFAULT_LANGUAGE) String langKey) {
		try {
			Connection<?> connection = providerSignInUtils.getConnectionFromSession(webRequest);
			User user = socialService.createSocialUser(connection, langKey.replace("\"", ""));

			String originalUrl = signInAdapter.signIn(user.getLogin(), connection, (NativeWebRequest) webRequest);

			return new RedirectView(originalUrl, true);
		} catch (Exception e) {
			log.error("Exception creating social user: ", e);
			return new RedirectView(URIBuilder.fromUri("/signin").queryParam("error", "provider").build().toString());
		}
	}
}
