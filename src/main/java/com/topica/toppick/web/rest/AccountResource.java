package com.topica.toppick.web.rest;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.domain.User;
import com.topica.toppick.repository.UserRepository;
import com.topica.toppick.security.SecurityUtils;
import com.topica.toppick.service.MailService;
import com.topica.toppick.service.UserService;
import com.topica.toppick.service.dto.UserDTO;
import com.topica.toppick.service.dto.userresponse.UserCurrentInfoDTO;
import com.topica.toppick.web.rest.errors.EmailAlreadyUsedException;
import com.topica.toppick.web.rest.errors.EmailNotFoundException;
import com.topica.toppick.web.rest.errors.InternalServerErrorException;
import com.topica.toppick.web.rest.errors.InvalidPasswordException;
import com.topica.toppick.web.rest.errors.LoginAlreadyUsedException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.vm.CurrentInfoVM;
import com.topica.toppick.web.rest.vm.KeyAndPasswordVM;
import com.topica.toppick.web.rest.vm.ManagedUserVM;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

	private final Logger log = LoggerFactory.getLogger(AccountResource.class);

	private final UserRepository userRepository;

	private final UserService userService;

	private final MailService mailService;

	public AccountResource(UserRepository userRepository, UserService userService, MailService mailService) {

		this.userRepository = userRepository;
		this.userService = userService;
		this.mailService = mailService;
	}

	/**
	 * POST /register : register the user.
	 *
	 * @param managedUserVM
	 *            the managed user View Model
	 * @throws InvalidPasswordException
	 *             400 (Bad Request) if the password is incorrect
	 * @throws EmailAlreadyUsedException
	 *             400 (Bad Request) if the email is already used
	 * @throws LoginAlreadyUsedException
	 *             400 (Bad Request) if the login is already used
	 */
	@PostMapping("/register")
	@Timed
	@ResponseStatus(HttpStatus.CREATED)
	public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
		if (!checkPasswordLength(managedUserVM.getPassword())) {
			throw new InvalidPasswordException();
		}
		userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase()).ifPresent(u -> {
			throw new LoginAlreadyUsedException();
		});
		userRepository.findOneByEmailIgnoreCase(managedUserVM.getEmail()).ifPresent(u -> {
			throw new EmailAlreadyUsedException();
		});
		User user = userService.registerUser(managedUserVM, managedUserVM.getPassword());
		mailService.sendActivationEmail(user);
	}

	/**
	 * GET /activate : activate the registered user.
	 *
	 * @param key
	 *            the activation key
	 * @throws RuntimeException
	 *             500 (Internal Server Error) if the user couldn't be activated
	 */
	@GetMapping("/activate")
	@Timed
	public void activateAccount(@RequestParam(value = "key") String key) {
		Optional<User> user = userService.activateRegistration(key);
		if (!user.isPresent()) {
			throw new InternalServerErrorException("No user was found for this reset key");
		}
	}

	/**
	 * GET /authenticate : check if the user is authenticated, and return its login.
	 *
	 * @param request
	 *            the HTTP request
	 * @return the login if the user is authenticated
	 */
	@GetMapping("/authenticate")
	@Timed
	public String isAuthenticated(HttpServletRequest request) {
		log.debug("REST request to check if the current user is authenticated");
		return request.getRemoteUser();
	}

	/**
	 * GET /account : get the current user.
	 *
	 * @return the current user
	 * @throws RuntimeException
	 *             500 (Internal Server Error) if the user couldn't be returned
	 */
	@GetMapping("/account")
	@Timed
	public UserDTO getAccount() {
		return userService.getUserWithAuthorities().map(UserDTO::new)
				.orElseThrow(() -> new InternalServerErrorException("User could not be found"));
	}

	/**
	 * GET /account : get the current user.
	 *
	 * @return the current user
	 * @throws RuntimeException
	 *             500 (Internal Server Error) if the user couldn't be returned
	 */
	@GetMapping("/currentinfo")
	@Timed
	public UserCurrentInfoDTO getCurrentInfo() {
		return userService.getCurrentInfo();
	}

	/**
	 * POST /account/change-info : changes the current user's info
	 *
	 * @param password
	 *            the new password
	 * @throws InvalidPasswordException
	 *             400 (Bad Request) if the new password is incorrect
	 */
	@PutMapping(path = "/account/change-info")
	@Timed
	public ResponseEntity<Void> updateCurrentInfo(@RequestBody CurrentInfoVM info) {
		final String userLogin = SecurityUtils.getCurrentUserLogin()
				.orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
		userService.updateCurrentInfo(info, userLogin);
		return ResponseEntity.ok().headers(HeaderUtil.createAlert("User information is updated with identifier ", userLogin))
				.build();
	}

	/**
	 * POST /account : update the current user information.
	 *
	 * @param userDTO
	 *            the current user information
	 * @throws EmailAlreadyUsedException
	 *             400 (Bad Request) if the email is already used
	 * @throws RuntimeException
	 *             500 (Internal Server Error) if the user login wasn't found
	 */
	@PostMapping("/account")
	@Timed
	public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
		final String userLogin = SecurityUtils.getCurrentUserLogin()
				.orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
		Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
		if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
			throw new EmailAlreadyUsedException();
		}
		Optional<User> user = userRepository.findOneByLogin(userLogin);
		if (!user.isPresent()) {
			throw new InternalServerErrorException("User could not be found");
		}
		userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(), userDTO.getLangKey(),
				userDTO.getImageUrl());
	}

	/**
	 * POST /account/change-password : changes the current user's password
	 *
	 * @param password
	 *            the new password
	 * @throws InvalidPasswordException
	 *             400 (Bad Request) if the new password is incorrect
	 */
	@PostMapping(path = "/account/change-password")
	@Timed
	public void changePassword(@RequestBody String password) {
		if (!checkPasswordLength(password)) {
			throw new InvalidPasswordException();
		}
		userService.changePassword(password);
	}

	/**
	 * POST /account/reset-password/init : Send an email to reset the password of
	 * the user
	 *
	 * @param mail
	 *            the mail of the user
	 * @throws EmailNotFoundException
	 *             400 (Bad Request) if the email address is not registered
	 */
	@PostMapping(path = "/account/reset-password/init")
	@Timed
	public void requestPasswordReset(@RequestBody String mail) {
		mailService
				.sendPasswordResetMail(userService.requestPasswordReset(mail).orElseThrow(EmailNotFoundException::new));
	}

	/**
	 * POST /account/reset-password/finish : Finish to reset the password of the
	 * user
	 *
	 * @param keyAndPassword
	 *            the generated key and the new password
	 * @throws InvalidPasswordException
	 *             400 (Bad Request) if the password is incorrect
	 * @throws RuntimeException
	 *             500 (Internal Server Error) if the password could not be reset
	 */
	@PostMapping(path = "/account/reset-password/finish")
	@Timed
	public void finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
		if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
			throw new InvalidPasswordException();
		}
		Optional<User> user = userService.completePasswordReset(keyAndPassword.getNewPassword(),
				keyAndPassword.getKey());

		if (!user.isPresent()) {
			throw new InternalServerErrorException("No user was found for this reset key");
		}
	}

	private static boolean checkPasswordLength(String password) {
		return !StringUtils.isEmpty(password) && password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH
				&& password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
	}
}
