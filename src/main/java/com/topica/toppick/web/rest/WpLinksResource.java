package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpLinksService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpLinksDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpLinks.
 */
@RestController
@RequestMapping("/api")
public class WpLinksResource {

    private final Logger log = LoggerFactory.getLogger(WpLinksResource.class);

    private static final String ENTITY_NAME = "wpLinks";

    private WpLinksService wpLinksService;

    public WpLinksResource(WpLinksService wpLinksService) {
        this.wpLinksService = wpLinksService;
    }

    /**
     * POST  /wp-links : Create a new wpLinks.
     *
     * @param wpLinksDTO the wpLinksDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpLinksDTO, or with status 400 (Bad Request) if the wpLinks has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-links")
    @Timed
    public ResponseEntity<WpLinksDTO> createWpLinks(@RequestBody WpLinksDTO wpLinksDTO) throws URISyntaxException {
        log.debug("REST request to save WpLinks : {}", wpLinksDTO);
        if (wpLinksDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpLinks cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpLinksDTO result = wpLinksService.save(wpLinksDTO);
        return ResponseEntity.created(new URI("/api/wp-links/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-links : Updates an existing wpLinks.
     *
     * @param wpLinksDTO the wpLinksDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpLinksDTO,
     * or with status 400 (Bad Request) if the wpLinksDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpLinksDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-links")
    @Timed
    public ResponseEntity<WpLinksDTO> updateWpLinks(@RequestBody WpLinksDTO wpLinksDTO) throws URISyntaxException {
        log.debug("REST request to update WpLinks : {}", wpLinksDTO);
        if (wpLinksDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpLinksDTO result = wpLinksService.save(wpLinksDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpLinksDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-links : get all the wpLinks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpLinks in body
     */
    @GetMapping("/wp-links")
    @Timed
    public ResponseEntity<List<WpLinksDTO>> getAllWpLinks(Pageable pageable) {
        log.debug("REST request to get a page of WpLinks");
        Page<WpLinksDTO> page = wpLinksService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-links");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-links/:id : get the "id" wpLinks.
     *
     * @param id the id of the wpLinksDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpLinksDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-links/{id}")
    @Timed
    public ResponseEntity<WpLinksDTO> getWpLinks(@PathVariable Long id) {
        log.debug("REST request to get WpLinks : {}", id);
        Optional<WpLinksDTO> wpLinksDTO = wpLinksService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpLinksDTO);
    }

    /**
     * DELETE  /wp-links/:id : delete the "id" wpLinks.
     *
     * @param id the id of the wpLinksDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-links/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpLinks(@PathVariable Long id) {
        log.debug("REST request to delete WpLinks : {}", id);
        wpLinksService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-links?query=:query : search for the wpLinks corresponding
     * to the query.
     *
     * @param query the query of the wpLinks search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-links")
    @Timed
    public ResponseEntity<List<WpLinksDTO>> searchWpLinks(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpLinks for query {}", query);
        Page<WpLinksDTO> page = wpLinksService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-links");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
