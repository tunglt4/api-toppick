package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpCommentmetaService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpCommentmetaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpCommentmeta.
 */
@RestController
@RequestMapping("/api")
public class WpCommentmetaResource {

    private final Logger log = LoggerFactory.getLogger(WpCommentmetaResource.class);

    private static final String ENTITY_NAME = "wpCommentmeta";

    private WpCommentmetaService wpCommentmetaService;

    public WpCommentmetaResource(WpCommentmetaService wpCommentmetaService) {
        this.wpCommentmetaService = wpCommentmetaService;
    }

    /**
     * POST  /wp-commentmetas : Create a new wpCommentmeta.
     *
     * @param wpCommentmetaDTO the wpCommentmetaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpCommentmetaDTO, or with status 400 (Bad Request) if the wpCommentmeta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-commentmetas")
    @Timed
    public ResponseEntity<WpCommentmetaDTO> createWpCommentmeta(@RequestBody WpCommentmetaDTO wpCommentmetaDTO) throws URISyntaxException {
        log.debug("REST request to save WpCommentmeta : {}", wpCommentmetaDTO);
        if (wpCommentmetaDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpCommentmeta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpCommentmetaDTO result = wpCommentmetaService.save(wpCommentmetaDTO);
        return ResponseEntity.created(new URI("/api/wp-commentmetas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-commentmetas : Updates an existing wpCommentmeta.
     *
     * @param wpCommentmetaDTO the wpCommentmetaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpCommentmetaDTO,
     * or with status 400 (Bad Request) if the wpCommentmetaDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpCommentmetaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-commentmetas")
    @Timed
    public ResponseEntity<WpCommentmetaDTO> updateWpCommentmeta(@RequestBody WpCommentmetaDTO wpCommentmetaDTO) throws URISyntaxException {
        log.debug("REST request to update WpCommentmeta : {}", wpCommentmetaDTO);
        if (wpCommentmetaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpCommentmetaDTO result = wpCommentmetaService.save(wpCommentmetaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpCommentmetaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-commentmetas : get all the wpCommentmetas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpCommentmetas in body
     */
    @GetMapping("/wp-commentmetas")
    @Timed
    public ResponseEntity<List<WpCommentmetaDTO>> getAllWpCommentmetas(Pageable pageable) {
        log.debug("REST request to get a page of WpCommentmetas");
        Page<WpCommentmetaDTO> page = wpCommentmetaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-commentmetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-commentmetas/:id : get the "id" wpCommentmeta.
     *
     * @param id the id of the wpCommentmetaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpCommentmetaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-commentmetas/{id}")
    @Timed
    public ResponseEntity<WpCommentmetaDTO> getWpCommentmeta(@PathVariable Long id) {
        log.debug("REST request to get WpCommentmeta : {}", id);
        Optional<WpCommentmetaDTO> wpCommentmetaDTO = wpCommentmetaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpCommentmetaDTO);
    }

    /**
     * DELETE  /wp-commentmetas/:id : delete the "id" wpCommentmeta.
     *
     * @param id the id of the wpCommentmetaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-commentmetas/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpCommentmeta(@PathVariable Long id) {
        log.debug("REST request to delete WpCommentmeta : {}", id);
        wpCommentmetaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-commentmetas?query=:query : search for the wpCommentmeta corresponding
     * to the query.
     *
     * @param query the query of the wpCommentmeta search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-commentmetas")
    @Timed
    public ResponseEntity<List<WpCommentmetaDTO>> searchWpCommentmetas(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpCommentmetas for query {}", query);
        Page<WpCommentmetaDTO> page = wpCommentmetaService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-commentmetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
