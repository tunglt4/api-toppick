package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpTermTaxonomyService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpTermTaxonomyDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpTermTaxonomy.
 */
@RestController
@RequestMapping("/api")
public class WpTermTaxonomyResource {

    private final Logger log = LoggerFactory.getLogger(WpTermTaxonomyResource.class);

    private static final String ENTITY_NAME = "wpTermTaxonomy";

    private WpTermTaxonomyService wpTermTaxonomyService;

    public WpTermTaxonomyResource(WpTermTaxonomyService wpTermTaxonomyService) {
        this.wpTermTaxonomyService = wpTermTaxonomyService;
    }

    /**
     * POST  /wp-term-taxonomies : Create a new wpTermTaxonomy.
     *
     * @param wpTermTaxonomyDTO the wpTermTaxonomyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpTermTaxonomyDTO, or with status 400 (Bad Request) if the wpTermTaxonomy has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-term-taxonomies")
    @Timed
    public ResponseEntity<WpTermTaxonomyDTO> createWpTermTaxonomy(@RequestBody WpTermTaxonomyDTO wpTermTaxonomyDTO) throws URISyntaxException {
        log.debug("REST request to save WpTermTaxonomy : {}", wpTermTaxonomyDTO);
        if (wpTermTaxonomyDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpTermTaxonomy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpTermTaxonomyDTO result = wpTermTaxonomyService.save(wpTermTaxonomyDTO);
        return ResponseEntity.created(new URI("/api/wp-term-taxonomies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-term-taxonomies : Updates an existing wpTermTaxonomy.
     *
     * @param wpTermTaxonomyDTO the wpTermTaxonomyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpTermTaxonomyDTO,
     * or with status 400 (Bad Request) if the wpTermTaxonomyDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpTermTaxonomyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-term-taxonomies")
    @Timed
    public ResponseEntity<WpTermTaxonomyDTO> updateWpTermTaxonomy(@RequestBody WpTermTaxonomyDTO wpTermTaxonomyDTO) throws URISyntaxException {
        log.debug("REST request to update WpTermTaxonomy : {}", wpTermTaxonomyDTO);
        if (wpTermTaxonomyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpTermTaxonomyDTO result = wpTermTaxonomyService.save(wpTermTaxonomyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpTermTaxonomyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-term-taxonomies : get all the wpTermTaxonomies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpTermTaxonomies in body
     */
    @GetMapping("/wp-term-taxonomies")
    @Timed
    public ResponseEntity<List<WpTermTaxonomyDTO>> getAllWpTermTaxonomies(Pageable pageable) {
        log.debug("REST request to get a page of WpTermTaxonomies");
        Page<WpTermTaxonomyDTO> page = wpTermTaxonomyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-term-taxonomies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-term-taxonomies/:id : get the "id" wpTermTaxonomy.
     *
     * @param id the id of the wpTermTaxonomyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpTermTaxonomyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-term-taxonomies/{id}")
    @Timed
    public ResponseEntity<WpTermTaxonomyDTO> getWpTermTaxonomy(@PathVariable Long id) {
        log.debug("REST request to get WpTermTaxonomy : {}", id);
        Optional<WpTermTaxonomyDTO> wpTermTaxonomyDTO = wpTermTaxonomyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpTermTaxonomyDTO);
    }

    /**
     * DELETE  /wp-term-taxonomies/:id : delete the "id" wpTermTaxonomy.
     *
     * @param id the id of the wpTermTaxonomyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-term-taxonomies/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpTermTaxonomy(@PathVariable Long id) {
        log.debug("REST request to delete WpTermTaxonomy : {}", id);
        wpTermTaxonomyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-term-taxonomies?query=:query : search for the wpTermTaxonomy corresponding
     * to the query.
     *
     * @param query the query of the wpTermTaxonomy search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-term-taxonomies")
    @Timed
    public ResponseEntity<List<WpTermTaxonomyDTO>> searchWpTermTaxonomies(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpTermTaxonomies for query {}", query);
        Page<WpTermTaxonomyDTO> page = wpTermTaxonomyService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-term-taxonomies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
