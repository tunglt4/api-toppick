package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpCommentsService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpCommentsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpComments.
 */
@RestController
@RequestMapping("/api")
public class WpCommentsResource {

    private final Logger log = LoggerFactory.getLogger(WpCommentsResource.class);

    private static final String ENTITY_NAME = "wpComments";

    private WpCommentsService wpCommentsService;

    public WpCommentsResource(WpCommentsService wpCommentsService) {
        this.wpCommentsService = wpCommentsService;
    }

    /**
     * POST  /wp-comments : Create a new wpComments.
     *
     * @param wpCommentsDTO the wpCommentsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpCommentsDTO, or with status 400 (Bad Request) if the wpComments has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-comments")
    @Timed
    public ResponseEntity<WpCommentsDTO> createWpComments(@RequestBody WpCommentsDTO wpCommentsDTO) throws URISyntaxException {
        log.debug("REST request to save WpComments : {}", wpCommentsDTO);
        if (wpCommentsDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpComments cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpCommentsDTO result = wpCommentsService.save(wpCommentsDTO);
        return ResponseEntity.created(new URI("/api/wp-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-comments : Updates an existing wpComments.
     *
     * @param wpCommentsDTO the wpCommentsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpCommentsDTO,
     * or with status 400 (Bad Request) if the wpCommentsDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpCommentsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-comments")
    @Timed
    public ResponseEntity<WpCommentsDTO> updateWpComments(@RequestBody WpCommentsDTO wpCommentsDTO) throws URISyntaxException {
        log.debug("REST request to update WpComments : {}", wpCommentsDTO);
        if (wpCommentsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpCommentsDTO result = wpCommentsService.save(wpCommentsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpCommentsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-comments : get all the wpComments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpComments in body
     */
    @GetMapping("/wp-comments")
    @Timed
    public ResponseEntity<List<WpCommentsDTO>> getAllWpComments(Pageable pageable) {
        log.debug("REST request to get a page of WpComments");
        Page<WpCommentsDTO> page = wpCommentsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-comments/:id : get the "id" wpComments.
     *
     * @param id the id of the wpCommentsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpCommentsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-comments/{id}")
    @Timed
    public ResponseEntity<WpCommentsDTO> getWpComments(@PathVariable Long id) {
        log.debug("REST request to get WpComments : {}", id);
        Optional<WpCommentsDTO> wpCommentsDTO = wpCommentsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpCommentsDTO);
    }

    /**
     * DELETE  /wp-comments/:id : delete the "id" wpComments.
     *
     * @param id the id of the wpCommentsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpComments(@PathVariable Long id) {
        log.debug("REST request to delete WpComments : {}", id);
        wpCommentsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-comments?query=:query : search for the wpComments corresponding
     * to the query.
     *
     * @param query the query of the wpComments search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-comments")
    @Timed
    public ResponseEntity<List<WpCommentsDTO>> searchWpComments(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpComments for query {}", query);
        Page<WpCommentsDTO> page = wpCommentsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
