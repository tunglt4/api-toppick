package com.topica.toppick.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.topica.toppick.service.WpPostViewsService;
import com.topica.toppick.web.rest.errors.BadRequestAlertException;
import com.topica.toppick.web.rest.util.HeaderUtil;
import com.topica.toppick.web.rest.util.PaginationUtil;
import com.topica.toppick.service.dto.WpPostViewsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WpPostViews.
 */
@RestController
@RequestMapping("/api")
public class WpPostViewsResource {

    private final Logger log = LoggerFactory.getLogger(WpPostViewsResource.class);

    private static final String ENTITY_NAME = "wpPostViews";

    private WpPostViewsService wpPostViewsService;

    public WpPostViewsResource(WpPostViewsService wpPostViewsService) {
        this.wpPostViewsService = wpPostViewsService;
    }

    /**
     * POST  /wp-post-views : Create a new wpPostViews.
     *
     * @param wpPostViewsDTO the wpPostViewsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new wpPostViewsDTO, or with status 400 (Bad Request) if the wpPostViews has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/wp-post-views")
    @Timed
    public ResponseEntity<WpPostViewsDTO> createWpPostViews(@RequestBody WpPostViewsDTO wpPostViewsDTO) throws URISyntaxException {
        log.debug("REST request to save WpPostViews : {}", wpPostViewsDTO);
        if (wpPostViewsDTO.getId() != null) {
            throw new BadRequestAlertException("A new wpPostViews cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WpPostViewsDTO result = wpPostViewsService.save(wpPostViewsDTO);
        return ResponseEntity.created(new URI("/api/wp-post-views/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /wp-post-views : Updates an existing wpPostViews.
     *
     * @param wpPostViewsDTO the wpPostViewsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated wpPostViewsDTO,
     * or with status 400 (Bad Request) if the wpPostViewsDTO is not valid,
     * or with status 500 (Internal Server Error) if the wpPostViewsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/wp-post-views")
    @Timed
    public ResponseEntity<WpPostViewsDTO> updateWpPostViews(@RequestBody WpPostViewsDTO wpPostViewsDTO) throws URISyntaxException {
        log.debug("REST request to update WpPostViews : {}", wpPostViewsDTO);
        if (wpPostViewsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WpPostViewsDTO result = wpPostViewsService.save(wpPostViewsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, wpPostViewsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /wp-post-views : get all the wpPostViews.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of wpPostViews in body
     */
    @GetMapping("/wp-post-views")
    @Timed
    public ResponseEntity<List<WpPostViewsDTO>> getAllWpPostViews(Pageable pageable) {
        log.debug("REST request to get a page of WpPostViews");
        Page<WpPostViewsDTO> page = wpPostViewsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/wp-post-views");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /wp-post-views/:id : get the "id" wpPostViews.
     *
     * @param id the id of the wpPostViewsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the wpPostViewsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/wp-post-views/{id}")
    @Timed
    public ResponseEntity<WpPostViewsDTO> getWpPostViews(@PathVariable Long id) {
        log.debug("REST request to get WpPostViews : {}", id);
        Optional<WpPostViewsDTO> wpPostViewsDTO = wpPostViewsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wpPostViewsDTO);
    }

    /**
     * DELETE  /wp-post-views/:id : delete the "id" wpPostViews.
     *
     * @param id the id of the wpPostViewsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/wp-post-views/{id}")
    @Timed
    public ResponseEntity<Void> deleteWpPostViews(@PathVariable Long id) {
        log.debug("REST request to delete WpPostViews : {}", id);
        wpPostViewsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/wp-post-views?query=:query : search for the wpPostViews corresponding
     * to the query.
     *
     * @param query the query of the wpPostViews search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/wp-post-views")
    @Timed
    public ResponseEntity<List<WpPostViewsDTO>> searchWpPostViews(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of WpPostViews for query {}", query);
        Page<WpPostViewsDTO> page = wpPostViewsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/wp-post-views");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
