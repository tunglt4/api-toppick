package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A WpComments.
 */
@Entity
@Table(name = "wp_comments")
@Document(indexName = "wpcomments")
public class WpComments implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "comment_id")
    private Long commentId;

    @Column(name = "comment_post_id")
    private Long commentPostId;

    @Column(name = "comment_author")
    private String commentAuthor;

    @Column(name = "comment_author_email")
    private String commentAuthorEmail;

    @Column(name = "comment_author_url")
    private String commentAuthorUrl;

    @Column(name = "comment_author_ip")
    private String commentAuthorIp;

    @Column(name = "comment_date")
    private Instant commentDate;

    @Column(name = "comment_date_gmt")
    private Instant commentDateGmt;

    @Column(name = "comment_content")
    private String commentContent;

    @Column(name = "comment_karma")
    private Integer commentKarma;

    @Column(name = "comment_approved")
    private String commentApproved;

    @Column(name = "comment_agent")
    private String commentAgent;

    @Column(name = "comment_type")
    private String commentType;

    @Column(name = "comment_parent")
    private Long commentParent;

    @Column(name = "user_id")
    private Long userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCommentId() {
        return commentId;
    }

    public WpComments commentId(Long commentId) {
        this.commentId = commentId;
        return this;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getCommentPostId() {
        return commentPostId;
    }

    public WpComments commentPostId(Long commentPostId) {
        this.commentPostId = commentPostId;
        return this;
    }

    public void setCommentPostId(Long commentPostId) {
        this.commentPostId = commentPostId;
    }

    public String getCommentAuthor() {
        return commentAuthor;
    }

    public WpComments commentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
        return this;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public String getCommentAuthorEmail() {
        return commentAuthorEmail;
    }

    public WpComments commentAuthorEmail(String commentAuthorEmail) {
        this.commentAuthorEmail = commentAuthorEmail;
        return this;
    }

    public void setCommentAuthorEmail(String commentAuthorEmail) {
        this.commentAuthorEmail = commentAuthorEmail;
    }

    public String getCommentAuthorUrl() {
        return commentAuthorUrl;
    }

    public WpComments commentAuthorUrl(String commentAuthorUrl) {
        this.commentAuthorUrl = commentAuthorUrl;
        return this;
    }

    public void setCommentAuthorUrl(String commentAuthorUrl) {
        this.commentAuthorUrl = commentAuthorUrl;
    }

    public String getCommentAuthorIp() {
        return commentAuthorIp;
    }

    public WpComments commentAuthorIp(String commentAuthorIp) {
        this.commentAuthorIp = commentAuthorIp;
        return this;
    }

    public void setCommentAuthorIp(String commentAuthorIp) {
        this.commentAuthorIp = commentAuthorIp;
    }

    public Instant getCommentDate() {
        return commentDate;
    }

    public WpComments commentDate(Instant commentDate) {
        this.commentDate = commentDate;
        return this;
    }

    public void setCommentDate(Instant commentDate) {
        this.commentDate = commentDate;
    }

    public Instant getCommentDateGmt() {
        return commentDateGmt;
    }

    public WpComments commentDateGmt(Instant commentDateGmt) {
        this.commentDateGmt = commentDateGmt;
        return this;
    }

    public void setCommentDateGmt(Instant commentDateGmt) {
        this.commentDateGmt = commentDateGmt;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public WpComments commentContent(String commentContent) {
        this.commentContent = commentContent;
        return this;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public Integer getCommentKarma() {
        return commentKarma;
    }

    public WpComments commentKarma(Integer commentKarma) {
        this.commentKarma = commentKarma;
        return this;
    }

    public void setCommentKarma(Integer commentKarma) {
        this.commentKarma = commentKarma;
    }

    public String getCommentApproved() {
        return commentApproved;
    }

    public WpComments commentApproved(String commentApproved) {
        this.commentApproved = commentApproved;
        return this;
    }

    public void setCommentApproved(String commentApproved) {
        this.commentApproved = commentApproved;
    }

    public String getCommentAgent() {
        return commentAgent;
    }

    public WpComments commentAgent(String commentAgent) {
        this.commentAgent = commentAgent;
        return this;
    }

    public void setCommentAgent(String commentAgent) {
        this.commentAgent = commentAgent;
    }

    public String getCommentType() {
        return commentType;
    }

    public WpComments commentType(String commentType) {
        this.commentType = commentType;
        return this;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
    }

    public Long getCommentParent() {
        return commentParent;
    }

    public WpComments commentParent(Long commentParent) {
        this.commentParent = commentParent;
        return this;
    }

    public void setCommentParent(Long commentParent) {
        this.commentParent = commentParent;
    }

    public Long getUserId() {
        return userId;
    }

    public WpComments userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpComments wpComments = (WpComments) o;
        if (wpComments.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpComments.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpComments{" +
            "id=" + getId() +
            ", commentId=" + getCommentId() +
            ", commentPostId=" + getCommentPostId() +
            ", commentAuthor='" + getCommentAuthor() + "'" +
            ", commentAuthorEmail='" + getCommentAuthorEmail() + "'" +
            ", commentAuthorUrl='" + getCommentAuthorUrl() + "'" +
            ", commentAuthorIp='" + getCommentAuthorIp() + "'" +
            ", commentDate='" + getCommentDate() + "'" +
            ", commentDateGmt='" + getCommentDateGmt() + "'" +
            ", commentContent='" + getCommentContent() + "'" +
            ", commentKarma=" + getCommentKarma() +
            ", commentApproved='" + getCommentApproved() + "'" +
            ", commentAgent='" + getCommentAgent() + "'" +
            ", commentType='" + getCommentType() + "'" +
            ", commentParent=" + getCommentParent() +
            ", userId=" + getUserId() +
            "}";
    }
}
