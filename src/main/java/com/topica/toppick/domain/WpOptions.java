package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WpOptions.
 */
@Entity
@Table(name = "wp_options")
@Document(indexName = "wpoptions")
public class WpOptions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "option_id")
    private Long optionId;

    @Column(name = "option_name")
    private String optionName;

    @Column(name = "option_value")
    private String optionValue;

    @Column(name = "autoload")
    private String autoload;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOptionId() {
        return optionId;
    }

    public WpOptions optionId(Long optionId) {
        this.optionId = optionId;
        return this;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public WpOptions optionName(String optionName) {
        this.optionName = optionName;
        return this;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public WpOptions optionValue(String optionValue) {
        this.optionValue = optionValue;
        return this;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    public String getAutoload() {
        return autoload;
    }

    public WpOptions autoload(String autoload) {
        this.autoload = autoload;
        return this;
    }

    public void setAutoload(String autoload) {
        this.autoload = autoload;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpOptions wpOptions = (WpOptions) o;
        if (wpOptions.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpOptions.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpOptions{" +
            "id=" + getId() +
            ", optionId=" + getOptionId() +
            ", optionName='" + getOptionName() + "'" +
            ", optionValue='" + getOptionValue() + "'" +
            ", autoload='" + getAutoload() + "'" +
            "}";
    }
}
