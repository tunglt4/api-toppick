package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WpTermTaxonomy.
 */
@Entity
@Table(name = "wp_term_taxonomy")
@Document(indexName = "wptermtaxonomy")
public class WpTermTaxonomy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "term_taxonomy_id")
    private Long termTaxonomyId;

    @Column(name = "term_id")
    private Long termId;

    @Column(name = "taxonomy")
    private String taxonomy;

    @Column(name = "description")
    private String description;

    @Column(name = "parent")
    private Long parent;

    @Column(name = "count")
    private Long count;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTermTaxonomyId() {
        return termTaxonomyId;
    }

    public WpTermTaxonomy termTaxonomyId(Long termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
        return this;
    }

    public void setTermTaxonomyId(Long termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
    }

    public Long getTermId() {
        return termId;
    }

    public WpTermTaxonomy termId(Long termId) {
        this.termId = termId;
        return this;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public WpTermTaxonomy taxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
        return this;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    public String getDescription() {
        return description;
    }

    public WpTermTaxonomy description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParent() {
        return parent;
    }

    public WpTermTaxonomy parent(Long parent) {
        this.parent = parent;
        return this;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Long getCount() {
        return count;
    }

    public WpTermTaxonomy count(Long count) {
        this.count = count;
        return this;
    }

    public void setCount(Long count) {
        this.count = count;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpTermTaxonomy wpTermTaxonomy = (WpTermTaxonomy) o;
        if (wpTermTaxonomy.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpTermTaxonomy.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpTermTaxonomy{" +
            "id=" + getId() +
            ", termTaxonomyId=" + getTermTaxonomyId() +
            ", termId=" + getTermId() +
            ", taxonomy='" + getTaxonomy() + "'" +
            ", description='" + getDescription() + "'" +
            ", parent=" + getParent() +
            ", count=" + getCount() +
            "}";
    }
}
