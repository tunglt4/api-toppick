package com.topica.toppick.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.elasticsearch.annotations.Document;

/**
 * A WpUsermeta.
 */
@Entity
@Table(name = "wp_usermeta")
@Document(indexName = "wpusermeta")
public class WpUsermeta implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "umeta_id")
	private Long id;

	// @Column(name = "user_id", insertable = false, updatable = false)
	// private Long userId;

	@Column(name = "meta_key")
	private String metaKey;

	@Column(name = "meta_value")
	private String metaValue;

	@JoinColumn(referencedColumnName = "id")
	@ManyToOne
	private User user;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMetaKey() {
		return metaKey;
	}

	public WpUsermeta metaKey(String metaKey) {
		this.metaKey = metaKey;
		return this;
	}

	public void setMetaKey(String metaKey) {
		this.metaKey = metaKey;
	}

	public String getMetaValue() {
		return metaValue;
	}

	public WpUsermeta metaValue(String metaValue) {
		this.metaValue = metaValue;
		return this;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setMetaValue(String metaValue) {
		this.metaValue = metaValue;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		WpUsermeta wpUsermeta = (WpUsermeta) o;
		if (wpUsermeta.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), wpUsermeta.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "WpUsermeta{" + "id=" + getId() + ", metaKey='" + getMetaKey() + "'" + ", metaValue='" + getMetaValue()
				+ "'" + "}";
	}
}
