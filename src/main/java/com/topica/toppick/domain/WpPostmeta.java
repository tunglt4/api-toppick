package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WpPostmeta.
 */
@Entity
@Table(name = "wp_postmeta")
@Document(indexName = "wppostmeta")
public class WpPostmeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "meta_id")
    private Long metaId;

    @Column(name = "post_id")
    private Long postId;

    @Column(name = "meta_key")
    private String metaKey;

    @Column(name = "meta_value")
    private String metaValue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMetaId() {
        return metaId;
    }

    public WpPostmeta metaId(Long metaId) {
        this.metaId = metaId;
        return this;
    }

    public void setMetaId(Long metaId) {
        this.metaId = metaId;
    }

    public Long getPostId() {
        return postId;
    }

    public WpPostmeta postId(Long postId) {
        this.postId = postId;
        return this;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public WpPostmeta metaKey(String metaKey) {
        this.metaKey = metaKey;
        return this;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public WpPostmeta metaValue(String metaValue) {
        this.metaValue = metaValue;
        return this;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpPostmeta wpPostmeta = (WpPostmeta) o;
        if (wpPostmeta.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPostmeta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPostmeta{" +
            "id=" + getId() +
            ", metaId=" + getMetaId() +
            ", postId=" + getPostId() +
            ", metaKey='" + getMetaKey() + "'" +
            ", metaValue='" + getMetaValue() + "'" +
            "}";
    }
}
