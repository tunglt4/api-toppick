package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WpTermRelationships.
 */
@Entity
@Table(name = "wp_term_relationships")
@Document(indexName = "wptermrelationships")
public class WpTermRelationships implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "object_id")
    private Long objectId;

    @Column(name = "term_taxonomy_id")
    private Long termTaxonomyId;

    @Column(name = "term_order")
    private Integer termOrder;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getObjectId() {
        return objectId;
    }

    public WpTermRelationships objectId(Long objectId) {
        this.objectId = objectId;
        return this;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getTermTaxonomyId() {
        return termTaxonomyId;
    }

    public WpTermRelationships termTaxonomyId(Long termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
        return this;
    }

    public void setTermTaxonomyId(Long termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
    }

    public Integer getTermOrder() {
        return termOrder;
    }

    public WpTermRelationships termOrder(Integer termOrder) {
        this.termOrder = termOrder;
        return this;
    }

    public void setTermOrder(Integer termOrder) {
        this.termOrder = termOrder;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpTermRelationships wpTermRelationships = (WpTermRelationships) o;
        if (wpTermRelationships.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpTermRelationships.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpTermRelationships{" +
            "id=" + getId() +
            ", objectId=" + getObjectId() +
            ", termTaxonomyId=" + getTermTaxonomyId() +
            ", termOrder=" + getTermOrder() +
            "}";
    }
}
