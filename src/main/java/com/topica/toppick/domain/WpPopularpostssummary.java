package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A WpPopularpostssummary.
 */
@Entity
@Table(name = "wp_popularpostssummary")
@Document(indexName = "wppopularpostssummary")
public class WpPopularpostssummary implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "postid")
    private Long postid;

    @Column(name = "pageviews")
    private Long pageviews;

    @Column(name = "view_date")
    private LocalDate viewDate;

    @Column(name = "view_datetime")
    private Instant viewDatetime;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostid() {
        return postid;
    }

    public WpPopularpostssummary postid(Long postid) {
        this.postid = postid;
        return this;
    }

    public void setPostid(Long postid) {
        this.postid = postid;
    }

    public Long getPageviews() {
        return pageviews;
    }

    public WpPopularpostssummary pageviews(Long pageviews) {
        this.pageviews = pageviews;
        return this;
    }

    public void setPageviews(Long pageviews) {
        this.pageviews = pageviews;
    }

    public LocalDate getViewDate() {
        return viewDate;
    }

    public WpPopularpostssummary viewDate(LocalDate viewDate) {
        this.viewDate = viewDate;
        return this;
    }

    public void setViewDate(LocalDate viewDate) {
        this.viewDate = viewDate;
    }

    public Instant getViewDatetime() {
        return viewDatetime;
    }

    public WpPopularpostssummary viewDatetime(Instant viewDatetime) {
        this.viewDatetime = viewDatetime;
        return this;
    }

    public void setViewDatetime(Instant viewDatetime) {
        this.viewDatetime = viewDatetime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpPopularpostssummary wpPopularpostssummary = (WpPopularpostssummary) o;
        if (wpPopularpostssummary.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPopularpostssummary.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPopularpostssummary{" +
            "id=" + getId() +
            ", postid=" + getPostid() +
            ", pageviews=" + getPageviews() +
            ", viewDate='" + getViewDate() + "'" +
            ", viewDatetime='" + getViewDatetime() + "'" +
            "}";
    }
}
