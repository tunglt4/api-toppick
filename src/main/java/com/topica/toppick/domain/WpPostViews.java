package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WpPostViews.
 */
@Entity
@Table(name = "wp_post_views")
@Document(indexName = "wppostviews")
public class WpPostViews implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jhi_type")
    private Integer type;

    @Column(name = "period")
    private String period;

    @Column(name = "count")
    private Long count;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public WpPostViews type(Integer type) {
        this.type = type;
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPeriod() {
        return period;
    }

    public WpPostViews period(String period) {
        this.period = period;
        return this;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Long getCount() {
        return count;
    }

    public WpPostViews count(Long count) {
        this.count = count;
        return this;
    }

    public void setCount(Long count) {
        this.count = count;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpPostViews wpPostViews = (WpPostViews) o;
        if (wpPostViews.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPostViews.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPostViews{" +
            "id=" + getId() +
            ", type=" + getType() +
            ", period='" + getPeriod() + "'" +
            ", count=" + getCount() +
            "}";
    }
}
