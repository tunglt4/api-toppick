package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WpTermmeta.
 */
@Entity
@Table(name = "wp_termmeta")
@Document(indexName = "wptermmeta")
public class WpTermmeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "meta_id")
    private Long metaId;

    @Column(name = "term_id")
    private Long termId;

    @Column(name = "meta_key")
    private String metaKey;

    @Column(name = "meta_value")
    private String metaValue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMetaId() {
        return metaId;
    }

    public WpTermmeta metaId(Long metaId) {
        this.metaId = metaId;
        return this;
    }

    public void setMetaId(Long metaId) {
        this.metaId = metaId;
    }

    public Long getTermId() {
        return termId;
    }

    public WpTermmeta termId(Long termId) {
        this.termId = termId;
        return this;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public WpTermmeta metaKey(String metaKey) {
        this.metaKey = metaKey;
        return this;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public WpTermmeta metaValue(String metaValue) {
        this.metaValue = metaValue;
        return this;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpTermmeta wpTermmeta = (WpTermmeta) o;
        if (wpTermmeta.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpTermmeta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpTermmeta{" +
            "id=" + getId() +
            ", metaId=" + getMetaId() +
            ", termId=" + getTermId() +
            ", metaKey='" + getMetaKey() + "'" +
            ", metaValue='" + getMetaValue() + "'" +
            "}";
    }
}
