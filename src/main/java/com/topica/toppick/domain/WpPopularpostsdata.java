package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A WpPopularpostsdata.
 */
@Entity
@Table(name = "wp_popularpostsdata")
@Document(indexName = "wppopularpostsdata")
public class WpPopularpostsdata implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "postid")
    private Long postid;

    @Column(name = "day")
    private Instant day;

    @Column(name = "last_viewed")
    private Instant lastViewed;

    @Column(name = "pageviews")
    private Long pageviews;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostid() {
        return postid;
    }

    public WpPopularpostsdata postid(Long postid) {
        this.postid = postid;
        return this;
    }

    public void setPostid(Long postid) {
        this.postid = postid;
    }

    public Instant getDay() {
        return day;
    }

    public WpPopularpostsdata day(Instant day) {
        this.day = day;
        return this;
    }

    public void setDay(Instant day) {
        this.day = day;
    }

    public Instant getLastViewed() {
        return lastViewed;
    }

    public WpPopularpostsdata lastViewed(Instant lastViewed) {
        this.lastViewed = lastViewed;
        return this;
    }

    public void setLastViewed(Instant lastViewed) {
        this.lastViewed = lastViewed;
    }

    public Long getPageviews() {
        return pageviews;
    }

    public WpPopularpostsdata pageviews(Long pageviews) {
        this.pageviews = pageviews;
        return this;
    }

    public void setPageviews(Long pageviews) {
        this.pageviews = pageviews;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpPopularpostsdata wpPopularpostsdata = (WpPopularpostsdata) o;
        if (wpPopularpostsdata.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPopularpostsdata.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPopularpostsdata{" +
            "id=" + getId() +
            ", postid=" + getPostid() +
            ", day='" + getDay() + "'" +
            ", lastViewed='" + getLastViewed() + "'" +
            ", pageviews=" + getPageviews() +
            "}";
    }
}
