package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A WpUsers.
 */
@Entity
@Table(name = "wp_users")
@Document(indexName = "wpusers")
public class WpUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_login")
    private String userLogin;

    @Column(name = "user_pass")
    private String userPass;

    @Column(name = "user_nicename")
    private String userNicename;

    @Column(name = "user_email")
    private String userEmail;

    @Column(name = "user_url")
    private String userUrl;

    @Column(name = "user_registered")
    private Instant userRegistered;

    @Column(name = "user_activation_key")
    private String userActivationKey;

    @Column(name = "user_status")
    private Integer userStatus;

    @Column(name = "display_name")
    private String displayName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public WpUsers userLogin(String userLogin) {
        this.userLogin = userLogin;
        return this;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPass() {
        return userPass;
    }

    public WpUsers userPass(String userPass) {
        this.userPass = userPass;
        return this;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getUserNicename() {
        return userNicename;
    }

    public WpUsers userNicename(String userNicename) {
        this.userNicename = userNicename;
        return this;
    }

    public void setUserNicename(String userNicename) {
        this.userNicename = userNicename;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public WpUsers userEmail(String userEmail) {
        this.userEmail = userEmail;
        return this;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public WpUsers userUrl(String userUrl) {
        this.userUrl = userUrl;
        return this;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    public Instant getUserRegistered() {
        return userRegistered;
    }

    public WpUsers userRegistered(Instant userRegistered) {
        this.userRegistered = userRegistered;
        return this;
    }

    public void setUserRegistered(Instant userRegistered) {
        this.userRegistered = userRegistered;
    }

    public String getUserActivationKey() {
        return userActivationKey;
    }

    public WpUsers userActivationKey(String userActivationKey) {
        this.userActivationKey = userActivationKey;
        return this;
    }

    public void setUserActivationKey(String userActivationKey) {
        this.userActivationKey = userActivationKey;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public WpUsers userStatus(Integer userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getDisplayName() {
        return displayName;
    }

    public WpUsers displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpUsers wpUsers = (WpUsers) o;
        if (wpUsers.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpUsers.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpUsers{" +
            "id=" + getId() +
            ", userLogin='" + getUserLogin() + "'" +
            ", userPass='" + getUserPass() + "'" +
            ", userNicename='" + getUserNicename() + "'" +
            ", userEmail='" + getUserEmail() + "'" +
            ", userUrl='" + getUserUrl() + "'" +
            ", userRegistered='" + getUserRegistered() + "'" +
            ", userActivationKey='" + getUserActivationKey() + "'" +
            ", userStatus=" + getUserStatus() +
            ", displayName='" + getDisplayName() + "'" +
            "}";
    }
}
