package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WpTerms.
 */
@Entity
@Table(name = "wp_terms")
@Document(indexName = "wpterms")
public class WpTerms implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "term_id")
    private Long termId;

    @Column(name = "name")
    private String name;

    @Column(name = "slug")
    private String slug;

    @Column(name = "term_group")
    private Long termGroup;

    @Column(name = "term_order")
    private Integer termOrder;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTermId() {
        return termId;
    }

    public WpTerms termId(Long termId) {
        this.termId = termId;
        return this;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public String getName() {
        return name;
    }

    public WpTerms name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public WpTerms slug(String slug) {
        this.slug = slug;
        return this;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Long getTermGroup() {
        return termGroup;
    }

    public WpTerms termGroup(Long termGroup) {
        this.termGroup = termGroup;
        return this;
    }

    public void setTermGroup(Long termGroup) {
        this.termGroup = termGroup;
    }

    public Integer getTermOrder() {
        return termOrder;
    }

    public WpTerms termOrder(Integer termOrder) {
        this.termOrder = termOrder;
        return this;
    }

    public void setTermOrder(Integer termOrder) {
        this.termOrder = termOrder;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpTerms wpTerms = (WpTerms) o;
        if (wpTerms.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpTerms.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpTerms{" +
            "id=" + getId() +
            ", termId=" + getTermId() +
            ", name='" + getName() + "'" +
            ", slug='" + getSlug() + "'" +
            ", termGroup=" + getTermGroup() +
            ", termOrder=" + getTermOrder() +
            "}";
    }
}
