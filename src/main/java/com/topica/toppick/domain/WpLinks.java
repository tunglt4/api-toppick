package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A WpLinks.
 */
@Entity
@Table(name = "wp_links")
@Document(indexName = "wplinks")
public class WpLinks implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "link_id")
    private Long linkId;

    @Column(name = "link_url")
    private String linkUrl;

    @Column(name = "link_name")
    private String linkName;

    @Column(name = "link_image")
    private String linkImage;

    @Column(name = "link_target")
    private String linkTarget;

    @Column(name = "link_description")
    private String linkDescription;

    @Column(name = "link_visible")
    private String linkVisible;

    @Column(name = "link_owner")
    private Long linkOwner;

    @Column(name = "link_rating")
    private Integer linkRating;

    @Column(name = "link_updated")
    private Instant linkUpdated;

    @Column(name = "link_rel")
    private String linkRel;

    @Column(name = "link_notes")
    private String linkNotes;

    @Column(name = "link_rss")
    private String linkRss;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLinkId() {
        return linkId;
    }

    public WpLinks linkId(Long linkId) {
        this.linkId = linkId;
        return this;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public WpLinks linkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
        return this;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkName() {
        return linkName;
    }

    public WpLinks linkName(String linkName) {
        this.linkName = linkName;
        return this;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public WpLinks linkImage(String linkImage) {
        this.linkImage = linkImage;
        return this;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public String getLinkTarget() {
        return linkTarget;
    }

    public WpLinks linkTarget(String linkTarget) {
        this.linkTarget = linkTarget;
        return this;
    }

    public void setLinkTarget(String linkTarget) {
        this.linkTarget = linkTarget;
    }

    public String getLinkDescription() {
        return linkDescription;
    }

    public WpLinks linkDescription(String linkDescription) {
        this.linkDescription = linkDescription;
        return this;
    }

    public void setLinkDescription(String linkDescription) {
        this.linkDescription = linkDescription;
    }

    public String getLinkVisible() {
        return linkVisible;
    }

    public WpLinks linkVisible(String linkVisible) {
        this.linkVisible = linkVisible;
        return this;
    }

    public void setLinkVisible(String linkVisible) {
        this.linkVisible = linkVisible;
    }

    public Long getLinkOwner() {
        return linkOwner;
    }

    public WpLinks linkOwner(Long linkOwner) {
        this.linkOwner = linkOwner;
        return this;
    }

    public void setLinkOwner(Long linkOwner) {
        this.linkOwner = linkOwner;
    }

    public Integer getLinkRating() {
        return linkRating;
    }

    public WpLinks linkRating(Integer linkRating) {
        this.linkRating = linkRating;
        return this;
    }

    public void setLinkRating(Integer linkRating) {
        this.linkRating = linkRating;
    }

    public Instant getLinkUpdated() {
        return linkUpdated;
    }

    public WpLinks linkUpdated(Instant linkUpdated) {
        this.linkUpdated = linkUpdated;
        return this;
    }

    public void setLinkUpdated(Instant linkUpdated) {
        this.linkUpdated = linkUpdated;
    }

    public String getLinkRel() {
        return linkRel;
    }

    public WpLinks linkRel(String linkRel) {
        this.linkRel = linkRel;
        return this;
    }

    public void setLinkRel(String linkRel) {
        this.linkRel = linkRel;
    }

    public String getLinkNotes() {
        return linkNotes;
    }

    public WpLinks linkNotes(String linkNotes) {
        this.linkNotes = linkNotes;
        return this;
    }

    public void setLinkNotes(String linkNotes) {
        this.linkNotes = linkNotes;
    }

    public String getLinkRss() {
        return linkRss;
    }

    public WpLinks linkRss(String linkRss) {
        this.linkRss = linkRss;
        return this;
    }

    public void setLinkRss(String linkRss) {
        this.linkRss = linkRss;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpLinks wpLinks = (WpLinks) o;
        if (wpLinks.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpLinks.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpLinks{" +
            "id=" + getId() +
            ", linkId=" + getLinkId() +
            ", linkUrl='" + getLinkUrl() + "'" +
            ", linkName='" + getLinkName() + "'" +
            ", linkImage='" + getLinkImage() + "'" +
            ", linkTarget='" + getLinkTarget() + "'" +
            ", linkDescription='" + getLinkDescription() + "'" +
            ", linkVisible='" + getLinkVisible() + "'" +
            ", linkOwner=" + getLinkOwner() +
            ", linkRating=" + getLinkRating() +
            ", linkUpdated='" + getLinkUpdated() + "'" +
            ", linkRel='" + getLinkRel() + "'" +
            ", linkNotes='" + getLinkNotes() + "'" +
            ", linkRss='" + getLinkRss() + "'" +
            "}";
    }
}
