package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

/**
 * A WpCommentmeta.
 */
@Entity
@Table(name = "wp_commentmeta")
@Document(indexName = "wpcommentmeta")
public class WpCommentmeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "meta_id")
    private Long metaId;

    @Column(name = "comment_id")
    private Long commentId;

    @Column(name = "meta_key")
    private String metaKey;

    @Column(name = "meta_value")
    private String metaValue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMetaId() {
        return metaId;
    }

    public WpCommentmeta metaId(Long metaId) {
        this.metaId = metaId;
        return this;
    }

    public void setMetaId(Long metaId) {
        this.metaId = metaId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public WpCommentmeta commentId(Long commentId) {
        this.commentId = commentId;
        return this;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public WpCommentmeta metaKey(String metaKey) {
        this.metaKey = metaKey;
        return this;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public WpCommentmeta metaValue(String metaValue) {
        this.metaValue = metaValue;
        return this;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpCommentmeta wpCommentmeta = (WpCommentmeta) o;
        if (wpCommentmeta.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpCommentmeta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpCommentmeta{" +
            "id=" + getId() +
            ", metaId=" + getMetaId() +
            ", commentId=" + getCommentId() +
            ", metaKey='" + getMetaKey() + "'" +
            ", metaValue='" + getMetaValue() + "'" +
            "}";
    }
}
