package com.topica.toppick.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A WpPosts.
 */
@Entity
@Table(name = "wp_posts")
@Document(indexName = "wpposts")
public class WpPosts implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "post_author")
    private Long postAuthor;

    @Column(name = "post_date")
    private Instant postDate;

    @Column(name = "post_date_gmt")
    private Instant postDateGmt;

    @Column(name = "post_content")
    private String postContent;

    @Column(name = "post_title")
    private String postTitle;

    @Column(name = "post_excerpt")
    private String postExcerpt;

    @Column(name = "post_status")
    private String postStatus;

    @Column(name = "comment_status")
    private String commentStatus;

    @Column(name = "ping_status")
    private String pingStatus;

    @Column(name = "post_password")
    private String postPassword;

    @Column(name = "post_name")
    private String postName;

    @Column(name = "to_ping")
    private String toPing;

    @Column(name = "pinged")
    private String pinged;

    @Column(name = "post_modified")
    private Instant postModified;

    @Column(name = "post_modified_gmt")
    private Instant postModifiedGmt;

    @Column(name = "post_content_filtered")
    private String postContentFiltered;

    @Column(name = "post_parent")
    private Long postParent;

    @Column(name = "guid")
    private String guid;

    @Column(name = "menu_order")
    private Integer menuOrder;

    @Column(name = "post_type")
    private String postType;

    @Column(name = "post_mime_type")
    private String postMimeType;

    @Column(name = "comment_count")
    private Long commentCount;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostAuthor() {
        return postAuthor;
    }

    public WpPosts postAuthor(Long postAuthor) {
        this.postAuthor = postAuthor;
        return this;
    }

    public void setPostAuthor(Long postAuthor) {
        this.postAuthor = postAuthor;
    }

    public Instant getPostDate() {
        return postDate;
    }

    public WpPosts postDate(Instant postDate) {
        this.postDate = postDate;
        return this;
    }

    public void setPostDate(Instant postDate) {
        this.postDate = postDate;
    }

    public Instant getPostDateGmt() {
        return postDateGmt;
    }

    public WpPosts postDateGmt(Instant postDateGmt) {
        this.postDateGmt = postDateGmt;
        return this;
    }

    public void setPostDateGmt(Instant postDateGmt) {
        this.postDateGmt = postDateGmt;
    }

    public String getPostContent() {
        return postContent;
    }

    public WpPosts postContent(String postContent) {
        this.postContent = postContent;
        return this;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public WpPosts postTitle(String postTitle) {
        this.postTitle = postTitle;
        return this;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostExcerpt() {
        return postExcerpt;
    }

    public WpPosts postExcerpt(String postExcerpt) {
        this.postExcerpt = postExcerpt;
        return this;
    }

    public void setPostExcerpt(String postExcerpt) {
        this.postExcerpt = postExcerpt;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public WpPosts postStatus(String postStatus) {
        this.postStatus = postStatus;
        return this;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public WpPosts commentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
        return this;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getPingStatus() {
        return pingStatus;
    }

    public WpPosts pingStatus(String pingStatus) {
        this.pingStatus = pingStatus;
        return this;
    }

    public void setPingStatus(String pingStatus) {
        this.pingStatus = pingStatus;
    }

    public String getPostPassword() {
        return postPassword;
    }

    public WpPosts postPassword(String postPassword) {
        this.postPassword = postPassword;
        return this;
    }

    public void setPostPassword(String postPassword) {
        this.postPassword = postPassword;
    }

    public String getPostName() {
        return postName;
    }

    public WpPosts postName(String postName) {
        this.postName = postName;
        return this;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getToPing() {
        return toPing;
    }

    public WpPosts toPing(String toPing) {
        this.toPing = toPing;
        return this;
    }

    public void setToPing(String toPing) {
        this.toPing = toPing;
    }

    public String getPinged() {
        return pinged;
    }

    public WpPosts pinged(String pinged) {
        this.pinged = pinged;
        return this;
    }

    public void setPinged(String pinged) {
        this.pinged = pinged;
    }

    public Instant getPostModified() {
        return postModified;
    }

    public WpPosts postModified(Instant postModified) {
        this.postModified = postModified;
        return this;
    }

    public void setPostModified(Instant postModified) {
        this.postModified = postModified;
    }

    public Instant getPostModifiedGmt() {
        return postModifiedGmt;
    }

    public WpPosts postModifiedGmt(Instant postModifiedGmt) {
        this.postModifiedGmt = postModifiedGmt;
        return this;
    }

    public void setPostModifiedGmt(Instant postModifiedGmt) {
        this.postModifiedGmt = postModifiedGmt;
    }

    public String getPostContentFiltered() {
        return postContentFiltered;
    }

    public WpPosts postContentFiltered(String postContentFiltered) {
        this.postContentFiltered = postContentFiltered;
        return this;
    }

    public void setPostContentFiltered(String postContentFiltered) {
        this.postContentFiltered = postContentFiltered;
    }

    public Long getPostParent() {
        return postParent;
    }

    public WpPosts postParent(Long postParent) {
        this.postParent = postParent;
        return this;
    }

    public void setPostParent(Long postParent) {
        this.postParent = postParent;
    }

    public String getGuid() {
        return guid;
    }

    public WpPosts guid(String guid) {
        this.guid = guid;
        return this;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Integer getMenuOrder() {
        return menuOrder;
    }

    public WpPosts menuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
        return this;
    }

    public void setMenuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getPostType() {
        return postType;
    }

    public WpPosts postType(String postType) {
        this.postType = postType;
        return this;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostMimeType() {
        return postMimeType;
    }

    public WpPosts postMimeType(String postMimeType) {
        this.postMimeType = postMimeType;
        return this;
    }

    public void setPostMimeType(String postMimeType) {
        this.postMimeType = postMimeType;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    public WpPosts commentCount(Long commentCount) {
        this.commentCount = commentCount;
        return this;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WpPosts wpPosts = (WpPosts) o;
        if (wpPosts.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPosts.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPosts{" +
            "id=" + getId() +
            ", postAuthor=" + getPostAuthor() +
            ", postDate='" + getPostDate() + "'" +
            ", postDateGmt='" + getPostDateGmt() + "'" +
            ", postContent='" + getPostContent() + "'" +
            ", postTitle='" + getPostTitle() + "'" +
            ", postExcerpt='" + getPostExcerpt() + "'" +
            ", postStatus='" + getPostStatus() + "'" +
            ", commentStatus='" + getCommentStatus() + "'" +
            ", pingStatus='" + getPingStatus() + "'" +
            ", postPassword='" + getPostPassword() + "'" +
            ", postName='" + getPostName() + "'" +
            ", toPing='" + getToPing() + "'" +
            ", pinged='" + getPinged() + "'" +
            ", postModified='" + getPostModified() + "'" +
            ", postModifiedGmt='" + getPostModifiedGmt() + "'" +
            ", postContentFiltered='" + getPostContentFiltered() + "'" +
            ", postParent=" + getPostParent() +
            ", guid='" + getGuid() + "'" +
            ", menuOrder=" + getMenuOrder() +
            ", postType='" + getPostType() + "'" +
            ", postMimeType='" + getPostMimeType() + "'" +
            ", commentCount=" + getCommentCount() +
            "}";
    }
}
