package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpOptionsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpOptions.
 */
public interface WpOptionsService {

    /**
     * Save a wpOptions.
     *
     * @param wpOptionsDTO the entity to save
     * @return the persisted entity
     */
    WpOptionsDTO save(WpOptionsDTO wpOptionsDTO);

    /**
     * Get all the wpOptions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpOptionsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpOptions.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpOptionsDTO> findOne(Long id);

    /**
     * Delete the "id" wpOptions.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpOptions corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpOptionsDTO> search(String query, Pageable pageable);
}
