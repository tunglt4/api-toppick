package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpTermmetaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpTermmeta.
 */
public interface WpTermmetaService {

    /**
     * Save a wpTermmeta.
     *
     * @param wpTermmetaDTO the entity to save
     * @return the persisted entity
     */
    WpTermmetaDTO save(WpTermmetaDTO wpTermmetaDTO);

    /**
     * Get all the wpTermmetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpTermmetaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpTermmeta.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpTermmetaDTO> findOne(Long id);

    /**
     * Delete the "id" wpTermmeta.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpTermmeta corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpTermmetaDTO> search(String query, Pageable pageable);
}
