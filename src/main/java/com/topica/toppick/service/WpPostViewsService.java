package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpPostViewsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpPostViews.
 */
public interface WpPostViewsService {

    /**
     * Save a wpPostViews.
     *
     * @param wpPostViewsDTO the entity to save
     * @return the persisted entity
     */
    WpPostViewsDTO save(WpPostViewsDTO wpPostViewsDTO);

    /**
     * Get all the wpPostViews.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPostViewsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpPostViews.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpPostViewsDTO> findOne(Long id);

    /**
     * Delete the "id" wpPostViews.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpPostViews corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPostViewsDTO> search(String query, Pageable pageable);
}
