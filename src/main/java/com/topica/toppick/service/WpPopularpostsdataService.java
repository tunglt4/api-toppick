package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpPopularpostsdataDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpPopularpostsdata.
 */
public interface WpPopularpostsdataService {

    /**
     * Save a wpPopularpostsdata.
     *
     * @param wpPopularpostsdataDTO the entity to save
     * @return the persisted entity
     */
    WpPopularpostsdataDTO save(WpPopularpostsdataDTO wpPopularpostsdataDTO);

    /**
     * Get all the wpPopularpostsdata.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPopularpostsdataDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpPopularpostsdata.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpPopularpostsdataDTO> findOne(Long id);

    /**
     * Delete the "id" wpPopularpostsdata.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpPopularpostsdata corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPopularpostsdataDTO> search(String query, Pageable pageable);
}
