package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpPostsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpPosts and its DTO WpPostsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpPostsMapper extends EntityMapper<WpPostsDTO, WpPosts> {



    default WpPosts fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpPosts wpPosts = new WpPosts();
        wpPosts.setId(id);
        return wpPosts;
    }
}
