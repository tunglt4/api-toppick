package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpCommentsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpComments and its DTO WpCommentsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpCommentsMapper extends EntityMapper<WpCommentsDTO, WpComments> {



    default WpComments fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpComments wpComments = new WpComments();
        wpComments.setId(id);
        return wpComments;
    }
}
