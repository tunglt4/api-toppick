package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpPopularpostssummaryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpPopularpostssummary and its DTO WpPopularpostssummaryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpPopularpostssummaryMapper extends EntityMapper<WpPopularpostssummaryDTO, WpPopularpostssummary> {



    default WpPopularpostssummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpPopularpostssummary wpPopularpostssummary = new WpPopularpostssummary();
        wpPopularpostssummary.setId(id);
        return wpPopularpostssummary;
    }
}
