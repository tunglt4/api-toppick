package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpLinksDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpLinks and its DTO WpLinksDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpLinksMapper extends EntityMapper<WpLinksDTO, WpLinks> {



    default WpLinks fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpLinks wpLinks = new WpLinks();
        wpLinks.setId(id);
        return wpLinks;
    }
}
