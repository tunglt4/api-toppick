package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpUsermetaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpUsermeta and its DTO WpUsermetaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpUsermetaMapper extends EntityMapper<WpUsermetaDTO, WpUsermeta> {



    default WpUsermeta fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpUsermeta wpUsermeta = new WpUsermeta();
        wpUsermeta.setId(id);
        return wpUsermeta;
    }
}
