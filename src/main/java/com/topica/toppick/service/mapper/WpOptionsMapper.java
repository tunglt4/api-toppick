package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpOptionsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpOptions and its DTO WpOptionsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpOptionsMapper extends EntityMapper<WpOptionsDTO, WpOptions> {



    default WpOptions fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpOptions wpOptions = new WpOptions();
        wpOptions.setId(id);
        return wpOptions;
    }
}
