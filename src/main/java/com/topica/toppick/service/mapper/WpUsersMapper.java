package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpUsersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpUsers and its DTO WpUsersDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpUsersMapper extends EntityMapper<WpUsersDTO, WpUsers> {



    default WpUsers fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpUsers wpUsers = new WpUsers();
        wpUsers.setId(id);
        return wpUsers;
    }
}
