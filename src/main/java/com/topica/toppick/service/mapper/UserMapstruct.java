package com.topica.toppick.service.mapper;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.topica.toppick.config.Constants;
import com.topica.toppick.domain.User;
import com.topica.toppick.domain.WpUsermeta;
import com.topica.toppick.service.dto.userresponse.UserCurrentInfoDTO;

@Mapper(componentModel = "spring", uses = {})
public interface UserMapstruct {
	@Mappings({ @Mapping(target = "levelPoint", ignore = true), @Mapping(target = "level", ignore = true),
			@Mapping(target = "follow", ignore = true), @Mapping(target = "followed", ignore = true),
			@Mapping(target = "avatarPostId", ignore = true), @Mapping(target = "avatarURL", ignore = true),
			@Mapping(target = "description", ignore = true) })
	UserCurrentInfoDTO toCurrentDTO(User user);

	default UserCurrentInfoDTO toCurrentInfoDTO(User user) {
		UserCurrentInfoDTO currentInfoDTO = toCurrentDTO(user);
		Set<WpUsermeta> usermetas = user.getUsermetas();
		for (WpUsermeta wpUsermeta : usermetas) {
			String value = wpUsermeta.getMetaValue();
			switch (wpUsermeta.getMetaKey()) {
			case Constants.META_LEVEL_POINT:
				currentInfoDTO.setLevelPoint(wpUsermeta.getMetaValue() == null ? null : Integer.valueOf(value));
				break;
			case Constants.META_DESCRIPTION:
				currentInfoDTO.setDescription(value);
				break;
			case Constants.META_LEVEL_NAME:
				currentInfoDTO.setLevel(value);
				break;

			case Constants.META_FOLLOW:
				List<Long> lstIdFollows = (value == null) ? null
						: Arrays.asList(StringUtils.split(value, ",")).stream().map(f -> Long.valueOf(f))
								.collect(Collectors.toList());
				currentInfoDTO.setFollow(lstIdFollows);
				break;
			case Constants.META_FOLLOWED:
				List<Long> lstIdFolloweds = (value == null) ? null
						: Arrays.asList(StringUtils.split(value, ",")).stream().map(f -> Long.valueOf(f))
								.collect(Collectors.toList());
				currentInfoDTO.setFollowed(lstIdFolloweds);
				break;
			case Constants.META_AVATAR_POST_ID:
				currentInfoDTO.setAvatarPostId(Long.valueOf(value));
				break;
			default:
				break;
			}
		}
		return currentInfoDTO;
	}
}
