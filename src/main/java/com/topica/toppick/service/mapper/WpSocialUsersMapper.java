package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpSocialUsersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpSocialUsers and its DTO WpSocialUsersDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpSocialUsersMapper extends EntityMapper<WpSocialUsersDTO, WpSocialUsers> {



    default WpSocialUsers fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpSocialUsers wpSocialUsers = new WpSocialUsers();
        wpSocialUsers.setId(id);
        return wpSocialUsers;
    }
}
