package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpPostViewsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpPostViews and its DTO WpPostViewsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpPostViewsMapper extends EntityMapper<WpPostViewsDTO, WpPostViews> {



    default WpPostViews fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpPostViews wpPostViews = new WpPostViews();
        wpPostViews.setId(id);
        return wpPostViews;
    }
}
