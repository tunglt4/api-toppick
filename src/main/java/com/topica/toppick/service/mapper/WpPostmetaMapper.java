package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpPostmetaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpPostmeta and its DTO WpPostmetaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpPostmetaMapper extends EntityMapper<WpPostmetaDTO, WpPostmeta> {



    default WpPostmeta fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpPostmeta wpPostmeta = new WpPostmeta();
        wpPostmeta.setId(id);
        return wpPostmeta;
    }
}
