package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpTermmetaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpTermmeta and its DTO WpTermmetaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpTermmetaMapper extends EntityMapper<WpTermmetaDTO, WpTermmeta> {



    default WpTermmeta fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpTermmeta wpTermmeta = new WpTermmeta();
        wpTermmeta.setId(id);
        return wpTermmeta;
    }
}
