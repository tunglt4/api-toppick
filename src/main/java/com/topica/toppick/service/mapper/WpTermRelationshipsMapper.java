package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpTermRelationshipsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpTermRelationships and its DTO WpTermRelationshipsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpTermRelationshipsMapper extends EntityMapper<WpTermRelationshipsDTO, WpTermRelationships> {



    default WpTermRelationships fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpTermRelationships wpTermRelationships = new WpTermRelationships();
        wpTermRelationships.setId(id);
        return wpTermRelationships;
    }
}
