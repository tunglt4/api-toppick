package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpTermsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpTerms and its DTO WpTermsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpTermsMapper extends EntityMapper<WpTermsDTO, WpTerms> {



    default WpTerms fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpTerms wpTerms = new WpTerms();
        wpTerms.setId(id);
        return wpTerms;
    }
}
