package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpTermTaxonomyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpTermTaxonomy and its DTO WpTermTaxonomyDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpTermTaxonomyMapper extends EntityMapper<WpTermTaxonomyDTO, WpTermTaxonomy> {



    default WpTermTaxonomy fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpTermTaxonomy wpTermTaxonomy = new WpTermTaxonomy();
        wpTermTaxonomy.setId(id);
        return wpTermTaxonomy;
    }
}
