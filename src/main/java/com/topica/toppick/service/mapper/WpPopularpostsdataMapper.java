package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpPopularpostsdataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpPopularpostsdata and its DTO WpPopularpostsdataDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpPopularpostsdataMapper extends EntityMapper<WpPopularpostsdataDTO, WpPopularpostsdata> {



    default WpPopularpostsdata fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpPopularpostsdata wpPopularpostsdata = new WpPopularpostsdata();
        wpPopularpostsdata.setId(id);
        return wpPopularpostsdata;
    }
}
