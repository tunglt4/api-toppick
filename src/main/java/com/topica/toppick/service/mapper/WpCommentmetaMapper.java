package com.topica.toppick.service.mapper;

import com.topica.toppick.domain.*;
import com.topica.toppick.service.dto.WpCommentmetaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity WpCommentmeta and its DTO WpCommentmetaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WpCommentmetaMapper extends EntityMapper<WpCommentmetaDTO, WpCommentmeta> {



    default WpCommentmeta fromId(Long id) {
        if (id == null) {
            return null;
        }
        WpCommentmeta wpCommentmeta = new WpCommentmeta();
        wpCommentmeta.setId(id);
        return wpCommentmeta;
    }
}
