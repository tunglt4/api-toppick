package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpTermmetaService;
import com.topica.toppick.domain.WpTermmeta;
import com.topica.toppick.repository.WpTermmetaRepository;
import com.topica.toppick.repository.search.WpTermmetaSearchRepository;
import com.topica.toppick.service.dto.WpTermmetaDTO;
import com.topica.toppick.service.mapper.WpTermmetaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpTermmeta.
 */
@Service
@Transactional
public class WpTermmetaServiceImpl implements WpTermmetaService {

    private final Logger log = LoggerFactory.getLogger(WpTermmetaServiceImpl.class);

    private WpTermmetaRepository wpTermmetaRepository;

    private WpTermmetaMapper wpTermmetaMapper;

    private WpTermmetaSearchRepository wpTermmetaSearchRepository;

    public WpTermmetaServiceImpl(WpTermmetaRepository wpTermmetaRepository, WpTermmetaMapper wpTermmetaMapper, WpTermmetaSearchRepository wpTermmetaSearchRepository) {
        this.wpTermmetaRepository = wpTermmetaRepository;
        this.wpTermmetaMapper = wpTermmetaMapper;
        this.wpTermmetaSearchRepository = wpTermmetaSearchRepository;
    }

    /**
     * Save a wpTermmeta.
     *
     * @param wpTermmetaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpTermmetaDTO save(WpTermmetaDTO wpTermmetaDTO) {
        log.debug("Request to save WpTermmeta : {}", wpTermmetaDTO);

        WpTermmeta wpTermmeta = wpTermmetaMapper.toEntity(wpTermmetaDTO);
        wpTermmeta = wpTermmetaRepository.save(wpTermmeta);
        WpTermmetaDTO result = wpTermmetaMapper.toDto(wpTermmeta);
        wpTermmetaSearchRepository.save(wpTermmeta);
        return result;
    }

    /**
     * Get all the wpTermmetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpTermmetaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpTermmetas");
        return wpTermmetaRepository.findAll(pageable)
            .map(wpTermmetaMapper::toDto);
    }


    /**
     * Get one wpTermmeta by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpTermmetaDTO> findOne(Long id) {
        log.debug("Request to get WpTermmeta : {}", id);
        return Optional.of(wpTermmetaRepository.findOne(id))
            .map(wpTermmetaMapper::toDto);
    }

    /**
     * Delete the wpTermmeta by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpTermmeta : {}", id);
        wpTermmetaRepository.delete(id);
        wpTermmetaSearchRepository.delete(id);
    }

    /**
     * Search for the wpTermmeta corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpTermmetaDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpTermmetas for query {}", query);
        return wpTermmetaSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpTermmetaMapper::toDto);
    }
}
