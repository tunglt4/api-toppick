package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpUsermetaService;
import com.topica.toppick.domain.WpUsermeta;
import com.topica.toppick.repository.WpUsermetaRepository;
import com.topica.toppick.repository.search.WpUsermetaSearchRepository;
import com.topica.toppick.service.dto.WpUsermetaDTO;
import com.topica.toppick.service.mapper.WpUsermetaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpUsermeta.
 */
@Service
@Transactional
public class WpUsermetaServiceImpl implements WpUsermetaService {

    private final Logger log = LoggerFactory.getLogger(WpUsermetaServiceImpl.class);

    private WpUsermetaRepository wpUsermetaRepository;

    private WpUsermetaMapper wpUsermetaMapper;

    private WpUsermetaSearchRepository wpUsermetaSearchRepository;

    public WpUsermetaServiceImpl(WpUsermetaRepository wpUsermetaRepository, WpUsermetaMapper wpUsermetaMapper, WpUsermetaSearchRepository wpUsermetaSearchRepository) {
        this.wpUsermetaRepository = wpUsermetaRepository;
        this.wpUsermetaMapper = wpUsermetaMapper;
        this.wpUsermetaSearchRepository = wpUsermetaSearchRepository;
    }

    /**
     * Save a wpUsermeta.
     *
     * @param wpUsermetaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpUsermetaDTO save(WpUsermetaDTO wpUsermetaDTO) {
        log.debug("Request to save WpUsermeta : {}", wpUsermetaDTO);

        WpUsermeta wpUsermeta = wpUsermetaMapper.toEntity(wpUsermetaDTO);
        wpUsermeta = wpUsermetaRepository.save(wpUsermeta);
        WpUsermetaDTO result = wpUsermetaMapper.toDto(wpUsermeta);
        wpUsermetaSearchRepository.save(wpUsermeta);
        return result;
    }

    /**
     * Get all the wpUsermetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpUsermetaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpUsermetas");
        return wpUsermetaRepository.findAll(pageable)
            .map(wpUsermetaMapper::toDto);
    }


    /**
     * Get one wpUsermeta by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpUsermetaDTO> findOne(Long id) {
        log.debug("Request to get WpUsermeta : {}", id);
        return Optional.of(wpUsermetaRepository.findOne(id))
            .map(wpUsermetaMapper::toDto);
    }

    /**
     * Delete the wpUsermeta by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpUsermeta : {}", id);
        wpUsermetaRepository.delete(id);
        wpUsermetaSearchRepository.delete(id);
    }

    /**
     * Search for the wpUsermeta corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpUsermetaDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpUsermetas for query {}", query);
        return wpUsermetaSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpUsermetaMapper::toDto);
    }
}
