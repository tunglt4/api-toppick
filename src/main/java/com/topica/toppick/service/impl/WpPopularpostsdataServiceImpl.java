package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpPopularpostsdataService;
import com.topica.toppick.domain.WpPopularpostsdata;
import com.topica.toppick.repository.WpPopularpostsdataRepository;
import com.topica.toppick.repository.search.WpPopularpostsdataSearchRepository;
import com.topica.toppick.service.dto.WpPopularpostsdataDTO;
import com.topica.toppick.service.mapper.WpPopularpostsdataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpPopularpostsdata.
 */
@Service
@Transactional
public class WpPopularpostsdataServiceImpl implements WpPopularpostsdataService {

    private final Logger log = LoggerFactory.getLogger(WpPopularpostsdataServiceImpl.class);

    private WpPopularpostsdataRepository wpPopularpostsdataRepository;

    private WpPopularpostsdataMapper wpPopularpostsdataMapper;

    private WpPopularpostsdataSearchRepository wpPopularpostsdataSearchRepository;

    public WpPopularpostsdataServiceImpl(WpPopularpostsdataRepository wpPopularpostsdataRepository, WpPopularpostsdataMapper wpPopularpostsdataMapper, WpPopularpostsdataSearchRepository wpPopularpostsdataSearchRepository) {
        this.wpPopularpostsdataRepository = wpPopularpostsdataRepository;
        this.wpPopularpostsdataMapper = wpPopularpostsdataMapper;
        this.wpPopularpostsdataSearchRepository = wpPopularpostsdataSearchRepository;
    }

    /**
     * Save a wpPopularpostsdata.
     *
     * @param wpPopularpostsdataDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpPopularpostsdataDTO save(WpPopularpostsdataDTO wpPopularpostsdataDTO) {
        log.debug("Request to save WpPopularpostsdata : {}", wpPopularpostsdataDTO);

        WpPopularpostsdata wpPopularpostsdata = wpPopularpostsdataMapper.toEntity(wpPopularpostsdataDTO);
        wpPopularpostsdata = wpPopularpostsdataRepository.save(wpPopularpostsdata);
        WpPopularpostsdataDTO result = wpPopularpostsdataMapper.toDto(wpPopularpostsdata);
        wpPopularpostsdataSearchRepository.save(wpPopularpostsdata);
        return result;
    }

    /**
     * Get all the wpPopularpostsdata.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPopularpostsdataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpPopularpostsdata");
        return wpPopularpostsdataRepository.findAll(pageable)
            .map(wpPopularpostsdataMapper::toDto);
    }


    /**
     * Get one wpPopularpostsdata by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpPopularpostsdataDTO> findOne(Long id) {
        log.debug("Request to get WpPopularpostsdata : {}", id);
        return Optional.of(wpPopularpostsdataRepository.findOne(id))
            .map(wpPopularpostsdataMapper::toDto);
    }

    /**
     * Delete the wpPopularpostsdata by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpPopularpostsdata : {}", id);
        wpPopularpostsdataRepository.delete(id);
        wpPopularpostsdataSearchRepository.delete(id);
    }

    /**
     * Search for the wpPopularpostsdata corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPopularpostsdataDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpPopularpostsdata for query {}", query);
        return wpPopularpostsdataSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpPopularpostsdataMapper::toDto);
    }
}
