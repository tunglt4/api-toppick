package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpPostmetaService;
import com.topica.toppick.domain.WpPostmeta;
import com.topica.toppick.repository.WpPostmetaRepository;
import com.topica.toppick.repository.search.WpPostmetaSearchRepository;
import com.topica.toppick.service.dto.WpPostmetaDTO;
import com.topica.toppick.service.mapper.WpPostmetaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpPostmeta.
 */
@Service
@Transactional
public class WpPostmetaServiceImpl implements WpPostmetaService {

    private final Logger log = LoggerFactory.getLogger(WpPostmetaServiceImpl.class);

    private WpPostmetaRepository wpPostmetaRepository;

    private WpPostmetaMapper wpPostmetaMapper;

    private WpPostmetaSearchRepository wpPostmetaSearchRepository;

    public WpPostmetaServiceImpl(WpPostmetaRepository wpPostmetaRepository, WpPostmetaMapper wpPostmetaMapper, WpPostmetaSearchRepository wpPostmetaSearchRepository) {
        this.wpPostmetaRepository = wpPostmetaRepository;
        this.wpPostmetaMapper = wpPostmetaMapper;
        this.wpPostmetaSearchRepository = wpPostmetaSearchRepository;
    }

    /**
     * Save a wpPostmeta.
     *
     * @param wpPostmetaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpPostmetaDTO save(WpPostmetaDTO wpPostmetaDTO) {
        log.debug("Request to save WpPostmeta : {}", wpPostmetaDTO);

        WpPostmeta wpPostmeta = wpPostmetaMapper.toEntity(wpPostmetaDTO);
        wpPostmeta = wpPostmetaRepository.save(wpPostmeta);
        WpPostmetaDTO result = wpPostmetaMapper.toDto(wpPostmeta);
        wpPostmetaSearchRepository.save(wpPostmeta);
        return result;
    }

    /**
     * Get all the wpPostmetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPostmetaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpPostmetas");
        return wpPostmetaRepository.findAll(pageable)
            .map(wpPostmetaMapper::toDto);
    }


    /**
     * Get one wpPostmeta by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpPostmetaDTO> findOne(Long id) {
        log.debug("Request to get WpPostmeta : {}", id);
        return Optional.of(wpPostmetaRepository.findOne(id))
            .map(wpPostmetaMapper::toDto);
    }

    /**
     * Delete the wpPostmeta by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpPostmeta : {}", id);
        wpPostmetaRepository.delete(id);
        wpPostmetaSearchRepository.delete(id);
    }

    /**
     * Search for the wpPostmeta corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPostmetaDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpPostmetas for query {}", query);
        return wpPostmetaSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpPostmetaMapper::toDto);
    }
}
