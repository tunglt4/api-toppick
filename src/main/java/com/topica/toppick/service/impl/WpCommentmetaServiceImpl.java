package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpCommentmetaService;
import com.topica.toppick.domain.WpCommentmeta;
import com.topica.toppick.repository.WpCommentmetaRepository;
import com.topica.toppick.repository.search.WpCommentmetaSearchRepository;
import com.topica.toppick.service.dto.WpCommentmetaDTO;
import com.topica.toppick.service.mapper.WpCommentmetaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpCommentmeta.
 */
@Service
@Transactional
public class WpCommentmetaServiceImpl implements WpCommentmetaService {

    private final Logger log = LoggerFactory.getLogger(WpCommentmetaServiceImpl.class);

    private WpCommentmetaRepository wpCommentmetaRepository;

    private WpCommentmetaMapper wpCommentmetaMapper;

    private WpCommentmetaSearchRepository wpCommentmetaSearchRepository;

    public WpCommentmetaServiceImpl(WpCommentmetaRepository wpCommentmetaRepository, WpCommentmetaMapper wpCommentmetaMapper, WpCommentmetaSearchRepository wpCommentmetaSearchRepository) {
        this.wpCommentmetaRepository = wpCommentmetaRepository;
        this.wpCommentmetaMapper = wpCommentmetaMapper;
        this.wpCommentmetaSearchRepository = wpCommentmetaSearchRepository;
    }

    /**
     * Save a wpCommentmeta.
     *
     * @param wpCommentmetaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpCommentmetaDTO save(WpCommentmetaDTO wpCommentmetaDTO) {
        log.debug("Request to save WpCommentmeta : {}", wpCommentmetaDTO);

        WpCommentmeta wpCommentmeta = wpCommentmetaMapper.toEntity(wpCommentmetaDTO);
        wpCommentmeta = wpCommentmetaRepository.save(wpCommentmeta);
        WpCommentmetaDTO result = wpCommentmetaMapper.toDto(wpCommentmeta);
        wpCommentmetaSearchRepository.save(wpCommentmeta);
        return result;
    }

    /**
     * Get all the wpCommentmetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpCommentmetaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpCommentmetas");
        return wpCommentmetaRepository.findAll(pageable)
            .map(wpCommentmetaMapper::toDto);
    }


    /**
     * Get one wpCommentmeta by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpCommentmetaDTO> findOne(Long id) {
        log.debug("Request to get WpCommentmeta : {}", id);
        return Optional.of(wpCommentmetaRepository.findOne(id))
            .map(wpCommentmetaMapper::toDto);
    }

    /**
     * Delete the wpCommentmeta by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpCommentmeta : {}", id);
        wpCommentmetaRepository.delete(id);
        wpCommentmetaSearchRepository.delete(id);
    }

    /**
     * Search for the wpCommentmeta corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpCommentmetaDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpCommentmetas for query {}", query);
        return wpCommentmetaSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpCommentmetaMapper::toDto);
    }
}
