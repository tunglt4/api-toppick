package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpUsersService;
import com.topica.toppick.domain.WpUsers;
import com.topica.toppick.repository.WpUsersRepository;
import com.topica.toppick.repository.search.WpUsersSearchRepository;
import com.topica.toppick.service.dto.WpUsersDTO;
import com.topica.toppick.service.mapper.WpUsersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpUsers.
 */
@Service
@Transactional
public class WpUsersServiceImpl implements WpUsersService {

    private final Logger log = LoggerFactory.getLogger(WpUsersServiceImpl.class);

    private WpUsersRepository wpUsersRepository;

    private WpUsersMapper wpUsersMapper;

    private WpUsersSearchRepository wpUsersSearchRepository;

    public WpUsersServiceImpl(WpUsersRepository wpUsersRepository, WpUsersMapper wpUsersMapper, WpUsersSearchRepository wpUsersSearchRepository) {
        this.wpUsersRepository = wpUsersRepository;
        this.wpUsersMapper = wpUsersMapper;
        this.wpUsersSearchRepository = wpUsersSearchRepository;
    }

    /**
     * Save a wpUsers.
     *
     * @param wpUsersDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpUsersDTO save(WpUsersDTO wpUsersDTO) {
        log.debug("Request to save WpUsers : {}", wpUsersDTO);

        WpUsers wpUsers = wpUsersMapper.toEntity(wpUsersDTO);
        wpUsers = wpUsersRepository.save(wpUsers);
        WpUsersDTO result = wpUsersMapper.toDto(wpUsers);
        wpUsersSearchRepository.save(wpUsers);
        return result;
    }

    /**
     * Get all the wpUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpUsersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpUsers");
        return wpUsersRepository.findAll(pageable)
            .map(wpUsersMapper::toDto);
    }


    /**
     * Get one wpUsers by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpUsersDTO> findOne(Long id) {
        log.debug("Request to get WpUsers : {}", id);
        return Optional.of(wpUsersRepository.findOne(id))
            .map(wpUsersMapper::toDto);
    }

    /**
     * Delete the wpUsers by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpUsers : {}", id);
        wpUsersRepository.delete(id);
        wpUsersSearchRepository.delete(id);
    }

    /**
     * Search for the wpUsers corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpUsersDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpUsers for query {}", query);
        return wpUsersSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpUsersMapper::toDto);
    }
}
