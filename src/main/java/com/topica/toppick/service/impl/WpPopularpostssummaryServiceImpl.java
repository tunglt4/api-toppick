package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpPopularpostssummaryService;
import com.topica.toppick.domain.WpPopularpostssummary;
import com.topica.toppick.repository.WpPopularpostssummaryRepository;
import com.topica.toppick.repository.search.WpPopularpostssummarySearchRepository;
import com.topica.toppick.service.dto.WpPopularpostssummaryDTO;
import com.topica.toppick.service.mapper.WpPopularpostssummaryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpPopularpostssummary.
 */
@Service
@Transactional
public class WpPopularpostssummaryServiceImpl implements WpPopularpostssummaryService {

    private final Logger log = LoggerFactory.getLogger(WpPopularpostssummaryServiceImpl.class);

    private WpPopularpostssummaryRepository wpPopularpostssummaryRepository;

    private WpPopularpostssummaryMapper wpPopularpostssummaryMapper;

    private WpPopularpostssummarySearchRepository wpPopularpostssummarySearchRepository;

    public WpPopularpostssummaryServiceImpl(WpPopularpostssummaryRepository wpPopularpostssummaryRepository, WpPopularpostssummaryMapper wpPopularpostssummaryMapper, WpPopularpostssummarySearchRepository wpPopularpostssummarySearchRepository) {
        this.wpPopularpostssummaryRepository = wpPopularpostssummaryRepository;
        this.wpPopularpostssummaryMapper = wpPopularpostssummaryMapper;
        this.wpPopularpostssummarySearchRepository = wpPopularpostssummarySearchRepository;
    }

    /**
     * Save a wpPopularpostssummary.
     *
     * @param wpPopularpostssummaryDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpPopularpostssummaryDTO save(WpPopularpostssummaryDTO wpPopularpostssummaryDTO) {
        log.debug("Request to save WpPopularpostssummary : {}", wpPopularpostssummaryDTO);

        WpPopularpostssummary wpPopularpostssummary = wpPopularpostssummaryMapper.toEntity(wpPopularpostssummaryDTO);
        wpPopularpostssummary = wpPopularpostssummaryRepository.save(wpPopularpostssummary);
        WpPopularpostssummaryDTO result = wpPopularpostssummaryMapper.toDto(wpPopularpostssummary);
        wpPopularpostssummarySearchRepository.save(wpPopularpostssummary);
        return result;
    }

    /**
     * Get all the wpPopularpostssummaries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPopularpostssummaryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpPopularpostssummaries");
        return wpPopularpostssummaryRepository.findAll(pageable)
            .map(wpPopularpostssummaryMapper::toDto);
    }


    /**
     * Get one wpPopularpostssummary by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpPopularpostssummaryDTO> findOne(Long id) {
        log.debug("Request to get WpPopularpostssummary : {}", id);
        return Optional.of(wpPopularpostssummaryRepository.findOne(id))
            .map(wpPopularpostssummaryMapper::toDto);
    }

    /**
     * Delete the wpPopularpostssummary by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpPopularpostssummary : {}", id);
        wpPopularpostssummaryRepository.delete(id);
        wpPopularpostssummarySearchRepository.delete(id);
    }

    /**
     * Search for the wpPopularpostssummary corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPopularpostssummaryDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpPopularpostssummaries for query {}", query);
        return wpPopularpostssummarySearchRepository.search(queryStringQuery(query), pageable)
            .map(wpPopularpostssummaryMapper::toDto);
    }
}
