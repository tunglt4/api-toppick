package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpPostsService;
import com.topica.toppick.domain.WpPosts;
import com.topica.toppick.repository.WpPostsRepository;
import com.topica.toppick.repository.search.WpPostsSearchRepository;
import com.topica.toppick.service.dto.WpPostsDTO;
import com.topica.toppick.service.mapper.WpPostsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpPosts.
 */
@Service
@Transactional
public class WpPostsServiceImpl implements WpPostsService {

    private final Logger log = LoggerFactory.getLogger(WpPostsServiceImpl.class);

    private WpPostsRepository wpPostsRepository;

    private WpPostsMapper wpPostsMapper;

    private WpPostsSearchRepository wpPostsSearchRepository;

    public WpPostsServiceImpl(WpPostsRepository wpPostsRepository, WpPostsMapper wpPostsMapper, WpPostsSearchRepository wpPostsSearchRepository) {
        this.wpPostsRepository = wpPostsRepository;
        this.wpPostsMapper = wpPostsMapper;
        this.wpPostsSearchRepository = wpPostsSearchRepository;
    }

    /**
     * Save a wpPosts.
     *
     * @param wpPostsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpPostsDTO save(WpPostsDTO wpPostsDTO) {
        log.debug("Request to save WpPosts : {}", wpPostsDTO);

        WpPosts wpPosts = wpPostsMapper.toEntity(wpPostsDTO);
        wpPosts = wpPostsRepository.save(wpPosts);
        WpPostsDTO result = wpPostsMapper.toDto(wpPosts);
        wpPostsSearchRepository.save(wpPosts);
        return result;
    }

    /**
     * Get all the wpPosts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPostsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpPosts");
        return wpPostsRepository.findAll(pageable)
            .map(wpPostsMapper::toDto);
    }


    /**
     * Get one wpPosts by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpPostsDTO> findOne(Long id) {
        log.debug("Request to get WpPosts : {}", id);
        return Optional.of(wpPostsRepository.findOne(id))
            .map(wpPostsMapper::toDto);
    }

    /**
     * Delete the wpPosts by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpPosts : {}", id);
        wpPostsRepository.delete(id);
        wpPostsSearchRepository.delete(id);
    }

    /**
     * Search for the wpPosts corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPostsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpPosts for query {}", query);
        return wpPostsSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpPostsMapper::toDto);
    }
}
