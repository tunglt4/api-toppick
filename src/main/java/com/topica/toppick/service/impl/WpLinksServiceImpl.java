package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpLinksService;
import com.topica.toppick.domain.WpLinks;
import com.topica.toppick.repository.WpLinksRepository;
import com.topica.toppick.repository.search.WpLinksSearchRepository;
import com.topica.toppick.service.dto.WpLinksDTO;
import com.topica.toppick.service.mapper.WpLinksMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpLinks.
 */
@Service
@Transactional
public class WpLinksServiceImpl implements WpLinksService {

    private final Logger log = LoggerFactory.getLogger(WpLinksServiceImpl.class);

    private WpLinksRepository wpLinksRepository;

    private WpLinksMapper wpLinksMapper;

    private WpLinksSearchRepository wpLinksSearchRepository;

    public WpLinksServiceImpl(WpLinksRepository wpLinksRepository, WpLinksMapper wpLinksMapper, WpLinksSearchRepository wpLinksSearchRepository) {
        this.wpLinksRepository = wpLinksRepository;
        this.wpLinksMapper = wpLinksMapper;
        this.wpLinksSearchRepository = wpLinksSearchRepository;
    }

    /**
     * Save a wpLinks.
     *
     * @param wpLinksDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpLinksDTO save(WpLinksDTO wpLinksDTO) {
        log.debug("Request to save WpLinks : {}", wpLinksDTO);

        WpLinks wpLinks = wpLinksMapper.toEntity(wpLinksDTO);
        wpLinks = wpLinksRepository.save(wpLinks);
        WpLinksDTO result = wpLinksMapper.toDto(wpLinks);
        wpLinksSearchRepository.save(wpLinks);
        return result;
    }

    /**
     * Get all the wpLinks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpLinksDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpLinks");
        return wpLinksRepository.findAll(pageable)
            .map(wpLinksMapper::toDto);
    }


    /**
     * Get one wpLinks by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpLinksDTO> findOne(Long id) {
        log.debug("Request to get WpLinks : {}", id);
        return Optional.of(wpLinksRepository.findOne(id))
            .map(wpLinksMapper::toDto);
    }

    /**
     * Delete the wpLinks by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpLinks : {}", id);
        wpLinksRepository.delete(id);
        wpLinksSearchRepository.delete(id);
    }

    /**
     * Search for the wpLinks corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpLinksDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpLinks for query {}", query);
        return wpLinksSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpLinksMapper::toDto);
    }
}
