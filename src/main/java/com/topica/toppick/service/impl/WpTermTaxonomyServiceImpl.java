package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpTermTaxonomyService;
import com.topica.toppick.domain.WpTermTaxonomy;
import com.topica.toppick.repository.WpTermTaxonomyRepository;
import com.topica.toppick.repository.search.WpTermTaxonomySearchRepository;
import com.topica.toppick.service.dto.WpTermTaxonomyDTO;
import com.topica.toppick.service.mapper.WpTermTaxonomyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpTermTaxonomy.
 */
@Service
@Transactional
public class WpTermTaxonomyServiceImpl implements WpTermTaxonomyService {

    private final Logger log = LoggerFactory.getLogger(WpTermTaxonomyServiceImpl.class);

    private WpTermTaxonomyRepository wpTermTaxonomyRepository;

    private WpTermTaxonomyMapper wpTermTaxonomyMapper;

    private WpTermTaxonomySearchRepository wpTermTaxonomySearchRepository;

    public WpTermTaxonomyServiceImpl(WpTermTaxonomyRepository wpTermTaxonomyRepository, WpTermTaxonomyMapper wpTermTaxonomyMapper, WpTermTaxonomySearchRepository wpTermTaxonomySearchRepository) {
        this.wpTermTaxonomyRepository = wpTermTaxonomyRepository;
        this.wpTermTaxonomyMapper = wpTermTaxonomyMapper;
        this.wpTermTaxonomySearchRepository = wpTermTaxonomySearchRepository;
    }

    /**
     * Save a wpTermTaxonomy.
     *
     * @param wpTermTaxonomyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpTermTaxonomyDTO save(WpTermTaxonomyDTO wpTermTaxonomyDTO) {
        log.debug("Request to save WpTermTaxonomy : {}", wpTermTaxonomyDTO);

        WpTermTaxonomy wpTermTaxonomy = wpTermTaxonomyMapper.toEntity(wpTermTaxonomyDTO);
        wpTermTaxonomy = wpTermTaxonomyRepository.save(wpTermTaxonomy);
        WpTermTaxonomyDTO result = wpTermTaxonomyMapper.toDto(wpTermTaxonomy);
        wpTermTaxonomySearchRepository.save(wpTermTaxonomy);
        return result;
    }

    /**
     * Get all the wpTermTaxonomies.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpTermTaxonomyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpTermTaxonomies");
        return wpTermTaxonomyRepository.findAll(pageable)
            .map(wpTermTaxonomyMapper::toDto);
    }


    /**
     * Get one wpTermTaxonomy by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpTermTaxonomyDTO> findOne(Long id) {
        log.debug("Request to get WpTermTaxonomy : {}", id);
        return Optional.of(wpTermTaxonomyRepository.findOne(id))
            .map(wpTermTaxonomyMapper::toDto);
    }

    /**
     * Delete the wpTermTaxonomy by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpTermTaxonomy : {}", id);
        wpTermTaxonomyRepository.delete(id);
        wpTermTaxonomySearchRepository.delete(id);
    }

    /**
     * Search for the wpTermTaxonomy corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpTermTaxonomyDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpTermTaxonomies for query {}", query);
        return wpTermTaxonomySearchRepository.search(queryStringQuery(query), pageable)
            .map(wpTermTaxonomyMapper::toDto);
    }
}
