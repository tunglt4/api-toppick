package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpOptionsService;
import com.topica.toppick.domain.WpOptions;
import com.topica.toppick.repository.WpOptionsRepository;
import com.topica.toppick.repository.search.WpOptionsSearchRepository;
import com.topica.toppick.service.dto.WpOptionsDTO;
import com.topica.toppick.service.mapper.WpOptionsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpOptions.
 */
@Service
@Transactional
public class WpOptionsServiceImpl implements WpOptionsService {

    private final Logger log = LoggerFactory.getLogger(WpOptionsServiceImpl.class);

    private WpOptionsRepository wpOptionsRepository;

    private WpOptionsMapper wpOptionsMapper;

    private WpOptionsSearchRepository wpOptionsSearchRepository;

    public WpOptionsServiceImpl(WpOptionsRepository wpOptionsRepository, WpOptionsMapper wpOptionsMapper, WpOptionsSearchRepository wpOptionsSearchRepository) {
        this.wpOptionsRepository = wpOptionsRepository;
        this.wpOptionsMapper = wpOptionsMapper;
        this.wpOptionsSearchRepository = wpOptionsSearchRepository;
    }

    /**
     * Save a wpOptions.
     *
     * @param wpOptionsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpOptionsDTO save(WpOptionsDTO wpOptionsDTO) {
        log.debug("Request to save WpOptions : {}", wpOptionsDTO);

        WpOptions wpOptions = wpOptionsMapper.toEntity(wpOptionsDTO);
        wpOptions = wpOptionsRepository.save(wpOptions);
        WpOptionsDTO result = wpOptionsMapper.toDto(wpOptions);
        wpOptionsSearchRepository.save(wpOptions);
        return result;
    }

    /**
     * Get all the wpOptions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpOptionsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpOptions");
        return wpOptionsRepository.findAll(pageable)
            .map(wpOptionsMapper::toDto);
    }


    /**
     * Get one wpOptions by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpOptionsDTO> findOne(Long id) {
        log.debug("Request to get WpOptions : {}", id);
        return Optional.of(wpOptionsRepository.findOne(id))
            .map(wpOptionsMapper::toDto);
    }

    /**
     * Delete the wpOptions by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpOptions : {}", id);
        wpOptionsRepository.delete(id);
        wpOptionsSearchRepository.delete(id);
    }

    /**
     * Search for the wpOptions corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpOptionsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpOptions for query {}", query);
        return wpOptionsSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpOptionsMapper::toDto);
    }
}
