package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpTermRelationshipsService;
import com.topica.toppick.domain.WpTermRelationships;
import com.topica.toppick.repository.WpTermRelationshipsRepository;
import com.topica.toppick.repository.search.WpTermRelationshipsSearchRepository;
import com.topica.toppick.service.dto.WpTermRelationshipsDTO;
import com.topica.toppick.service.mapper.WpTermRelationshipsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpTermRelationships.
 */
@Service
@Transactional
public class WpTermRelationshipsServiceImpl implements WpTermRelationshipsService {

    private final Logger log = LoggerFactory.getLogger(WpTermRelationshipsServiceImpl.class);

    private WpTermRelationshipsRepository wpTermRelationshipsRepository;

    private WpTermRelationshipsMapper wpTermRelationshipsMapper;

    private WpTermRelationshipsSearchRepository wpTermRelationshipsSearchRepository;

    public WpTermRelationshipsServiceImpl(WpTermRelationshipsRepository wpTermRelationshipsRepository, WpTermRelationshipsMapper wpTermRelationshipsMapper, WpTermRelationshipsSearchRepository wpTermRelationshipsSearchRepository) {
        this.wpTermRelationshipsRepository = wpTermRelationshipsRepository;
        this.wpTermRelationshipsMapper = wpTermRelationshipsMapper;
        this.wpTermRelationshipsSearchRepository = wpTermRelationshipsSearchRepository;
    }

    /**
     * Save a wpTermRelationships.
     *
     * @param wpTermRelationshipsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpTermRelationshipsDTO save(WpTermRelationshipsDTO wpTermRelationshipsDTO) {
        log.debug("Request to save WpTermRelationships : {}", wpTermRelationshipsDTO);

        WpTermRelationships wpTermRelationships = wpTermRelationshipsMapper.toEntity(wpTermRelationshipsDTO);
        wpTermRelationships = wpTermRelationshipsRepository.save(wpTermRelationships);
        WpTermRelationshipsDTO result = wpTermRelationshipsMapper.toDto(wpTermRelationships);
        wpTermRelationshipsSearchRepository.save(wpTermRelationships);
        return result;
    }

    /**
     * Get all the wpTermRelationships.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpTermRelationshipsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpTermRelationships");
        return wpTermRelationshipsRepository.findAll(pageable)
            .map(wpTermRelationshipsMapper::toDto);
    }


    /**
     * Get one wpTermRelationships by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpTermRelationshipsDTO> findOne(Long id) {
        log.debug("Request to get WpTermRelationships : {}", id);
        return Optional.of(wpTermRelationshipsRepository.findOne(id))
            .map(wpTermRelationshipsMapper::toDto);
    }

    /**
     * Delete the wpTermRelationships by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpTermRelationships : {}", id);
        wpTermRelationshipsRepository.delete(id);
        wpTermRelationshipsSearchRepository.delete(id);
    }

    /**
     * Search for the wpTermRelationships corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpTermRelationshipsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpTermRelationships for query {}", query);
        return wpTermRelationshipsSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpTermRelationshipsMapper::toDto);
    }
}
