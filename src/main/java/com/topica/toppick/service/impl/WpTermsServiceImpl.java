package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpTermsService;
import com.topica.toppick.domain.WpTerms;
import com.topica.toppick.repository.WpTermsRepository;
import com.topica.toppick.repository.search.WpTermsSearchRepository;
import com.topica.toppick.service.dto.WpTermsDTO;
import com.topica.toppick.service.mapper.WpTermsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpTerms.
 */
@Service
@Transactional
public class WpTermsServiceImpl implements WpTermsService {

    private final Logger log = LoggerFactory.getLogger(WpTermsServiceImpl.class);

    private WpTermsRepository wpTermsRepository;

    private WpTermsMapper wpTermsMapper;

    private WpTermsSearchRepository wpTermsSearchRepository;

    public WpTermsServiceImpl(WpTermsRepository wpTermsRepository, WpTermsMapper wpTermsMapper, WpTermsSearchRepository wpTermsSearchRepository) {
        this.wpTermsRepository = wpTermsRepository;
        this.wpTermsMapper = wpTermsMapper;
        this.wpTermsSearchRepository = wpTermsSearchRepository;
    }

    /**
     * Save a wpTerms.
     *
     * @param wpTermsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpTermsDTO save(WpTermsDTO wpTermsDTO) {
        log.debug("Request to save WpTerms : {}", wpTermsDTO);

        WpTerms wpTerms = wpTermsMapper.toEntity(wpTermsDTO);
        wpTerms = wpTermsRepository.save(wpTerms);
        WpTermsDTO result = wpTermsMapper.toDto(wpTerms);
        wpTermsSearchRepository.save(wpTerms);
        return result;
    }

    /**
     * Get all the wpTerms.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpTermsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpTerms");
        return wpTermsRepository.findAll(pageable)
            .map(wpTermsMapper::toDto);
    }


    /**
     * Get one wpTerms by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpTermsDTO> findOne(Long id) {
        log.debug("Request to get WpTerms : {}", id);
        return Optional.of(wpTermsRepository.findOne(id))
            .map(wpTermsMapper::toDto);
    }

    /**
     * Delete the wpTerms by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpTerms : {}", id);
        wpTermsRepository.delete(id);
        wpTermsSearchRepository.delete(id);
    }

    /**
     * Search for the wpTerms corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpTermsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpTerms for query {}", query);
        return wpTermsSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpTermsMapper::toDto);
    }
}
