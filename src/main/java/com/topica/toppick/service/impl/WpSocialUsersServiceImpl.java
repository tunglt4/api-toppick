package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpSocialUsersService;
import com.topica.toppick.domain.WpSocialUsers;
import com.topica.toppick.repository.WpSocialUsersRepository;
import com.topica.toppick.repository.search.WpSocialUsersSearchRepository;
import com.topica.toppick.service.dto.WpSocialUsersDTO;
import com.topica.toppick.service.mapper.WpSocialUsersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpSocialUsers.
 */
@Service
@Transactional
public class WpSocialUsersServiceImpl implements WpSocialUsersService {

    private final Logger log = LoggerFactory.getLogger(WpSocialUsersServiceImpl.class);

    private WpSocialUsersRepository wpSocialUsersRepository;

    private WpSocialUsersMapper wpSocialUsersMapper;

    private WpSocialUsersSearchRepository wpSocialUsersSearchRepository;

    public WpSocialUsersServiceImpl(WpSocialUsersRepository wpSocialUsersRepository, WpSocialUsersMapper wpSocialUsersMapper, WpSocialUsersSearchRepository wpSocialUsersSearchRepository) {
        this.wpSocialUsersRepository = wpSocialUsersRepository;
        this.wpSocialUsersMapper = wpSocialUsersMapper;
        this.wpSocialUsersSearchRepository = wpSocialUsersSearchRepository;
    }

    /**
     * Save a wpSocialUsers.
     *
     * @param wpSocialUsersDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpSocialUsersDTO save(WpSocialUsersDTO wpSocialUsersDTO) {
        log.debug("Request to save WpSocialUsers : {}", wpSocialUsersDTO);

        WpSocialUsers wpSocialUsers = wpSocialUsersMapper.toEntity(wpSocialUsersDTO);
        wpSocialUsers = wpSocialUsersRepository.save(wpSocialUsers);
        WpSocialUsersDTO result = wpSocialUsersMapper.toDto(wpSocialUsers);
        wpSocialUsersSearchRepository.save(wpSocialUsers);
        return result;
    }

    /**
     * Get all the wpSocialUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpSocialUsersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpSocialUsers");
        return wpSocialUsersRepository.findAll(pageable)
            .map(wpSocialUsersMapper::toDto);
    }


    /**
     * Get one wpSocialUsers by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpSocialUsersDTO> findOne(Long id) {
        log.debug("Request to get WpSocialUsers : {}", id);
        return Optional.of(wpSocialUsersRepository.findOne(id))
            .map(wpSocialUsersMapper::toDto);
    }

    /**
     * Delete the wpSocialUsers by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpSocialUsers : {}", id);
        wpSocialUsersRepository.delete(id);
        wpSocialUsersSearchRepository.delete(id);
    }

    /**
     * Search for the wpSocialUsers corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpSocialUsersDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpSocialUsers for query {}", query);
        return wpSocialUsersSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpSocialUsersMapper::toDto);
    }
}
