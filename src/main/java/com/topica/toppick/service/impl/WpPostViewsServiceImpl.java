package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpPostViewsService;
import com.topica.toppick.domain.WpPostViews;
import com.topica.toppick.repository.WpPostViewsRepository;
import com.topica.toppick.repository.search.WpPostViewsSearchRepository;
import com.topica.toppick.service.dto.WpPostViewsDTO;
import com.topica.toppick.service.mapper.WpPostViewsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpPostViews.
 */
@Service
@Transactional
public class WpPostViewsServiceImpl implements WpPostViewsService {

    private final Logger log = LoggerFactory.getLogger(WpPostViewsServiceImpl.class);

    private WpPostViewsRepository wpPostViewsRepository;

    private WpPostViewsMapper wpPostViewsMapper;

    private WpPostViewsSearchRepository wpPostViewsSearchRepository;

    public WpPostViewsServiceImpl(WpPostViewsRepository wpPostViewsRepository, WpPostViewsMapper wpPostViewsMapper, WpPostViewsSearchRepository wpPostViewsSearchRepository) {
        this.wpPostViewsRepository = wpPostViewsRepository;
        this.wpPostViewsMapper = wpPostViewsMapper;
        this.wpPostViewsSearchRepository = wpPostViewsSearchRepository;
    }

    /**
     * Save a wpPostViews.
     *
     * @param wpPostViewsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpPostViewsDTO save(WpPostViewsDTO wpPostViewsDTO) {
        log.debug("Request to save WpPostViews : {}", wpPostViewsDTO);

        WpPostViews wpPostViews = wpPostViewsMapper.toEntity(wpPostViewsDTO);
        wpPostViews = wpPostViewsRepository.save(wpPostViews);
        WpPostViewsDTO result = wpPostViewsMapper.toDto(wpPostViews);
        wpPostViewsSearchRepository.save(wpPostViews);
        return result;
    }

    /**
     * Get all the wpPostViews.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPostViewsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpPostViews");
        return wpPostViewsRepository.findAll(pageable)
            .map(wpPostViewsMapper::toDto);
    }


    /**
     * Get one wpPostViews by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpPostViewsDTO> findOne(Long id) {
        log.debug("Request to get WpPostViews : {}", id);
        return Optional.of(wpPostViewsRepository.findOne(id))
            .map(wpPostViewsMapper::toDto);
    }

    /**
     * Delete the wpPostViews by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpPostViews : {}", id);
        wpPostViewsRepository.delete(id);
        wpPostViewsSearchRepository.delete(id);
    }

    /**
     * Search for the wpPostViews corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpPostViewsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpPostViews for query {}", query);
        return wpPostViewsSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpPostViewsMapper::toDto);
    }
}
