package com.topica.toppick.service.impl;

import com.topica.toppick.service.WpCommentsService;
import com.topica.toppick.domain.WpComments;
import com.topica.toppick.repository.WpCommentsRepository;
import com.topica.toppick.repository.search.WpCommentsSearchRepository;
import com.topica.toppick.service.dto.WpCommentsDTO;
import com.topica.toppick.service.mapper.WpCommentsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing WpComments.
 */
@Service
@Transactional
public class WpCommentsServiceImpl implements WpCommentsService {

    private final Logger log = LoggerFactory.getLogger(WpCommentsServiceImpl.class);

    private WpCommentsRepository wpCommentsRepository;

    private WpCommentsMapper wpCommentsMapper;

    private WpCommentsSearchRepository wpCommentsSearchRepository;

    public WpCommentsServiceImpl(WpCommentsRepository wpCommentsRepository, WpCommentsMapper wpCommentsMapper, WpCommentsSearchRepository wpCommentsSearchRepository) {
        this.wpCommentsRepository = wpCommentsRepository;
        this.wpCommentsMapper = wpCommentsMapper;
        this.wpCommentsSearchRepository = wpCommentsSearchRepository;
    }

    /**
     * Save a wpComments.
     *
     * @param wpCommentsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public WpCommentsDTO save(WpCommentsDTO wpCommentsDTO) {
        log.debug("Request to save WpComments : {}", wpCommentsDTO);

        WpComments wpComments = wpCommentsMapper.toEntity(wpCommentsDTO);
        wpComments = wpCommentsRepository.save(wpComments);
        WpCommentsDTO result = wpCommentsMapper.toDto(wpComments);
        wpCommentsSearchRepository.save(wpComments);
        return result;
    }

    /**
     * Get all the wpComments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpCommentsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WpComments");
        return wpCommentsRepository.findAll(pageable)
            .map(wpCommentsMapper::toDto);
    }


    /**
     * Get one wpComments by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WpCommentsDTO> findOne(Long id) {
        log.debug("Request to get WpComments : {}", id);
        return Optional.of(wpCommentsRepository.findOne(id))
            .map(wpCommentsMapper::toDto);
    }

    /**
     * Delete the wpComments by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WpComments : {}", id);
        wpCommentsRepository.delete(id);
        wpCommentsSearchRepository.delete(id);
    }

    /**
     * Search for the wpComments corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WpCommentsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of WpComments for query {}", query);
        return wpCommentsSearchRepository.search(queryStringQuery(query), pageable)
            .map(wpCommentsMapper::toDto);
    }
}
