package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpLinksDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpLinks.
 */
public interface WpLinksService {

    /**
     * Save a wpLinks.
     *
     * @param wpLinksDTO the entity to save
     * @return the persisted entity
     */
    WpLinksDTO save(WpLinksDTO wpLinksDTO);

    /**
     * Get all the wpLinks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpLinksDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpLinks.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpLinksDTO> findOne(Long id);

    /**
     * Delete the "id" wpLinks.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpLinks corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpLinksDTO> search(String query, Pageable pageable);
}
