package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpPostmetaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpPostmeta.
 */
public interface WpPostmetaService {

    /**
     * Save a wpPostmeta.
     *
     * @param wpPostmetaDTO the entity to save
     * @return the persisted entity
     */
    WpPostmetaDTO save(WpPostmetaDTO wpPostmetaDTO);

    /**
     * Get all the wpPostmetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPostmetaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpPostmeta.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpPostmetaDTO> findOne(Long id);

    /**
     * Delete the "id" wpPostmeta.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpPostmeta corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPostmetaDTO> search(String query, Pageable pageable);
}
