package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpPopularpostssummaryDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpPopularpostssummary.
 */
public interface WpPopularpostssummaryService {

    /**
     * Save a wpPopularpostssummary.
     *
     * @param wpPopularpostssummaryDTO the entity to save
     * @return the persisted entity
     */
    WpPopularpostssummaryDTO save(WpPopularpostssummaryDTO wpPopularpostssummaryDTO);

    /**
     * Get all the wpPopularpostssummaries.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPopularpostssummaryDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpPopularpostssummary.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpPopularpostssummaryDTO> findOne(Long id);

    /**
     * Delete the "id" wpPopularpostssummary.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpPopularpostssummary corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPopularpostssummaryDTO> search(String query, Pageable pageable);
}
