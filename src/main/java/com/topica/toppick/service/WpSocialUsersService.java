package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpSocialUsersDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpSocialUsers.
 */
public interface WpSocialUsersService {

    /**
     * Save a wpSocialUsers.
     *
     * @param wpSocialUsersDTO the entity to save
     * @return the persisted entity
     */
    WpSocialUsersDTO save(WpSocialUsersDTO wpSocialUsersDTO);

    /**
     * Get all the wpSocialUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpSocialUsersDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpSocialUsers.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpSocialUsersDTO> findOne(Long id);

    /**
     * Delete the "id" wpSocialUsers.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpSocialUsers corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpSocialUsersDTO> search(String query, Pageable pageable);
}
