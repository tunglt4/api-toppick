package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpTermsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpTerms.
 */
public interface WpTermsService {

    /**
     * Save a wpTerms.
     *
     * @param wpTermsDTO the entity to save
     * @return the persisted entity
     */
    WpTermsDTO save(WpTermsDTO wpTermsDTO);

    /**
     * Get all the wpTerms.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpTermsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpTerms.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpTermsDTO> findOne(Long id);

    /**
     * Delete the "id" wpTerms.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpTerms corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpTermsDTO> search(String query, Pageable pageable);
}
