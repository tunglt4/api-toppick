package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpTermRelationshipsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpTermRelationships.
 */
public interface WpTermRelationshipsService {

    /**
     * Save a wpTermRelationships.
     *
     * @param wpTermRelationshipsDTO the entity to save
     * @return the persisted entity
     */
    WpTermRelationshipsDTO save(WpTermRelationshipsDTO wpTermRelationshipsDTO);

    /**
     * Get all the wpTermRelationships.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpTermRelationshipsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpTermRelationships.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpTermRelationshipsDTO> findOne(Long id);

    /**
     * Delete the "id" wpTermRelationships.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpTermRelationships corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpTermRelationshipsDTO> search(String query, Pageable pageable);
}
