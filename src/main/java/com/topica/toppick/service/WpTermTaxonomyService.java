package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpTermTaxonomyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpTermTaxonomy.
 */
public interface WpTermTaxonomyService {

    /**
     * Save a wpTermTaxonomy.
     *
     * @param wpTermTaxonomyDTO the entity to save
     * @return the persisted entity
     */
    WpTermTaxonomyDTO save(WpTermTaxonomyDTO wpTermTaxonomyDTO);

    /**
     * Get all the wpTermTaxonomies.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpTermTaxonomyDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpTermTaxonomy.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpTermTaxonomyDTO> findOne(Long id);

    /**
     * Delete the "id" wpTermTaxonomy.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpTermTaxonomy corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpTermTaxonomyDTO> search(String query, Pageable pageable);
}
