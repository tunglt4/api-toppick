package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpTerms entity.
 */
public class WpTermsDTO implements Serializable {

    private Long id;

    private Long termId;

    private String name;

    private String slug;

    private Long termGroup;

    private Integer termOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTermId() {
        return termId;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Long getTermGroup() {
        return termGroup;
    }

    public void setTermGroup(Long termGroup) {
        this.termGroup = termGroup;
    }

    public Integer getTermOrder() {
        return termOrder;
    }

    public void setTermOrder(Integer termOrder) {
        this.termOrder = termOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpTermsDTO wpTermsDTO = (WpTermsDTO) o;
        if (wpTermsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpTermsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpTermsDTO{" +
            "id=" + getId() +
            ", termId=" + getTermId() +
            ", name='" + getName() + "'" +
            ", slug='" + getSlug() + "'" +
            ", termGroup=" + getTermGroup() +
            ", termOrder=" + getTermOrder() +
            "}";
    }
}
