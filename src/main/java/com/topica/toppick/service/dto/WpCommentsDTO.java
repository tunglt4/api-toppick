package com.topica.toppick.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpComments entity.
 */
public class WpCommentsDTO implements Serializable {

    private Long id;

    private Long commentId;

    private Long commentPostId;

    private String commentAuthor;

    private String commentAuthorEmail;

    private String commentAuthorUrl;

    private String commentAuthorIp;

    private Instant commentDate;

    private Instant commentDateGmt;

    private String commentContent;

    private Integer commentKarma;

    private String commentApproved;

    private String commentAgent;

    private String commentType;

    private Long commentParent;

    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getCommentPostId() {
        return commentPostId;
    }

    public void setCommentPostId(Long commentPostId) {
        this.commentPostId = commentPostId;
    }

    public String getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public String getCommentAuthorEmail() {
        return commentAuthorEmail;
    }

    public void setCommentAuthorEmail(String commentAuthorEmail) {
        this.commentAuthorEmail = commentAuthorEmail;
    }

    public String getCommentAuthorUrl() {
        return commentAuthorUrl;
    }

    public void setCommentAuthorUrl(String commentAuthorUrl) {
        this.commentAuthorUrl = commentAuthorUrl;
    }

    public String getCommentAuthorIp() {
        return commentAuthorIp;
    }

    public void setCommentAuthorIp(String commentAuthorIp) {
        this.commentAuthorIp = commentAuthorIp;
    }

    public Instant getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Instant commentDate) {
        this.commentDate = commentDate;
    }

    public Instant getCommentDateGmt() {
        return commentDateGmt;
    }

    public void setCommentDateGmt(Instant commentDateGmt) {
        this.commentDateGmt = commentDateGmt;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public Integer getCommentKarma() {
        return commentKarma;
    }

    public void setCommentKarma(Integer commentKarma) {
        this.commentKarma = commentKarma;
    }

    public String getCommentApproved() {
        return commentApproved;
    }

    public void setCommentApproved(String commentApproved) {
        this.commentApproved = commentApproved;
    }

    public String getCommentAgent() {
        return commentAgent;
    }

    public void setCommentAgent(String commentAgent) {
        this.commentAgent = commentAgent;
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
    }

    public Long getCommentParent() {
        return commentParent;
    }

    public void setCommentParent(Long commentParent) {
        this.commentParent = commentParent;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpCommentsDTO wpCommentsDTO = (WpCommentsDTO) o;
        if (wpCommentsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpCommentsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpCommentsDTO{" +
            "id=" + getId() +
            ", commentId=" + getCommentId() +
            ", commentPostId=" + getCommentPostId() +
            ", commentAuthor='" + getCommentAuthor() + "'" +
            ", commentAuthorEmail='" + getCommentAuthorEmail() + "'" +
            ", commentAuthorUrl='" + getCommentAuthorUrl() + "'" +
            ", commentAuthorIp='" + getCommentAuthorIp() + "'" +
            ", commentDate='" + getCommentDate() + "'" +
            ", commentDateGmt='" + getCommentDateGmt() + "'" +
            ", commentContent='" + getCommentContent() + "'" +
            ", commentKarma=" + getCommentKarma() +
            ", commentApproved='" + getCommentApproved() + "'" +
            ", commentAgent='" + getCommentAgent() + "'" +
            ", commentType='" + getCommentType() + "'" +
            ", commentParent=" + getCommentParent() +
            ", userId=" + getUserId() +
            "}";
    }
}
