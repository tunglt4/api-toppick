package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpCommentmeta entity.
 */
public class WpCommentmetaDTO implements Serializable {

    private Long id;

    private Long metaId;

    private Long commentId;

    private String metaKey;

    private String metaValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMetaId() {
        return metaId;
    }

    public void setMetaId(Long metaId) {
        this.metaId = metaId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpCommentmetaDTO wpCommentmetaDTO = (WpCommentmetaDTO) o;
        if (wpCommentmetaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpCommentmetaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpCommentmetaDTO{" +
            "id=" + getId() +
            ", metaId=" + getMetaId() +
            ", commentId=" + getCommentId() +
            ", metaKey='" + getMetaKey() + "'" +
            ", metaValue='" + getMetaValue() + "'" +
            "}";
    }
}
