package com.topica.toppick.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpLinks entity.
 */
public class WpLinksDTO implements Serializable {

    private Long id;

    private Long linkId;

    private String linkUrl;

    private String linkName;

    private String linkImage;

    private String linkTarget;

    private String linkDescription;

    private String linkVisible;

    private Long linkOwner;

    private Integer linkRating;

    private Instant linkUpdated;

    private String linkRel;

    private String linkNotes;

    private String linkRss;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public String getLinkTarget() {
        return linkTarget;
    }

    public void setLinkTarget(String linkTarget) {
        this.linkTarget = linkTarget;
    }

    public String getLinkDescription() {
        return linkDescription;
    }

    public void setLinkDescription(String linkDescription) {
        this.linkDescription = linkDescription;
    }

    public String getLinkVisible() {
        return linkVisible;
    }

    public void setLinkVisible(String linkVisible) {
        this.linkVisible = linkVisible;
    }

    public Long getLinkOwner() {
        return linkOwner;
    }

    public void setLinkOwner(Long linkOwner) {
        this.linkOwner = linkOwner;
    }

    public Integer getLinkRating() {
        return linkRating;
    }

    public void setLinkRating(Integer linkRating) {
        this.linkRating = linkRating;
    }

    public Instant getLinkUpdated() {
        return linkUpdated;
    }

    public void setLinkUpdated(Instant linkUpdated) {
        this.linkUpdated = linkUpdated;
    }

    public String getLinkRel() {
        return linkRel;
    }

    public void setLinkRel(String linkRel) {
        this.linkRel = linkRel;
    }

    public String getLinkNotes() {
        return linkNotes;
    }

    public void setLinkNotes(String linkNotes) {
        this.linkNotes = linkNotes;
    }

    public String getLinkRss() {
        return linkRss;
    }

    public void setLinkRss(String linkRss) {
        this.linkRss = linkRss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpLinksDTO wpLinksDTO = (WpLinksDTO) o;
        if (wpLinksDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpLinksDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpLinksDTO{" +
            "id=" + getId() +
            ", linkId=" + getLinkId() +
            ", linkUrl='" + getLinkUrl() + "'" +
            ", linkName='" + getLinkName() + "'" +
            ", linkImage='" + getLinkImage() + "'" +
            ", linkTarget='" + getLinkTarget() + "'" +
            ", linkDescription='" + getLinkDescription() + "'" +
            ", linkVisible='" + getLinkVisible() + "'" +
            ", linkOwner=" + getLinkOwner() +
            ", linkRating=" + getLinkRating() +
            ", linkUpdated='" + getLinkUpdated() + "'" +
            ", linkRel='" + getLinkRel() + "'" +
            ", linkNotes='" + getLinkNotes() + "'" +
            ", linkRss='" + getLinkRss() + "'" +
            "}";
    }
}
