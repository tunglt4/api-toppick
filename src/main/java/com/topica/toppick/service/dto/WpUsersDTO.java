package com.topica.toppick.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpUsers entity.
 */
public class WpUsersDTO implements Serializable {

    private Long id;

    private String userLogin;

    private String userPass;

    private String userNicename;

    private String userEmail;

    private String userUrl;

    private Instant userRegistered;

    private String userActivationKey;

    private Integer userStatus;

    private String displayName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getUserNicename() {
        return userNicename;
    }

    public void setUserNicename(String userNicename) {
        this.userNicename = userNicename;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    public Instant getUserRegistered() {
        return userRegistered;
    }

    public void setUserRegistered(Instant userRegistered) {
        this.userRegistered = userRegistered;
    }

    public String getUserActivationKey() {
        return userActivationKey;
    }

    public void setUserActivationKey(String userActivationKey) {
        this.userActivationKey = userActivationKey;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpUsersDTO wpUsersDTO = (WpUsersDTO) o;
        if (wpUsersDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpUsersDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpUsersDTO{" +
            "id=" + getId() +
            ", userLogin='" + getUserLogin() + "'" +
            ", userPass='" + getUserPass() + "'" +
            ", userNicename='" + getUserNicename() + "'" +
            ", userEmail='" + getUserEmail() + "'" +
            ", userUrl='" + getUserUrl() + "'" +
            ", userRegistered='" + getUserRegistered() + "'" +
            ", userActivationKey='" + getUserActivationKey() + "'" +
            ", userStatus=" + getUserStatus() +
            ", displayName='" + getDisplayName() + "'" +
            "}";
    }
}
