package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpSocialUsers entity.
 */
public class WpSocialUsersDTO implements Serializable {

    private Long id;

    private String type;

    private String identifier;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpSocialUsersDTO wpSocialUsersDTO = (WpSocialUsersDTO) o;
        if (wpSocialUsersDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpSocialUsersDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpSocialUsersDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", identifier='" + getIdentifier() + "'" +
            "}";
    }
}
