package com.topica.toppick.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpPosts entity.
 */
public class WpPostsDTO implements Serializable {

    private Long id;

    private Long postAuthor;

    private Instant postDate;

    private Instant postDateGmt;

    private String postContent;

    private String postTitle;

    private String postExcerpt;

    private String postStatus;

    private String commentStatus;

    private String pingStatus;

    private String postPassword;

    private String postName;

    private String toPing;

    private String pinged;

    private Instant postModified;

    private Instant postModifiedGmt;

    private String postContentFiltered;

    private Long postParent;

    private String guid;

    private Integer menuOrder;

    private String postType;

    private String postMimeType;

    private Long commentCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(Long postAuthor) {
        this.postAuthor = postAuthor;
    }

    public Instant getPostDate() {
        return postDate;
    }

    public void setPostDate(Instant postDate) {
        this.postDate = postDate;
    }

    public Instant getPostDateGmt() {
        return postDateGmt;
    }

    public void setPostDateGmt(Instant postDateGmt) {
        this.postDateGmt = postDateGmt;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostExcerpt() {
        return postExcerpt;
    }

    public void setPostExcerpt(String postExcerpt) {
        this.postExcerpt = postExcerpt;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getPingStatus() {
        return pingStatus;
    }

    public void setPingStatus(String pingStatus) {
        this.pingStatus = pingStatus;
    }

    public String getPostPassword() {
        return postPassword;
    }

    public void setPostPassword(String postPassword) {
        this.postPassword = postPassword;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getToPing() {
        return toPing;
    }

    public void setToPing(String toPing) {
        this.toPing = toPing;
    }

    public String getPinged() {
        return pinged;
    }

    public void setPinged(String pinged) {
        this.pinged = pinged;
    }

    public Instant getPostModified() {
        return postModified;
    }

    public void setPostModified(Instant postModified) {
        this.postModified = postModified;
    }

    public Instant getPostModifiedGmt() {
        return postModifiedGmt;
    }

    public void setPostModifiedGmt(Instant postModifiedGmt) {
        this.postModifiedGmt = postModifiedGmt;
    }

    public String getPostContentFiltered() {
        return postContentFiltered;
    }

    public void setPostContentFiltered(String postContentFiltered) {
        this.postContentFiltered = postContentFiltered;
    }

    public Long getPostParent() {
        return postParent;
    }

    public void setPostParent(Long postParent) {
        this.postParent = postParent;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Integer getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostMimeType() {
        return postMimeType;
    }

    public void setPostMimeType(String postMimeType) {
        this.postMimeType = postMimeType;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpPostsDTO wpPostsDTO = (WpPostsDTO) o;
        if (wpPostsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPostsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPostsDTO{" +
            "id=" + getId() +
            ", postAuthor=" + getPostAuthor() +
            ", postDate='" + getPostDate() + "'" +
            ", postDateGmt='" + getPostDateGmt() + "'" +
            ", postContent='" + getPostContent() + "'" +
            ", postTitle='" + getPostTitle() + "'" +
            ", postExcerpt='" + getPostExcerpt() + "'" +
            ", postStatus='" + getPostStatus() + "'" +
            ", commentStatus='" + getCommentStatus() + "'" +
            ", pingStatus='" + getPingStatus() + "'" +
            ", postPassword='" + getPostPassword() + "'" +
            ", postName='" + getPostName() + "'" +
            ", toPing='" + getToPing() + "'" +
            ", pinged='" + getPinged() + "'" +
            ", postModified='" + getPostModified() + "'" +
            ", postModifiedGmt='" + getPostModifiedGmt() + "'" +
            ", postContentFiltered='" + getPostContentFiltered() + "'" +
            ", postParent=" + getPostParent() +
            ", guid='" + getGuid() + "'" +
            ", menuOrder=" + getMenuOrder() +
            ", postType='" + getPostType() + "'" +
            ", postMimeType='" + getPostMimeType() + "'" +
            ", commentCount=" + getCommentCount() +
            "}";
    }
}
