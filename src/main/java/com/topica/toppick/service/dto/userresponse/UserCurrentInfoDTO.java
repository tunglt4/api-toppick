package com.topica.toppick.service.dto.userresponse;

import java.util.List;

/**
 * A DTO representing a user, with his authorities. name giới thiệu avatar level
 * reward info flowing"
 * 
 */

public class UserCurrentInfoDTO {
	private String login;
	private String niceName;
	private String displayName;
	private String description;
	private String email;
	private Integer levelPoint;
	private String level;
	private List<Long> follow;
	private List<Long> followed;
	private Long avatarPostId;
	private String avatarURL;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNiceName() {
		return niceName;
	}

	public void setNiceName(String niceName) {
		this.niceName = niceName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getLevelPoint() {
		return levelPoint;
	}

	public void setLevelPoint(Integer levelPoint) {
		this.levelPoint = levelPoint;
	}

	public List<Long> getFollow() {
		return follow;
	}

	public void setFollow(List<Long> follow) {
		this.follow = follow;
	}

	public List<Long> getFollowed() {
		return followed;
	}

	public void setFollowed(List<Long> followed) {
		this.followed = followed;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getAvatarPostId() {
		return avatarPostId;
	}

	public void setAvatarPostId(Long avatarPostId) {
		this.avatarPostId = avatarPostId;
	}

	public String getAvatarURL() {
		return avatarURL;
	}

	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}

}
