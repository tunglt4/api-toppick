package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpTermRelationships entity.
 */
public class WpTermRelationshipsDTO implements Serializable {

    private Long id;

    private Long objectId;

    private Long termTaxonomyId;

    private Integer termOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getTermTaxonomyId() {
        return termTaxonomyId;
    }

    public void setTermTaxonomyId(Long termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
    }

    public Integer getTermOrder() {
        return termOrder;
    }

    public void setTermOrder(Integer termOrder) {
        this.termOrder = termOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpTermRelationshipsDTO wpTermRelationshipsDTO = (WpTermRelationshipsDTO) o;
        if (wpTermRelationshipsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpTermRelationshipsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpTermRelationshipsDTO{" +
            "id=" + getId() +
            ", objectId=" + getObjectId() +
            ", termTaxonomyId=" + getTermTaxonomyId() +
            ", termOrder=" + getTermOrder() +
            "}";
    }
}
