package com.topica.toppick.service.dto;

import java.time.Instant;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpPopularpostssummary entity.
 */
public class WpPopularpostssummaryDTO implements Serializable {

    private Long id;

    private Long postid;

    private Long pageviews;

    private LocalDate viewDate;

    private Instant viewDatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostid() {
        return postid;
    }

    public void setPostid(Long postid) {
        this.postid = postid;
    }

    public Long getPageviews() {
        return pageviews;
    }

    public void setPageviews(Long pageviews) {
        this.pageviews = pageviews;
    }

    public LocalDate getViewDate() {
        return viewDate;
    }

    public void setViewDate(LocalDate viewDate) {
        this.viewDate = viewDate;
    }

    public Instant getViewDatetime() {
        return viewDatetime;
    }

    public void setViewDatetime(Instant viewDatetime) {
        this.viewDatetime = viewDatetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpPopularpostssummaryDTO wpPopularpostssummaryDTO = (WpPopularpostssummaryDTO) o;
        if (wpPopularpostssummaryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPopularpostssummaryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPopularpostssummaryDTO{" +
            "id=" + getId() +
            ", postid=" + getPostid() +
            ", pageviews=" + getPageviews() +
            ", viewDate='" + getViewDate() + "'" +
            ", viewDatetime='" + getViewDatetime() + "'" +
            "}";
    }
}
