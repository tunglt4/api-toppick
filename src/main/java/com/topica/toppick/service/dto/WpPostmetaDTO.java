package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpPostmeta entity.
 */
public class WpPostmetaDTO implements Serializable {

    private Long id;

    private Long metaId;

    private Long postId;

    private String metaKey;

    private String metaValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMetaId() {
        return metaId;
    }

    public void setMetaId(Long metaId) {
        this.metaId = metaId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpPostmetaDTO wpPostmetaDTO = (WpPostmetaDTO) o;
        if (wpPostmetaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPostmetaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPostmetaDTO{" +
            "id=" + getId() +
            ", metaId=" + getMetaId() +
            ", postId=" + getPostId() +
            ", metaKey='" + getMetaKey() + "'" +
            ", metaValue='" + getMetaValue() + "'" +
            "}";
    }
}
