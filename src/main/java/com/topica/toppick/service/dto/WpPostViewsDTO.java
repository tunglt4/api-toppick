package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpPostViews entity.
 */
public class WpPostViewsDTO implements Serializable {

    private Long id;

    private Integer type;

    private String period;

    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpPostViewsDTO wpPostViewsDTO = (WpPostViewsDTO) o;
        if (wpPostViewsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPostViewsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPostViewsDTO{" +
            "id=" + getId() +
            ", type=" + getType() +
            ", period='" + getPeriod() + "'" +
            ", count=" + getCount() +
            "}";
    }
}
