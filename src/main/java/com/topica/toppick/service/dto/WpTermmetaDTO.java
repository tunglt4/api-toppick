package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpTermmeta entity.
 */
public class WpTermmetaDTO implements Serializable {

    private Long id;

    private Long metaId;

    private Long termId;

    private String metaKey;

    private String metaValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMetaId() {
        return metaId;
    }

    public void setMetaId(Long metaId) {
        this.metaId = metaId;
    }

    public Long getTermId() {
        return termId;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpTermmetaDTO wpTermmetaDTO = (WpTermmetaDTO) o;
        if (wpTermmetaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpTermmetaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpTermmetaDTO{" +
            "id=" + getId() +
            ", metaId=" + getMetaId() +
            ", termId=" + getTermId() +
            ", metaKey='" + getMetaKey() + "'" +
            ", metaValue='" + getMetaValue() + "'" +
            "}";
    }
}
