package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpTermTaxonomy entity.
 */
public class WpTermTaxonomyDTO implements Serializable {

    private Long id;

    private Long termTaxonomyId;

    private Long termId;

    private String taxonomy;

    private String description;

    private Long parent;

    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTermTaxonomyId() {
        return termTaxonomyId;
    }

    public void setTermTaxonomyId(Long termTaxonomyId) {
        this.termTaxonomyId = termTaxonomyId;
    }

    public Long getTermId() {
        return termId;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpTermTaxonomyDTO wpTermTaxonomyDTO = (WpTermTaxonomyDTO) o;
        if (wpTermTaxonomyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpTermTaxonomyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpTermTaxonomyDTO{" +
            "id=" + getId() +
            ", termTaxonomyId=" + getTermTaxonomyId() +
            ", termId=" + getTermId() +
            ", taxonomy='" + getTaxonomy() + "'" +
            ", description='" + getDescription() + "'" +
            ", parent=" + getParent() +
            ", count=" + getCount() +
            "}";
    }
}
