package com.topica.toppick.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpOptions entity.
 */
public class WpOptionsDTO implements Serializable {

    private Long id;

    private Long optionId;

    private String optionName;

    private String optionValue;

    private String autoload;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOptionId() {
        return optionId;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    public String getAutoload() {
        return autoload;
    }

    public void setAutoload(String autoload) {
        this.autoload = autoload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpOptionsDTO wpOptionsDTO = (WpOptionsDTO) o;
        if (wpOptionsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpOptionsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpOptionsDTO{" +
            "id=" + getId() +
            ", optionId=" + getOptionId() +
            ", optionName='" + getOptionName() + "'" +
            ", optionValue='" + getOptionValue() + "'" +
            ", autoload='" + getAutoload() + "'" +
            "}";
    }
}
