package com.topica.toppick.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the WpPopularpostsdata entity.
 */
public class WpPopularpostsdataDTO implements Serializable {

    private Long id;

    private Long postid;

    private Instant day;

    private Instant lastViewed;

    private Long pageviews;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostid() {
        return postid;
    }

    public void setPostid(Long postid) {
        this.postid = postid;
    }

    public Instant getDay() {
        return day;
    }

    public void setDay(Instant day) {
        this.day = day;
    }

    public Instant getLastViewed() {
        return lastViewed;
    }

    public void setLastViewed(Instant lastViewed) {
        this.lastViewed = lastViewed;
    }

    public Long getPageviews() {
        return pageviews;
    }

    public void setPageviews(Long pageviews) {
        this.pageviews = pageviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WpPopularpostsdataDTO wpPopularpostsdataDTO = (WpPopularpostsdataDTO) o;
        if (wpPopularpostsdataDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wpPopularpostsdataDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WpPopularpostsdataDTO{" +
            "id=" + getId() +
            ", postid=" + getPostid() +
            ", day='" + getDay() + "'" +
            ", lastViewed='" + getLastViewed() + "'" +
            ", pageviews=" + getPageviews() +
            "}";
    }
}
