package com.topica.toppick.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.topica.toppick.config.Constants;
import com.topica.toppick.domain.Authority;
import com.topica.toppick.domain.User;
import com.topica.toppick.domain.WpPosts;
import com.topica.toppick.domain.WpUsermeta;
import com.topica.toppick.repository.AuthorityRepository;
import com.topica.toppick.repository.UserRepository;
import com.topica.toppick.repository.WpPostsRepository;
import com.topica.toppick.repository.search.UserSearchRepository;
import com.topica.toppick.security.AuthoritiesConstants;
import com.topica.toppick.security.SecurityUtils;
import com.topica.toppick.service.dto.UserDTO;
import com.topica.toppick.service.dto.userresponse.UserCurrentInfoDTO;
import com.topica.toppick.service.mapper.UserMapstruct;
import com.topica.toppick.service.util.RandomUtil;
import com.topica.toppick.web.rest.errors.InternalServerErrorException;
import com.topica.toppick.web.rest.vm.CurrentInfoVM;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	private final SocialService socialService;

	private final UserSearchRepository userSearchRepository;

	private final AuthorityRepository authorityRepository;

	private final WpPostsRepository wpPostsRepository;

	private final CacheManager cacheManager;

	private final UserMapstruct userMapstruct;

	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, SocialService socialService,
			UserSearchRepository userSearchRepository, AuthorityRepository authorityRepository,
			CacheManager cacheManager, UserMapstruct userMapstruct, WpPostsRepository wpPostsRepository) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.socialService = socialService;
		this.userSearchRepository = userSearchRepository;
		this.authorityRepository = authorityRepository;
		this.cacheManager = cacheManager;
		this.userMapstruct = userMapstruct;
		this.wpPostsRepository = wpPostsRepository;
	}

	public Optional<User> activateRegistration(String key) {
		log.debug("Activating user for activation key {}", key);
		return userRepository.findOneByActivationKey(key).map(user -> {
			// activate given user for the registration key.
			user.setActivated(true);
			user.setActivationKey(null);
			userSearchRepository.save(user);
			cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
			cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
			log.debug("Activated user: {}", user);
			return user;
		});
	}

	public Optional<User> completePasswordReset(String newPassword, String key) {
		log.debug("Reset user password for reset key {}", key);

		return userRepository.findOneByResetKey(key)
				.filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400))).map(user -> {
					user.setPassword(passwordEncoder.encode(newPassword));
					user.setResetKey(null);
					user.setResetDate(null);
					cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
					cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
					return user;
				});
	}

	public Optional<User> requestPasswordReset(String mail) {
		return userRepository.findOneByEmailIgnoreCase(mail).filter(User::getActivated).map(user -> {
			user.setResetKey(RandomUtil.generateResetKey());
			user.setResetDate(Instant.now());
			cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
			cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
			return user;
		});
	}

	public User registerUser(UserDTO userDTO, String password) {

		User newUser = new User();
		Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
		Set<Authority> authorities = new HashSet<>();
		String encryptedPassword = passwordEncoder.encode(password);
		newUser.setLogin(userDTO.getLogin());
		// new user gets initially a generated password
		newUser.setPassword(encryptedPassword);
		newUser.setFirstName(userDTO.getFirstName());
		newUser.setLastName(userDTO.getLastName());
		newUser.setEmail(userDTO.getEmail());
		newUser.setImageUrl(userDTO.getImageUrl());
		newUser.setLangKey(userDTO.getLangKey());
		// new user is not active
		newUser.setActivated(false);
		// new user gets registration key
		newUser.setActivationKey(RandomUtil.generateActivationKey());
		authorities.add(authority);
		newUser.setAuthorities(authorities);
		userRepository.save(newUser);
		userSearchRepository.save(newUser);
		cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(newUser.getLogin());
		cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(newUser.getEmail());
		log.debug("Created Information for User: {}", newUser);
		return newUser;
	}

	public User createUser(UserDTO userDTO) {
		User user = new User();
		user.setLogin(userDTO.getLogin());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setEmail(userDTO.getEmail());
		user.setImageUrl(userDTO.getImageUrl());
		if (userDTO.getLangKey() == null) {
			user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
		} else {
			user.setLangKey(userDTO.getLangKey());
		}
		if (userDTO.getAuthorities() != null) {
			Set<Authority> authorities = userDTO.getAuthorities().stream().map(authorityRepository::findOne)
					.collect(Collectors.toSet());
			user.setAuthorities(authorities);
		}
		String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
		user.setPassword(encryptedPassword);
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(Instant.now());
		user.setActivated(true);
		userRepository.save(user);
		userSearchRepository.save(user);
		cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
		cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
		log.debug("Created Information for User: {}", user);
		return user;
	}

	/**
	 * Update basic information (first name, last name, email, language) for the
	 * current user.
	 *
	 * @param firstName
	 *            first name of user
	 * @param lastName
	 *            last name of user
	 * @param email
	 *            email id of user
	 * @param langKey
	 *            language key
	 * @param imageUrl
	 *            image URL of user
	 */
	public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
		SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setLangKey(langKey);
			user.setImageUrl(imageUrl);
			userSearchRepository.save(user);
			cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
			cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
			log.debug("Changed Information for User: {}", user);
		});
	}

	/**
	 * Update all information for a specific user, and return the modified user.
	 *
	 * @param userDTO
	 *            user to update
	 * @return updated user
	 */
	public Optional<UserDTO> updateUser(UserDTO userDTO) {
		return Optional.of(userRepository.findOne(userDTO.getId())).map(user -> {
			user.setLogin(userDTO.getLogin());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setEmail(userDTO.getEmail());
			user.setImageUrl(userDTO.getImageUrl());
			user.setActivated(userDTO.isActivated());
			user.setLangKey(userDTO.getLangKey());
			Set<Authority> managedAuthorities = user.getAuthorities();
			managedAuthorities.clear();
			userDTO.getAuthorities().stream().map(authorityRepository::findOne).forEach(managedAuthorities::add);
			userSearchRepository.save(user);
			cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
			cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(UserDTO::new);
	}

	public void deleteUser(String login) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
			socialService.deleteUserSocialConnection(user.getLogin());
			userRepository.delete(user);
			userSearchRepository.delete(user);
			cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
			cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
			log.debug("Deleted User: {}", user);
		});
	}

	public void changePassword(String password) {
		SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			String encryptedPassword = passwordEncoder.encode(password);
			user.setPassword(encryptedPassword);
			cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
			cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
			log.debug("Changed password for User: {}", user);
		});
	}

	@Transactional(readOnly = true)
	public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
		return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthoritiesByLogin(String login) {
		return userRepository.findOneWithAuthoritiesByLogin(login);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthorities(Long id) {
		return userRepository.findOneWithAuthoritiesById(id);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthorities() {
		return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
	}

	/**
	 * Get current info user
	 * 
	 * @return UserCurrentInfoDTO
	 */
	@Transactional(readOnly = true)
	public UserCurrentInfoDTO getCurrentInfo() {
		Optional<User> optional = SecurityUtils.getCurrentUserLogin()
				.flatMap(userRepository::findOneWithUsermetasByLogin);
		if (!optional.isPresent()) {
			new InternalServerErrorException("User could not be found");
		}
		User user = optional.get();
		// Get basic information
		UserCurrentInfoDTO infoDTO = userMapstruct.toCurrentInfoDTO(user);
		// Get avatar by post
		if (infoDTO.getAvatarPostId() != null) {
			WpPosts wpPosts = wpPostsRepository.findOne(infoDTO.getAvatarPostId());
			if (wpPosts != null) {
				infoDTO.setAvatarURL(wpPosts.getGuid());
			}
		}

		return infoDTO;
	}

	/**
	 * Not activated users should be automatically deleted after 3 days.
	 * <p>
	 * This is scheduled to get fired everyday, at 01:00 (am).
	 */
	@Scheduled(cron = "0 0 1 * * ?")
	public void removeNotActivatedUsers() {
		List<User> users = userRepository
				.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
		for (User user : users) {
			log.debug("Deleting not activated user {}", user.getLogin());
			userRepository.delete(user);
			userSearchRepository.delete(user);
			cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE).evict(user.getLogin());
			cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE).evict(user.getEmail());
		}
	}

	/**
	 * @return a list of all the authorities
	 */
	public List<String> getAuthorities() {
		return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
	}

	/**
	 * Update current info
	 * 
	 * @param info
	 * @param login
	 */
	public void updateCurrentInfo(CurrentInfoVM info, String login) {
		log.info("Update user current info by login {}", login);
		// Check login
		Optional<User> optional = userRepository.findOneWithUsermetasByLogin(login);
		if (!optional.isPresent()) {
			throw new InternalServerErrorException("User could not be found");
		}
		User user = optional.get();
		for (WpUsermeta wpUsermeta : user.getUsermetas()) {
			switch (wpUsermeta.getMetaKey()) {
			case Constants.META_DESCRIPTION:
				wpUsermeta.setMetaValue(info.getDescription());
				break;
			case Constants.META_AVATAR_POST_ID:
				wpUsermeta.setMetaValue(String.valueOf(info.getWpPostId()));
				break;
			default:
				break;
			}
		}

		// Create meta description if description not exists
		addUserMetas(user, Constants.META_DESCRIPTION, info.getDescription());
		// Create meta avatar if avatar field not exists
		addUserMetas(user, Constants.META_AVATAR_POST_ID, String.valueOf(info.getWpPostId()));
		userRepository.save(user);
	}

	private void addUserMetas(User user, String metaKey, String value) {
		if (user.getUsermetas().stream().filter(f -> StringUtils.equals(f.getMetaKey(), Constants.META_AVATAR_POST_ID))
				.count() == 0) {
			WpUsermeta usermeta = new WpUsermeta();
			usermeta.setMetaKey(metaKey);
			usermeta.setMetaValue(String.valueOf(value));
			usermeta.setUser(user);
			user.getUsermetas().add(usermeta);
		}
	}
}
