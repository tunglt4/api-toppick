package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpPostsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpPosts.
 */
public interface WpPostsService {

    /**
     * Save a wpPosts.
     *
     * @param wpPostsDTO the entity to save
     * @return the persisted entity
     */
    WpPostsDTO save(WpPostsDTO wpPostsDTO);

    /**
     * Get all the wpPosts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPostsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpPosts.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpPostsDTO> findOne(Long id);

    /**
     * Delete the "id" wpPosts.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpPosts corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpPostsDTO> search(String query, Pageable pageable);
}
