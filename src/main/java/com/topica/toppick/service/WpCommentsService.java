package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpCommentsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpComments.
 */
public interface WpCommentsService {

    /**
     * Save a wpComments.
     *
     * @param wpCommentsDTO the entity to save
     * @return the persisted entity
     */
    WpCommentsDTO save(WpCommentsDTO wpCommentsDTO);

    /**
     * Get all the wpComments.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpCommentsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpComments.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpCommentsDTO> findOne(Long id);

    /**
     * Delete the "id" wpComments.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpComments corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpCommentsDTO> search(String query, Pageable pageable);
}
