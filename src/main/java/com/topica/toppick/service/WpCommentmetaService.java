package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpCommentmetaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpCommentmeta.
 */
public interface WpCommentmetaService {

    /**
     * Save a wpCommentmeta.
     *
     * @param wpCommentmetaDTO the entity to save
     * @return the persisted entity
     */
    WpCommentmetaDTO save(WpCommentmetaDTO wpCommentmetaDTO);

    /**
     * Get all the wpCommentmetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpCommentmetaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpCommentmeta.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpCommentmetaDTO> findOne(Long id);

    /**
     * Delete the "id" wpCommentmeta.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpCommentmeta corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpCommentmetaDTO> search(String query, Pageable pageable);
}
