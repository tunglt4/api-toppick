package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpUsermetaDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpUsermeta.
 */
public interface WpUsermetaService {

    /**
     * Save a wpUsermeta.
     *
     * @param wpUsermetaDTO the entity to save
     * @return the persisted entity
     */
    WpUsermetaDTO save(WpUsermetaDTO wpUsermetaDTO);

    /**
     * Get all the wpUsermetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpUsermetaDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpUsermeta.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpUsermetaDTO> findOne(Long id);

    /**
     * Delete the "id" wpUsermeta.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpUsermeta corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpUsermetaDTO> search(String query, Pageable pageable);
}
