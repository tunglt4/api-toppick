package com.topica.toppick.service;

import com.topica.toppick.service.dto.WpUsersDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing WpUsers.
 */
public interface WpUsersService {

    /**
     * Save a wpUsers.
     *
     * @param wpUsersDTO the entity to save
     * @return the persisted entity
     */
    WpUsersDTO save(WpUsersDTO wpUsersDTO);

    /**
     * Get all the wpUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpUsersDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wpUsers.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<WpUsersDTO> findOne(Long id);

    /**
     * Delete the "id" wpUsers.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the wpUsers corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<WpUsersDTO> search(String query, Pageable pageable);
}
