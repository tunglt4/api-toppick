package com.topica.toppick.service;

import java.time.Instant;
import java.util.HashSet;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.stereotype.Service;

import com.topica.toppick.domain.Authority;
import com.topica.toppick.domain.User;
import com.topica.toppick.repository.AuthorityRepository;
import com.topica.toppick.repository.UserRepository;
import com.topica.toppick.repository.search.UserSearchRepository;
import com.topica.toppick.security.AuthoritiesConstants;

@Service
public class SocialService {

	private final Logger log = LoggerFactory.getLogger(SocialService.class);

	private final UsersConnectionRepository usersConnectionRepository;

	private final AuthorityRepository authorityRepository;

	private final PasswordEncoder passwordEncoder;

	private final UserRepository userRepository;

	private final MailService mailService;

	private final UserSearchRepository userSearchRepository;

	public SocialService(UsersConnectionRepository usersConnectionRepository, AuthorityRepository authorityRepository,
			PasswordEncoder passwordEncoder, UserRepository userRepository, MailService mailService,
			UserSearchRepository userSearchRepository) {

		this.usersConnectionRepository = usersConnectionRepository;
		this.authorityRepository = authorityRepository;
		this.passwordEncoder = passwordEncoder;
		this.userRepository = userRepository;
		this.mailService = mailService;
		this.userSearchRepository = userSearchRepository;
	}

	public void deleteUserSocialConnection(String login) {
		ConnectionRepository connectionRepository = usersConnectionRepository.createConnectionRepository(login);
		connectionRepository.findAllConnections().keySet().stream().forEach(providerId -> {
			connectionRepository.removeConnections(providerId);
			log.debug("Delete user social connection providerId: {}", providerId);
		});
	}

	public User createSocialUser(Connection<?> connection, String langKey) {
		if (connection == null) {
			log.error("Cannot create social user because connection is null");
			throw new IllegalArgumentException("Connection cannot be null");
		}
		UserProfile userProfile = connection.fetchUserProfile();
		String providerId = connection.getKey().getProviderId();
		String imageUrl = connection.getImageUrl();
		User user = createUserIfNotExist(userProfile, langKey, providerId, imageUrl);
		createSocialConnection(user.getEmail(), connection);
		return user;
	}

	@Transactional
	private User createUserIfNotExist(UserProfile userProfile, String langKey, String providerId, String imageUrl) {
		String email = userProfile.getEmail();
		String userName = userProfile.getUsername();
		if (!StringUtils.isBlank(userName)) {
			userName = userName.toLowerCase(Locale.ENGLISH);
		}
		if (StringUtils.isBlank(email) && StringUtils.isBlank(userName)) {
			log.error("Cannot create social user because email and login are null");
			throw new IllegalArgumentException("Email and login cannot be null");
		}
		if (StringUtils.isBlank(email) && userRepository.findOneByLogin(userName).isPresent()) {
			log.error("Cannot create social user because email is null and login already exist, login -> {}", userName);
			throw new IllegalArgumentException("Email cannot be null with an existing login");
		}
		if (!StringUtils.isBlank(email)) {
			Optional<User> optional = userRepository.findOneByEmailIgnoreCase(email);
			if (optional.isPresent()) {
				log.info("User already exist associate the connection to this account");
				User user = optional.get();
				if (user.getAuthorities() == null || user.getAuthorities().size() == 0) {
					Set<Authority> authorities = new HashSet<>(1);
					authorities.add(authorityRepository.findOne(AuthoritiesConstants.USER));
					user.setAuthorities(authorities);
					userSearchRepository.save(user);
					return userRepository.save(user);
				}
				return optional.get();
			}
		}

		String login = getLoginDependingOnProviderId(userProfile, providerId);
		String encryptedPassword = passwordEncoder.encode(RandomStringUtils.random(10));
		Set<Authority> authorities = new HashSet<>(1);
		authorities.add(authorityRepository.findOne(AuthoritiesConstants.USER));

		User newUser = new User();
		newUser.setLogin(login);
		newUser.setPassword(encryptedPassword);
		newUser.setFirstName(userProfile.getFirstName());
		newUser.setLastName(userProfile.getLastName());
		newUser.setNiceName(login);
		newUser.setDisplayName(String.format("%s %s", userProfile.getLastName(), userProfile.getFirstName()));
		newUser.setEmail(email);
		newUser.setActivated(true);
		newUser.setStatus(0L);
		newUser.setRegisted(Instant.now());
		newUser.setAuthorities(authorities);
		newUser.setLangKey(langKey);
		newUser.setImageUrl(imageUrl);

		userSearchRepository.save(newUser);
		return userRepository.save(newUser);
	}

	/**
	 * @return login if provider manage a login like Twitter or GitHub otherwise
	 *         email address. Because provider like Google or Facebook didn't
	 *         provide login or login like "12099388847393"
	 */
	private String getLoginDependingOnProviderId(UserProfile userProfile, String providerId) {
		switch (providerId) {
		case "twitter":
			return userProfile.getUsername().toLowerCase();
		case "google":
			return userProfile.getEmail().toLowerCase();
		case "facebook":
			return userProfile.getId() != null ? userProfile.getId() : userProfile.getEmail();
		default:
			return userProfile.getId();
		}
	}

	private void createSocialConnection(String login, Connection<?> connection) {
		ConnectionRepository connectionRepository = usersConnectionRepository.createConnectionRepository(login);
		connectionRepository.addConnection(connection);
	}
}
