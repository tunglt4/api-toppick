package com.topica.toppick.repository;

import com.topica.toppick.domain.WpTermRelationships;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpTermRelationships entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpTermRelationshipsRepository extends JpaRepository<WpTermRelationships, Long> {

}
