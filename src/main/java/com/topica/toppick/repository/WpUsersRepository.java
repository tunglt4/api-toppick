package com.topica.toppick.repository;

import com.topica.toppick.domain.WpUsers;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpUsers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpUsersRepository extends JpaRepository<WpUsers, Long> {

}
