package com.topica.toppick.repository;

import com.topica.toppick.domain.WpCommentmeta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpCommentmeta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpCommentmetaRepository extends JpaRepository<WpCommentmeta, Long> {

}
