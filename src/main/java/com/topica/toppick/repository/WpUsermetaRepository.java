package com.topica.toppick.repository;

import com.topica.toppick.domain.WpUsermeta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpUsermeta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpUsermetaRepository extends JpaRepository<WpUsermeta, Long> {

}
