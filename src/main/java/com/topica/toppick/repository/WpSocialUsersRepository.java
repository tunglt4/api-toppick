package com.topica.toppick.repository;

import com.topica.toppick.domain.WpSocialUsers;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpSocialUsers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpSocialUsersRepository extends JpaRepository<WpSocialUsers, Long> {

}
