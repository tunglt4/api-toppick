package com.topica.toppick.repository;

import com.topica.toppick.domain.WpTerms;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpTerms entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpTermsRepository extends JpaRepository<WpTerms, Long> {

}
