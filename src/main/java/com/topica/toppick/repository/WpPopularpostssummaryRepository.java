package com.topica.toppick.repository;

import com.topica.toppick.domain.WpPopularpostssummary;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpPopularpostssummary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpPopularpostssummaryRepository extends JpaRepository<WpPopularpostssummary, Long> {

}
