package com.topica.toppick.repository;

import com.topica.toppick.domain.WpPostmeta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpPostmeta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpPostmetaRepository extends JpaRepository<WpPostmeta, Long> {

}
