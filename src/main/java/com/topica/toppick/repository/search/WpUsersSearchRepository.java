package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpUsers;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpUsers entity.
 */
public interface WpUsersSearchRepository extends ElasticsearchRepository<WpUsers, Long> {
}
