package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpCommentmeta;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpCommentmeta entity.
 */
public interface WpCommentmetaSearchRepository extends ElasticsearchRepository<WpCommentmeta, Long> {
}
