package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpComments;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpComments entity.
 */
public interface WpCommentsSearchRepository extends ElasticsearchRepository<WpComments, Long> {
}
