package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpPopularpostsdata;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpPopularpostsdata entity.
 */
public interface WpPopularpostsdataSearchRepository extends ElasticsearchRepository<WpPopularpostsdata, Long> {
}
