package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpTermmeta;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpTermmeta entity.
 */
public interface WpTermmetaSearchRepository extends ElasticsearchRepository<WpTermmeta, Long> {
}
