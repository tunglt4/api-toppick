package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpOptions;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpOptions entity.
 */
public interface WpOptionsSearchRepository extends ElasticsearchRepository<WpOptions, Long> {
}
