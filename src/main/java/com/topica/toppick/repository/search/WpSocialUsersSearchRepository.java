package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpSocialUsers;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpSocialUsers entity.
 */
public interface WpSocialUsersSearchRepository extends ElasticsearchRepository<WpSocialUsers, Long> {
}
