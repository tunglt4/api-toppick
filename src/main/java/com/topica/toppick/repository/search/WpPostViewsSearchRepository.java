package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpPostViews;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpPostViews entity.
 */
public interface WpPostViewsSearchRepository extends ElasticsearchRepository<WpPostViews, Long> {
}
