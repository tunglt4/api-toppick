package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpLinks;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpLinks entity.
 */
public interface WpLinksSearchRepository extends ElasticsearchRepository<WpLinks, Long> {
}
