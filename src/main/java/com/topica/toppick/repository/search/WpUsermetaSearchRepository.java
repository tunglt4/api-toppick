package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpUsermeta;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpUsermeta entity.
 */
public interface WpUsermetaSearchRepository extends ElasticsearchRepository<WpUsermeta, Long> {
}
