package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpTermRelationships;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpTermRelationships entity.
 */
public interface WpTermRelationshipsSearchRepository extends ElasticsearchRepository<WpTermRelationships, Long> {
}
