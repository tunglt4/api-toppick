package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpTerms;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpTerms entity.
 */
public interface WpTermsSearchRepository extends ElasticsearchRepository<WpTerms, Long> {
}
