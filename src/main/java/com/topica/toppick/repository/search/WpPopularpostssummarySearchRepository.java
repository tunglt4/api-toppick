package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpPopularpostssummary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpPopularpostssummary entity.
 */
public interface WpPopularpostssummarySearchRepository extends ElasticsearchRepository<WpPopularpostssummary, Long> {
}
