package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpTermTaxonomy;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpTermTaxonomy entity.
 */
public interface WpTermTaxonomySearchRepository extends ElasticsearchRepository<WpTermTaxonomy, Long> {
}
