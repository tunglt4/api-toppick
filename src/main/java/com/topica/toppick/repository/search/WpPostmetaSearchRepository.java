package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpPostmeta;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpPostmeta entity.
 */
public interface WpPostmetaSearchRepository extends ElasticsearchRepository<WpPostmeta, Long> {
}
