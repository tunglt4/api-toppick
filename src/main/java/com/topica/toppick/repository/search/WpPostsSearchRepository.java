package com.topica.toppick.repository.search;

import com.topica.toppick.domain.WpPosts;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the WpPosts entity.
 */
public interface WpPostsSearchRepository extends ElasticsearchRepository<WpPosts, Long> {
}
