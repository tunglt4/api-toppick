package com.topica.toppick.repository;

import com.topica.toppick.domain.WpComments;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpComments entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpCommentsRepository extends JpaRepository<WpComments, Long> {

}
