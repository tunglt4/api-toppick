package com.topica.toppick.repository;

import com.topica.toppick.domain.WpTermTaxonomy;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpTermTaxonomy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpTermTaxonomyRepository extends JpaRepository<WpTermTaxonomy, Long> {

}
