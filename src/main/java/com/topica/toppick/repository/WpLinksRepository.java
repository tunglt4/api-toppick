package com.topica.toppick.repository;

import com.topica.toppick.domain.WpLinks;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpLinks entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpLinksRepository extends JpaRepository<WpLinks, Long> {

}
