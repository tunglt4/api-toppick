package com.topica.toppick.repository;

import com.topica.toppick.domain.WpTermmeta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpTermmeta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpTermmetaRepository extends JpaRepository<WpTermmeta, Long> {

}
