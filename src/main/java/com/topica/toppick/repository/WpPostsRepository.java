package com.topica.toppick.repository;

import com.topica.toppick.domain.WpPosts;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpPosts entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpPostsRepository extends JpaRepository<WpPosts, Long> {

}
