package com.topica.toppick.repository;

import com.topica.toppick.domain.WpPostViews;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpPostViews entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpPostViewsRepository extends JpaRepository<WpPostViews, Long> {

}
