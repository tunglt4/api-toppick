package com.topica.toppick.repository;

import com.topica.toppick.domain.WpOptions;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpOptions entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpOptionsRepository extends JpaRepository<WpOptions, Long> {

}
