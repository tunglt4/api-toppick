package com.topica.toppick.repository;

import com.topica.toppick.domain.WpPopularpostsdata;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WpPopularpostsdata entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WpPopularpostsdataRepository extends JpaRepository<WpPopularpostsdata, Long> {

}
