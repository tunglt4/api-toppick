package com.topica.toppick.config;

/**
 * Application constants.
 */
public final class Constants {

	// Regex for acceptable logins
	public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

	public static final String SYSTEM_ACCOUNT = "system";
	public static final String ANONYMOUS_USER = "anonymoususer";
	public static final String DEFAULT_LANGUAGE = "en";

	// CONSTANT META KEY
	public static final String META_LEVEL_POINT = "status";
	public static final String META_LEVEL_NAME = "level";
	public static final String META_FOLLOW = "follow";
	public static final String META_FOLLOWED = "followed";
	public static final String META_DESCRIPTION = "description";
	public static final String META_AVATAR_POST_ID = "avatar";

	private Constants() {
	}
}
