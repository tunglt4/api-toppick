package com.topica.toppick.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of WpTermRelationshipsSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class WpTermRelationshipsSearchRepositoryMockConfiguration {

    @MockBean
    private WpTermRelationshipsSearchRepository mockWpTermRelationshipsSearchRepository;

}
