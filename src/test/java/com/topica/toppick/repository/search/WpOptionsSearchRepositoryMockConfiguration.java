package com.topica.toppick.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of WpOptionsSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class WpOptionsSearchRepositoryMockConfiguration {

    @MockBean
    private WpOptionsSearchRepository mockWpOptionsSearchRepository;

}
