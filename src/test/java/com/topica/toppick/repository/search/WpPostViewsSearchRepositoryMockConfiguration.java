package com.topica.toppick.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of WpPostViewsSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class WpPostViewsSearchRepositoryMockConfiguration {

    @MockBean
    private WpPostViewsSearchRepository mockWpPostViewsSearchRepository;

}
