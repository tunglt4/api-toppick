package com.topica.toppick.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of WpTermTaxonomySearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class WpTermTaxonomySearchRepositoryMockConfiguration {

    @MockBean
    private WpTermTaxonomySearchRepository mockWpTermTaxonomySearchRepository;

}
