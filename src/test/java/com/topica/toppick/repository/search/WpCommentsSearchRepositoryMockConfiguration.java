package com.topica.toppick.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of WpCommentsSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class WpCommentsSearchRepositoryMockConfiguration {

    @MockBean
    private WpCommentsSearchRepository mockWpCommentsSearchRepository;

}
